//db.banco.find({ nome: "Alfa"}, {"agencia.endereco": 1}) -> o primeiro é o filtro e o segundo sao os campos q tu quer encontrar

db.dropDatabase()

db.createCollection("banco")
db.createCollection("agencia")
db.createCollection("endereco")
db.createCollection("consolidacao")
db.createCollection("conta")
db.createCollection("pessoas")
db.createCollection("pais")
db.createCollection("estado")
db.createCollection("cidade")
db.createCollection("bairro")

pais = {
    nome: "Brasil"
}
db.pais.insert(pais)
db.pais.find()

estado = {
    nome: "Rio Grande do Sul",
    id_pais: ObjectId("60bfb757ccac9226bc718497")
}
db.estado.insert(estado)
db.estado.find()

cidade = {
    nome: "Porto Alegre",
    id_estado: ObjectId("60bfb7b0ccac9226bc718498")
}
db.cidade.insert(cidade)
db.cidade.find()

bairro = {
    nome: "Sarandi",
    id_cidade: ObjectId("60bfb7d8ccac9226bc718499")
}
db.bairro.insert(bairro)
db.bairro.find()

endereco = {
    logradouro: "rua testando",
    numero: 55,
    complemento: "loja1",
    cep: "00000-00",
    bairro: ObjectId("60bfb7f8ccac9226bc71849a"),
    cidade: ObjectId("60bfb7d8ccac9226bc718499"),
    estado: ObjectId("60bfb7b0ccac9226bc718498"),
    pais: ObjectId("60bfb757ccac9226bc718497")
}
db.endereco.insert(endereco)
db.endereco.find()

gerente = {
    nome: "paulao",
    cpf: "21321312332",
    estadoCivil: "rei delas",
    dataNascimento: "02/12/1890",
    codigoFuncionario: "232132",
    tipoGerente: "GC",
    endereco: ObjectId("60bfb83eccac9226bc71849b")
}
db.pessoas.insert(gerente)
db.pessoas.find()

cliente = {
    nome: "Robozao",
    cpf: "2132123423423",
    estadoCivil: "rei delas",
    dataNascimento: "02/12/1891",
    tipoGerente: "GC",
    endereco: ObjectId("60bfb83eccac9226bc71849b")
}
db.pessoas.insert(cliente)
db.pessoas.find()

clienta = {
    nome: "Robozona",
    cpf: "2132123423423",
    estadoCivil: "rainha deles",
    dataNascimento: "02/12/1891",
    tipoGerente: "GC",
    endereco: ObjectId("60bfb83eccac9226bc71849b")
}
db.pessoas.insert(clienta)
db.pessoas.find()

conta = {
    codigo: "0",
    tipoConta: "Conjunta",
    saldo: 0.0,
    gerente: [
        ObjectId("60bfb8a3ccac9226bc71849c")
    ],
    cliente: [
        ObjectId("60bfb90eccac9226bc71849e"),
        ObjectId("60bfb9baccac9226bc71849f")
    ]
}
db.conta.insert(conta)
db.conta.find()

conta2 = {
    codigo: "0",
    tipoConta: "PF",
    saldo: 0.0,
    gerente: [
        ObjectId("60bfb8a3ccac9226bc71849c")
    ],
    cliente: [
        ObjectId("60bfb90eccac9226bc71849e"),
    ]
}
db.conta.insert(conta2)
db.conta.find()

consolidacao = {
    saldoAtual: 1234.0,
    saque: 0.0,
    deposito: 0.0,
    numeroCorrentistas: 0.0,
}
db.consolidacao.insert(consolidacao)
db.consolidacao.find()

agencia = {
    codigo: "0001",
    nome: "Web",
    endereco: ObjectId("60bfb83eccac9226bc71849b"),
    consolidacao: [
        ObjectId("60bfc170ccac9226bc7184a8")
    ],
    conta: [
        ObjectId("60bfbfd7ccac9226bc7184a5"),
        ObjectId("60bfbfe8ccac9226bc7184a6")
    ]
}
db.agencia.insert(agencia)
db.agencia.find()


banco = {
    codigo: "011",
    nome: "Alfa",
    agencia: [
        ObjectId("60bfc19cccac9226bc7184a9")
    ]
}
db.banco.insert(banco)
db.banco.find()


