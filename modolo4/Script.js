//db.createCollection("banco")

bancoAlfa = {
    codigo: "001",
    nome: "Alfa",
    agencia: [
        {
            codigo: "0001",
            nome: "Web",
            endereco: {
                logradouro: "rua testando",
                numero: 55,
                complemento: "loja1",
                cep: "00000-00",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            },
            consolidacao: [
                {
                    saldoAtual: 0,
                    saque: 0,
                    deposito: 0,
                    numeroCorrentistas: 0
                }
            ],
            conta: [
                {
                    codigo: "0",
                    tipoConta: "PF",
                    saldo: 0.0,
                    gerente: [
                        {
                            nome: "paulao",
                            cpf: "000.000.000-00",
                            estadoCivil: "solteiro",
                            dataNascimento: "00/00/0000",
                            codigoFuncionario: "1323123",
                            tipoGente: "GC",
                            endereco: {
                                logradouro: "rua testando",
                                numero: 55,
                                complemento: "loja1",
                                cep: "00000-00",
                                bairro: "NA",
                                cidade: "NA",
                                estado: "NA",
                                pais: "Brasil"
                            }
                        }
                    ],
                    cliente: [
                        {
                            nome: "paulao",
                            cpf: "000.000.000-00",
                            estadoCivil: "solteiro",
                            dataNascimento: "00/00/0000",
                            codigoFuncionario: "1323123",
                            tipoGente: "GC",
                            endereco: {
                                logradouro: "rua testando",
                                numero: 55,
                                complemento: "loja1",
                                cep: "00000-00",
                                bairro: "NA",
                                cidade: "NA",
                                estado: "NA",
                                pais: "Brasil"
                            }
                        }
                    ],
                    movimentacoes: [
                        {
                            tipo: "credito",
                            valor: 1200.00
                        },
                        {
                            tipo: "credito",
                            valor: 1300.00
                        },
                        {
                            tipo: "credito",
                            valor: 1400.00
                        }
                    ]
                }
            ]
        },
        {
            codigo: "0002",
            nome: "California",
            endereco: {
                logradouro: "rua testando",
                numero: 55,
                complemento: "loja1",
                cep: "00000-00",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            },
            consolidacao: [
                {
                    saldoAtual: 0,
                    saque: 0,
                    deposito: 0,
                    numeroCorrentistas: 0
                }
            ],
            conta: [
                {
                    codigo: "0",
                    tipoConta: "PF",
                    saldo: 0.0,
                    gerente: [
                        {
                            nome: "paulao",
                            cpf: "000.000.000-00",
                            estadoCivil: "solteiro",
                            dataNascimento: "00/00/0000",
                            codigoFuncionario: "1323123",
                            tipoGente: "GC",
                            endereco: {
                                logradouro: "rua testando",
                                numero: 55,
                                complemento: "loja1",
                                cep: "00000-00",
                                bairro: "NA",
                                cidade: "NA",
                                estado: "NA",
                                pais: "Brasil"
                            }
                        }
                    ],
                    cliente: [
                        {
                            nome: "paulao",
                            cpf: "000.000.000-00",
                            estadoCivil: "solteiro",
                            dataNascimento: "00/00/0000",
                            codigoFuncionario: "1323123",
                            tipoGente: "GC",
                            endereco: {
                                logradouro: "rua testando",
                                numero: 55,
                                complemento: "loja1",
                                cep: "00000-00",
                                bairro: "NA",
                                cidade: "NA",
                                estado: "NA",
                                pais: "Brasil"
                            }
                        }
                    ],
                    movimentacoes: [
                        {
                            tipo: "credito",
                            valor: 1200.00
                        },
                        {
                            tipo: "credito",
                            valor: 1300.00
                        },
                        {
                            tipo: "credito",
                            valor: 1400.00
                        }
                    ]
                }
            ]
        }
    ]
}

//db.banco.insert(bancoAlfa)

bancoOmega = {
    codigo: "001",
    nome: "Alfa",
    agencia: [
        {
            codigo: "0001",
            nome: "Web",
            endereco: {
                logradouro: "rua testando",
                numero: 55,
                complemento: "loja1",
                cep: "00000-00",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            },
            consolidacao: [
                {
                    saldoAtual: 0,
                    saque: 0,
                    deposito: 0,
                    numeroCorrentistas: 0
                }
            ],
            conta: [
                {
                    codigo: "0",
                    tipoConta: "PF",
                    saldo: 0.0,
                    gerente: [
                        {
                            nome: "paulao",
                            cpf: "000.000.000-00",
                            estadoCivil: "solteiro",
                            dataNascimento: "00/00/0000",
                            codigoFuncionario: "1323123",
                            tipoGente: "GC",
                            endereco: {
                                logradouro: "rua testando",
                                numero: 55,
                                complemento: "loja1",
                                cep: "00000-00",
                                bairro: "NA",
                                cidade: "NA",
                                estado: "NA",
                                pais: "Brasil"
                            }
                        }
                    ],
                    cliente: [
                        {
                            nome: "paulao",
                            cpf: "000.000.000-00",
                            estadoCivil: "solteiro",
                            dataNascimento: "00/00/0000",
                            codigoFuncionario: "1323123",
                            tipoGente: "GC",
                            endereco: {
                                logradouro: "rua testando",
                                numero: 55,
                                complemento: "loja1",
                                cep: "00000-00",
                                bairro: "NA",
                                cidade: "NA",
                                estado: "NA",
                                pais: "Brasil"
                            }
                        }
                    ],
                    movimentacoes: [
                        {
                            tipo: "credito",
                            valor: 1200.00
                        },
                        {
                            tipo: "credito",
                            valor: 1300.00
                        },
                        {
                            tipo: "credito",
                            valor: 1400.00
                        }
                    ]
                }
            ]
        },
        {
            codigo: "0002",
            nome: "California",
            endereco: {
                logradouro: "rua testando",
                numero: 55,
                complemento: "loja1",
                cep: "00000-00",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            },
            consolidacao: [
                {
                    saldoAtual: 0,
                    saque: 0,
                    deposito: 0,
                    numeroCorrentistas: 0
                }
            ],
            conta: [
                {
                    codigo: "0",
                    tipoConta: "PF",
                    saldo: 0.0,
                    gerente: [
                        {
                            nome: "paulao",
                            cpf: "000.000.000-00",
                            estadoCivil: "solteiro",
                            dataNascimento: "00/00/0000",
                            codigoFuncionario: "1323123",
                            tipoGente: "GC",
                            endereco: {
                                logradouro: "rua testando",
                                numero: 55,
                                complemento: "loja1",
                                cep: "00000-00",
                                bairro: "NA",
                                cidade: "NA",
                                estado: "NA",
                                pais: "Brasil"
                            }
                        }
                    ],
                    cliente: [
                        {
                            nome: "paulao",
                            cpf: "000.000.000-00",
                            estadoCivil: "solteiro",
                            dataNascimento: "00/00/0000",
                            codigoFuncionario: "1323123",
                            tipoGente: "GC",
                            endereco: {
                                logradouro: "rua testando",
                                numero: 55,
                                complemento: "loja1",
                                cep: "00000-00",
                                bairro: "NA",
                                cidade: "NA",
                                estado: "NA",
                                pais: "Brasil"
                            }
                        }
                    ],
                    movimentacoes: [
                        {
                            tipo: "credito",
                            valor: 1200.00
                        },
                        {
                            tipo: "credito",
                            valor: 1300.00
                        },
                        {
                            tipo: "credito",
                            valor: 1400.00
                        }
                    ]
                }
            ]
        }
    ]
}
    
//db.banco.insert(bancoOmega)

bancoBeta = {
    codigo: "001",
    nome: "Alfa",
    agencia: [
        {
            codigo: "0001",
            nome: "Web",
            endereco: {
                logradouro: "rua testando",
                numero: 55,
                complemento: "loja1",
                cep: "00000-00",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            },
            consolidacao: [
                {
                    saldoAtual: 0,
                    saque: 0,
                    deposito: 0,
                    numeroCorrentistas: 0
                }
            ],
            conta: [
                {
                    codigo: "0",
                    tipoConta: "PF",
                    saldo: 0.0,
                    gerente: [
                        {
                            nome: "paulao",
                            cpf: "000.000.000-00",
                            estadoCivil: "solteiro",
                            dataNascimento: "00/00/0000",
                            codigoFuncionario: "1323123",
                            tipoGente: "GC",
                            endereco: {
                                logradouro: "rua testando",
                                numero: 55,
                                complemento: "loja1",
                                cep: "00000-00",
                                bairro: "NA",
                                cidade: "NA",
                                estado: "NA",
                                pais: "Brasil"
                            }
                        }
                    ],
                    cliente: [
                        {
                            nome: "paulao",
                            cpf: "000.000.000-00",
                            estadoCivil: "solteiro",
                            dataNascimento: "00/00/0000",
                            codigoFuncionario: "1323123",
                            tipoGente: "GC",
                            endereco: {
                                logradouro: "rua testando",
                                numero: 55,
                                complemento: "loja1",
                                cep: "00000-00",
                                bairro: "NA",
                                cidade: "NA",
                                estado: "NA",
                                pais: "Brasil"
                            }
                        }
                    ],
                    movimentacoes: [
                        {
                            tipo: "credito",
                            valor: 1200.00
                        },
                        {
                            tipo: "credito",
                            valor: 1300.00
                        },
                        {
                            tipo: "credito",
                            valor: 1400.00
                        }
                    ]
                }
            ]
        },
        {
            codigo: "0002",
            nome: "California",
            endereco: {
                logradouro: "rua testando",
                numero: 55,
                complemento: "loja1",
                cep: "00000-00",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            },
            consolidacao: [
                {
                    saldoAtual: 0,
                    saque: 0,
                    deposito: 0,
                    numeroCorrentistas: 0
                }
            ],
            conta: [
                {
                    codigo: "0",
                    tipoConta: "PF",
                    saldo: 0.0,
                    gerente: [
                        {
                            nome: "paulao",
                            cpf: "000.000.000-00",
                            estadoCivil: "solteiro",
                            dataNascimento: "00/00/0000",
                            codigoFuncionario: "1323123",
                            tipoGente: "GC",
                            endereco: {
                                logradouro: "rua testando",
                                numero: 55,
                                complemento: "loja1",
                                cep: "00000-00",
                                bairro: "NA",
                                cidade: "NA",
                                estado: "NA",
                                pais: "Brasil"
                            }
                        }
                    ],
                    cliente: [
                        {
                            nome: "paulao",
                            cpf: "000.000.000-00",
                            estadoCivil: "solteiro",
                            dataNascimento: "00/00/0000",
                            codigoFuncionario: "1323123",
                            tipoGente: "GC",
                            endereco: {
                                logradouro: "rua testando",
                                numero: 55,
                                complemento: "loja1",
                                cep: "00000-00",
                                bairro: "NA",
                                cidade: "NA",
                                estado: "NA",
                                pais: "Brasil"
                            }
                        }
                    ],
                    movimentacoes: [
                        {
                            tipo: "credito",
                            valor: 1200.00
                        },
                        {
                            tipo: "credito",
                            valor: 1300.00
                        },
                        {
                            tipo: "credito",
                            valor: 1400.00
                        }
                    ]
                }
            ]
        }
    ]
}

db.banco.find()

//db.banco.insert(bancoBeta)

//db.banco.find({nome: "Alfa"});
//db.banco.find({nome: { $regex: /lf/ }}); -> expressao regular, no caso buscaria algo q tem lf, por ex: Alfa
//db.banco.find({nome: { $regex: /f }}); -> todos q terminam com f
//db.banco.find({codigo: { $eq: "011" }}); ->equal
//db.banco.find({codigo: { $ne: "011" }}); ->notequal
//db.banco.find({codigo: { $gt: "011" }}); ->greater than, porem n vai funcionar pq o 011 é string e n numero
//db.banco.find({ "agencia.nome" : "Web" })
//db.banco.find({ "agencia.conta.movimentacoes.valor" : { $gt: 10 } })
//db.banco.find({ "agencia.conta.movimentacoes.valor" : { $lt: 10 } }) -> menor que,
//db.banco.find({ "agencia.conta.movimentacoes.valor" : { $gte: 10 } }) -> maior ou igual que,
//db.banco.find().sort({"agencia.conta.movimentacoes.valor": -1}); -> -1 = decrescente
//db.banco.find().sort({"agencia.conta.movimentacoes.valor": +1}); -> +1 = crescente
//db.banco.find({ "agencia.conta.movimentacoes.valor": { $in: { 10, 100 } } }); in 

//db.banco.update( { nome: "Alfa" }, { nome:"Alfa 2" } ) -> dessa forma, ele excluiria tudo do documento e so colocaria o nome de Alfa 2
//db.banco.update( { nome: "Alfa" }, { $set: { nome:"Alfa 2" } } ) -> muda o primeiro q ele achar, no campo nome vai alterar o nome.
//db.banco.update( { _id: ObjectId("60bf9d27cb5e2f2d0dc58382") }, { $set: { nome:"Alfa 2" } } ) -> procurar pelo id

// $unset
// $inc -> incremento
// $mul
// $rename -> vai alterar o nome do campo

//db.banco.deleteOne({ nome: "Alfa" });
