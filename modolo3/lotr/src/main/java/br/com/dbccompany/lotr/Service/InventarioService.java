package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventario;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosItem;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import br.com.dbccompany.lotr.Repository.Inventario_X_ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InventarioService {

    @Autowired
    private InventarioRepository repository;

    public List<InventarioDTO> trazerTodosOsItens() {
        return this.converterListaParaDTO(repository.findAll());
    }

    @Transactional( rollbackFor = Exception.class)
    public InventarioDTO salvar( InventarioEntity inventario ) throws ArgumentosInvalidosInventario{
        try {
            return new InventarioDTO(this.salvarEEditar(inventario));
        } catch (Exception e) {
            throw new ArgumentosInvalidosInventario();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public InventarioDTO editar( InventarioEntity inventario, Integer id ){
        inventario.setId(id);
        return new InventarioDTO( this.salvarEEditar( inventario ) );
    }

    private InventarioEntity salvarEEditar( InventarioEntity inventario ){
        return repository.save( inventario );
    }

    public InventarioDTO buscarPorId( Integer id ) throws InventarioNaoEncontrado{
        try {
            return new InventarioDTO(repository.findById(id).get());
        } catch (Exception e) {
            throw new InventarioNaoEncontrado();
        }
    }

    public InventarioDTO buscarPorPersonagem(Integer id_personagem){
        return new InventarioDTO( repository.findByPersonagem(id_personagem) );
    }

    public List<InventarioDTO> buscarTodosPorPersonagem(Integer id_personagem){
        return this.converterListaParaDTO( repository.findAllByPersonagem(id_personagem) );
    }

    public List<InventarioDTO> buscarTodosPorPersonagemIn(List<Integer> ids_personagem){
        if (!ids_personagem.isEmpty()) {
            return this.converterListaParaDTO( repository.findAllByPersonagemIn(ids_personagem) );
        }
        return null;
    }

    private List<InventarioDTO> converterListaParaDTO(List<InventarioEntity> inventario){
        List<InventarioDTO> listaDTO = new ArrayList<>();
        for(InventarioEntity inventarioAux : inventario){
            listaDTO.add( new InventarioDTO(inventarioAux) );
        }
        return listaDTO;
    }

}
