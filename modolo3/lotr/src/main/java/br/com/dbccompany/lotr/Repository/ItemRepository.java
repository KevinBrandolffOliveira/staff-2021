package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface ItemRepository extends CrudRepository<ItemEntity, Integer> {
    List<ItemEntity> findAll();
    Optional<ItemEntity> findById(Integer id);
    List<ItemEntity> findAllById(Integer id);
    List<ItemEntity> findAllByIdIn(List<Integer> ids);
    ItemEntity findByDescricao(String descricao);
    List<ItemEntity> findAllByDescricao(String descricao);
    List<ItemEntity> findAllByDescricaoIn(List<String> descricao);
}
