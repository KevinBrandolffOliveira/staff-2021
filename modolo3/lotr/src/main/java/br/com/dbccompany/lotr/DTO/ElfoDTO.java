package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemStatusEnum;

public class ElfoDTO extends PersonagemDTO {

    private Integer id;
    private String nome;
    private Double vida;
    private PersonagemStatusEnum status;
    private Double qtdDano = 0.0;
    private int experiencia = 0;
    private Integer qtdExperienciaPorAtaque = 1;
    private InventarioEntity inventario;

    public ElfoDTO() {
    }

    public ElfoDTO(PersonagemEntity elfo) {
        this.id = elfo.getId();
        this.nome = elfo.getNome();
        this.vida = elfo.getVida();
        this.status = elfo.getStatus();
        this.qtdDano = elfo.getQtdDano();
        this.experiencia = elfo.getExperiencia();
        this.qtdExperienciaPorAtaque = elfo.getQtdExperienciaPorAtaque();
        this.inventario = elfo.getInventario();
    }

    public ElfoEntity converter(){
        ElfoEntity elfo = new ElfoEntity(this.nome);
        elfo.setVida(this.vida);
        elfo.setStatus(this.status);
        elfo.setQtdDano(this.qtdDano);
        elfo.setExperiencia(this.experiencia);
        elfo.setQtdExperienciaPorAtaque(this.qtdExperienciaPorAtaque);
        elfo.setInventario(this.inventario);
        return elfo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public PersonagemStatusEnum getStatus() {
        return status;
    }

    public void setStatus(PersonagemStatusEnum status) {
        this.status = status;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public int getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public Integer getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(Integer qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
