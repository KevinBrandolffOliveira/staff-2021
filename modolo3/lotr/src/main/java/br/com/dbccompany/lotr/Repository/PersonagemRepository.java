package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonagemRepository<T extends PersonagemEntity> extends CrudRepository<T, Integer> {
    List<T> findAll();
    Optional<T> findById(Integer id);
    List<T> findAllById(Integer id); //faz sentido? ha mais de um personagem com o mesmo id?
    List<T> findAllByIdIn(List<Integer> ids);
    T findByNome(String name);
    List<T> findAllByNome(String name);
    List<T> findAllByNomeIn(List<String> names);
    T findByInventario(Integer id_inventario);
    List<T> findAllByInventario(Integer id_inventario);
    List<T> findAllByInventarioIn(List<Integer> ids_inventario);
    T findByExperiencia(Integer experiencia);
    List<T> findAllByExperiencia(Integer experiencia);
    List<T> findAllByExperienciaIn(List<Integer> experiencias);
    T findByQtdDano(Double dano);
    List<T> findAllByQtdDano(Double dano);
    List<T> findAllByQtdDanoIn(List<Double> danos);
    T findByQtdExperienciaPorAtaque(Integer qtd_xp_atk);
    List<T> findAllByQtdExperienciaPorAtaque(Integer qtd_xp_atk);
    List<T> findAllByQtdExperienciaPorAtaqueIn(List<Integer> qtds_xp_atk);
    T findByStatus(String status);
    List<T> findAllByStatus(String status);
    List<T> findAllByStatusIn(List<String> status);
    T findByVida(Double vida);
    List<T> findAllByVida(Double vida);
    List<T> findAllByVidaIn(List<Double> vidas);
}
