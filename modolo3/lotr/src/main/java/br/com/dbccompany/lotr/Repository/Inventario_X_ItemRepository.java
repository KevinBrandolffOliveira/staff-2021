package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.Inventario_X_itemid;
import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface Inventario_X_ItemRepository extends CrudRepository<Inventario_x_Item, Inventario_X_itemid> {
    List<Inventario_x_Item> findAll();
    Optional<Inventario_x_Item> findById(Inventario_X_itemid id); //faz sentido? id é chave composta, como faz?
    List<Inventario_x_Item> findAllByIdIn(List<Inventario_X_itemid> ids);
    Inventario_x_Item findByItem(Integer id_item);
    Inventario_x_Item findByInventario(Integer id_inventario);
    List<Inventario_x_Item> findAllByItem(Integer id_item);
    List<Inventario_x_Item> findAllByInventarioId(Integer id_inventario);
    List<Inventario_x_Item> findAllByItemIn(List<Integer> id_item);
    List<Inventario_x_Item> findAllByInventarioIn(List<Integer> id_inventario);
    Inventario_x_Item findByQuantidade(int quantidade);
    List<Inventario_x_Item> findAllByQuantidade(int quantidade);
    List<Inventario_x_Item> findAllByQuantidadeIn(List<Integer> quantidade);
}
