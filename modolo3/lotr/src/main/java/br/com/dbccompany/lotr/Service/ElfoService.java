package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Repository.ElfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ElfoService extends PersonagemService<ElfoRepository, ElfoEntity>{

    @Autowired
    protected ElfoRepository repository;

    public List<ElfoDTO> trazerTodosOsItensElfo() {
        return this.converterListaParaDTO( super.trazerTodosOsItens() );
    }

    public ElfoDTO salvarElfo( ElfoEntity elfo ){
        return new ElfoDTO( super.salvarEEditar(elfo) );
    }

    public ElfoDTO editarElfo( ElfoEntity elfo, int id ){
        return new ElfoDTO( super.editar(elfo, id));
    }

    public ElfoDTO buscarPorIdElfo( Integer id ){
        return new ElfoDTO( super.buscarPorId(id) );
    }

    public List<ElfoDTO> buscarTodosPorIdInElfo(List<Integer> ids){
        return this.converterListaParaDTO( super.buscarTodosPorIdIn(ids) );
    }

    public ElfoDTO buscarPorVidaElfo(Double vida){
        return new ElfoDTO( super.buscarPorVida(vida) );
    }
    public List<ElfoDTO> buscarTodosPorVidaElfo(Double vida){
        return this.converterListaParaDTO(super.buscarTodosPorVida(vida));
    }
    public List<ElfoDTO> buscarTodosPorVidaInElfo(List<Double> vidas){
        return this.converterListaParaDTO(super.buscarTodosPorVidaIn(vidas));
    }

    public ElfoDTO buscarPorExperienciaElfo(Integer experiencia){
        return new ElfoDTO( super.buscarPorExperiencia(experiencia) );
    }
    public List<ElfoDTO> buscarTodosPorExperienciaElfo(Integer experiencia){
        return this.converterListaParaDTO( super.buscarTodosPorExperiencia(experiencia) );
    }
    public List<ElfoDTO> buscarTodosPorExperienciaInElfo(List<Integer> experiencias){
        return this.converterListaParaDTO( super.buscarTodosPorExperienciaIn(experiencias) );
    }

    public ElfoDTO buscarPorNomeElfo(String nome){
        return new ElfoDTO( super.buscarPorNome(nome) );
    }
    public List<ElfoDTO> buscarTodosPorNomeElfo(String nome){
        return this.converterListaParaDTO( super.buscarTodosPorNome(nome) );
    }
    public List<ElfoDTO> buscarTodosPorNomeInElfo(List<String> nomes){
        return this.converterListaParaDTO( super.buscarTodosPorNomeIn(nomes) );
    }

    public ElfoDTO buscarPorQtdDanoElfo(Double qtdDano){
        return new ElfoDTO( super.buscarPorQtdDano(qtdDano) );
    }
    public List<ElfoDTO> buscarTodosPorQtdDanoElfo(Double qtdDano){
        return this.converterListaParaDTO( super.buscarTodosPorQtdDano(qtdDano) );
    }
    public List<ElfoDTO> buscarTodosPorQtdDanoInElfo(List<Double> qtdDanos){
        return this.converterListaParaDTO( super.buscarTodosPorQtdDanoIn(qtdDanos) );
    }

    public ElfoDTO buscarPorQtdXpAtkElfo(Integer qtdXpAtk){
        return new ElfoDTO( super.buscarPorQtdXpAtk(qtdXpAtk) );
    }
    public List<ElfoDTO> buscarTodosPorQtdXpAtkElfo(Integer qtdXpAtk){
        return this.converterListaParaDTO( super.buscarTodosPorQtdXpAtk(qtdXpAtk) );
    }
    public List<ElfoDTO> buscarTodosPorQtdXpAtkInElfo(List<Integer> qtdsXpAtk){
        return this.converterListaParaDTO( super.buscarTodosPorQtdXpAtkIn(qtdsXpAtk) );
    }

    public ElfoDTO buscarPorStatusElfo(String status){
        return new ElfoDTO( super.buscarPorStatus(status) );
    }
    public List<ElfoDTO> buscarTodosPorStatusElfo(String status){
        return this.converterListaParaDTO( buscarTodosPorStatus(status) );
    }
    public List<ElfoDTO> buscarTodosPorStatusInElfo(List<String> status){
        return this.converterListaParaDTO( super.buscarTodosPorStatusIn(status) );
    }

    public ElfoDTO buscarPorInventarioElfo(Integer idInventario){
        return new ElfoDTO( super.buscarPorInventario(idInventario) );
    }
    public List<ElfoDTO> buscarTodosPorInventarioElfo(Integer idInventario){
        return this.converterListaParaDTO( super.buscarTodosPorInventario(idInventario) );
    }
    public List<ElfoDTO> buscarTodosPorInventarioInElfo(List<Integer> idsInventarios){
        return this.converterListaParaDTO( super.buscarTodosPorInventarioIn(idsInventarios) );
    }

    private List<ElfoDTO> converterListaParaDTO(List<ElfoEntity> elfo){
        List<ElfoDTO> listaDTO = new ArrayList<>();
        for(ElfoEntity elfoAux : elfo){
            listaDTO.add( new ElfoDTO(elfoAux) );
        }
        return listaDTO;
    }

}
