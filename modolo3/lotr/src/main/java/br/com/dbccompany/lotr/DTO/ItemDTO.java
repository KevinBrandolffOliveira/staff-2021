package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.ItemEntity;

public class ItemDTO {

    private Integer id;
    private String descricao;

    public ItemDTO(){}

    public ItemDTO(ItemEntity item) {
        this.id = item.getId();
        this.descricao = item.getDescricao();
    }

    public ItemEntity converter() {
        ItemEntity item = new ItemEntity();
        item.setDescricao(this.descricao);
        return item;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
