package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventario;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/inventario" )
public class InventarioController {

    @Autowired
    private InventarioService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<InventarioDTO> trazerTodosInventarios(){
        return service.trazerTodosOsItens();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<Object> trazerInventarioEspecifico(@PathVariable Integer id ){
        try {
            return new ResponseEntity<>( service.buscarPorId(id), HttpStatus.ACCEPTED );
        }catch( InventarioNaoEncontrado e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public ResponseEntity<Object> salvarItem(@RequestBody InventarioDTO inventario ) {
        try {
            return new ResponseEntity<>( service.salvar(inventario.converter()), HttpStatus.ACCEPTED );
        }catch (ArgumentosInvalidosInventario e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public InventarioDTO editarItem( @RequestBody InventarioDTO inventario, @PathVariable Integer id ){
        return service.editar(inventario.converter(), id);
    }

    @GetMapping( value = "/personagem/{id_personagem}" )
    @ResponseBody
    public InventarioDTO trazerPorPersonagem(@PathVariable Integer id_personagem){
        return service.buscarPorPersonagem(id_personagem);
    }

    @GetMapping( value = "/personagem/{ids_personagem}" )
    @ResponseBody
    public List<InventarioDTO> trazerTodosPorPersonagens(@PathVariable List<Integer> ids_personagem){
        return service.buscarTodosPorPersonagemIn(ids_personagem);
    }   //ta certo?

}
