package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AnaoDTO.class, name = "anao"),
        @JsonSubTypes.Type(value = ElfoDTO.class, name = "elfo")})
public abstract class PersonagemDTO {

    public PersonagemDTO() {
    }

    public abstract PersonagemEntity converter();
}
