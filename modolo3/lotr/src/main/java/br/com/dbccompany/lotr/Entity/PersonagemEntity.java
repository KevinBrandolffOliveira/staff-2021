package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@DiscriminatorColumn( name = "tipo_personagem", length = 32, discriminatorType = DiscriminatorType.STRING )
public abstract class PersonagemEntity {

    @Id
    @SequenceGenerator( name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ")
    @GeneratedValue( generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( unique = true, nullable = false )
    protected String nome;

    @Column( nullable = true )
    protected int experiencia = 0;

    @Column ( nullable = true, precision = 4, scale = 2 )
    protected Double vida;

    @Column( nullable = true, precision = 4, scale = 2 )
    protected Double qtdDano = 0.0;

    @Column( nullable = true )
    protected int qtdExperienciaPorAtaque = 1;

    @Enumerated( EnumType.STRING )
    protected PersonagemStatusEnum status;

    @OneToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_inventario" )
    private InventarioEntity inventario;

    public PersonagemEntity() {
    }

    public PersonagemEntity(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public int getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(int qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public PersonagemStatusEnum getStatus() {
        return status;
    }

    public void setStatus(PersonagemStatusEnum status) {
        this.status = status;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }

}
