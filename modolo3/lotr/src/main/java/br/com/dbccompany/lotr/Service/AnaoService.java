package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnaoService extends PersonagemService<AnaoRepository, AnaoEntity>{

    @Autowired
    protected AnaoRepository repository;

    public List<AnaoDTO> trazerTodosOsItensAnao() {
        return this.converterListaParaDTO( super.trazerTodosOsItens() );
    }

    public AnaoDTO salvarAnao( AnaoEntity anao ){
        return new AnaoDTO( super.salvarEEditar(anao) );
    }

    public AnaoDTO editarAnao( AnaoEntity anao, int id ){
        return new AnaoDTO( super.editar(anao, id));
    }

    public AnaoDTO buscarPorIdAnao( Integer id ){
        return new AnaoDTO( super.buscarPorId(id) );
    }

    public List<AnaoDTO> buscarTodosPorIdInAnao(List<Integer> ids){
        return this.converterListaParaDTO( super.buscarTodosPorIdIn(ids) );
    }

    public AnaoDTO buscarPorVidaAnao(Double vida){
        return new AnaoDTO( super.buscarPorVida(vida) );
    }
    public List<AnaoDTO> buscarTodosPorVidaAnao(Double vida){
        return this.converterListaParaDTO(super.buscarTodosPorVida(vida));
    }
    public List<AnaoDTO> buscarTodosPorVidaInAnao(List<Double> vidas){
        return this.converterListaParaDTO(super.buscarTodosPorVidaIn(vidas));
    }

    public AnaoDTO buscarPorExperienciaAnao(Integer experiencia){
        return new AnaoDTO( super.buscarPorExperiencia(experiencia) );
    }
    public List<AnaoDTO> buscarTodosPorExperienciaAnao(Integer experiencia){
        return this.converterListaParaDTO( super.buscarTodosPorExperiencia(experiencia) );
    }
    public List<AnaoDTO> buscarTodosPorExperienciaInAnao(List<Integer> experiencias){
        return this.converterListaParaDTO( super.buscarTodosPorExperienciaIn(experiencias) );
    }

    public AnaoDTO buscarPorNomeAnao(String nome){
        return new AnaoDTO( super.buscarPorNome(nome) );
    }
    public List<AnaoDTO> buscarTodosPorNomeAnao(String nome){
        return this.converterListaParaDTO( super.buscarTodosPorNome(nome) );
    }
    public List<AnaoDTO> buscarTodosPorNomeInAnao(List<String> nomes){
        return this.converterListaParaDTO( super.buscarTodosPorNomeIn(nomes) );
    }

    public AnaoDTO buscarPorQtdDanoAnao(Double qtdDano){
        return new AnaoDTO( super.buscarPorQtdDano(qtdDano) );
    }
    public List<AnaoDTO> buscarTodosPorQtdDanoAnao(Double qtdDano){
        return this.converterListaParaDTO( super.buscarTodosPorQtdDano(qtdDano) );
    }
    public List<AnaoDTO> buscarTodosPorQtdDanoInAnao(List<Double> qtdDanos){
        return this.converterListaParaDTO( super.buscarTodosPorQtdDanoIn(qtdDanos) );
    }

    public AnaoDTO buscarPorQtdXpAtkAnao(Integer qtdXpAtk){
        return new AnaoDTO( super.buscarPorQtdXpAtk(qtdXpAtk) );
    }
    public List<AnaoDTO> buscarTodosPorQtdXpAtkAnao(Integer qtdXpAtk){
        return this.converterListaParaDTO( super.buscarTodosPorQtdXpAtk(qtdXpAtk) );
    }
    public List<AnaoDTO> buscarTodosPorQtdXpAtkInAnao(List<Integer> qtdsXpAtk){
        return this.converterListaParaDTO( super.buscarTodosPorQtdXpAtkIn(qtdsXpAtk) );
    }

    public AnaoDTO buscarPorStatusAnao(String status){
        return new AnaoDTO( super.buscarPorStatus(status) );
    }
    public List<AnaoDTO> buscarTodosPorStatusAnao(String status){
        return this.converterListaParaDTO( buscarTodosPorStatus(status) );
    }
    public List<AnaoDTO> buscarTodosPorStatusInAnao(List<String> status){
        return this.converterListaParaDTO( super.buscarTodosPorStatusIn(status) );
    }

    public AnaoDTO buscarPorInventarioAnao(Integer idInventario){
        return new AnaoDTO( super.buscarPorInventario(idInventario) );
    }
    public List<AnaoDTO> buscarTodosPorInventarioAnao(Integer idInventario){
        return this.converterListaParaDTO( super.buscarTodosPorInventario(idInventario) );
    }
    public List<AnaoDTO> buscarTodosPorInventarioInAnao(List<Integer> idsInventarios){
        return this.converterListaParaDTO( super.buscarTodosPorInventarioIn(idsInventarios) );
    }

    private List<AnaoDTO> converterListaParaDTO(List<AnaoEntity> anao){
        List<AnaoDTO> listaDTO = new ArrayList<>();
        for(AnaoEntity anaoAux : anao){
            listaDTO.add( new AnaoDTO(anaoAux) );
        }
        return listaDTO;
    }

}
