package br.com.dbccompany.lotr.Entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable //incorporado em outra classe
public class Inventario_X_itemid implements Serializable {

    private int id_inventario;
    private int id_item;

    public Inventario_X_itemid() {
    }

    public Inventario_X_itemid(int id_inventario, int id_item) {
        this.id_inventario = id_inventario;
        this.id_item = id_item;
    }

    public int getId_inventario() {
        return id_inventario;
    }

    public void setId_inventario(int id_inventario) {
        this.id_inventario = id_inventario;
    }

    public int getId_item() {
        return id_item;
    }

    public void setId_item(int id_item) {
        this.id_item = id_item;
    }
}
