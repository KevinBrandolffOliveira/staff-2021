package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.DTO.InventariosParaUnirDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_itemid;
import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import br.com.dbccompany.lotr.Repository.Inventario_X_ItemRepository;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Inventario_X_ItemService {

    @Autowired
    private Inventario_X_ItemRepository repository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private InventarioRepository inventarioRepository;

    public List<Inventario_X_ItemDTO> trazerTodosOsInventarioXItens() {
        return this.converterListaParaDTO(repository.findAll());
    }

    @Transactional( rollbackFor = Exception.class)
    public Inventario_X_ItemDTO salvar( Inventario_x_Item inventario_x_item ){
        return new Inventario_X_ItemDTO( this.salvarEEditar( inventario_x_item ) );
    }

    @Transactional( rollbackFor = Exception.class)
    public Inventario_X_ItemDTO editar( Inventario_x_Item inventario_x_item, Inventario_X_itemid id ){
        inventario_x_item.setId(id);
        return new Inventario_X_ItemDTO( this.salvarEEditar( inventario_x_item ) );
    }

    private Inventario_x_Item salvarEEditar( Inventario_x_Item inventario_x_item){
        ItemEntity item = itemRepository.findById(inventario_x_item.getId().getId_item()).get();
        InventarioEntity inventario = inventarioRepository.findById(inventario_x_item.getId().getId_inventario()).get();
        inventario_x_item.setInventario(inventario);
        inventario_x_item.setItem(item);
        return repository.save( inventario_x_item );
    }

    public Inventario_X_ItemDTO buscarPorId( Inventario_X_itemid id ){
        Optional<Inventario_x_Item> e = repository.findById(id);
        if(e.isPresent()) {
            return new Inventario_X_ItemDTO( repository.findById(id).get() );
        }
        return null;
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorIdIn(List<Inventario_X_itemid> ids){
        if(!ids.isEmpty()){
            return this.converterListaParaDTO( repository.findAllByIdIn(ids) );
        }
        return null;
    }

    public Inventario_X_ItemDTO findByInventario(Integer id_inventario){
        return new Inventario_X_ItemDTO( repository.findByInventario(id_inventario) );
    }
    public List<Inventario_X_ItemDTO> findAllByInventario(Integer id_inventario){
        return this.converterListaParaDTO( repository.findAllByInventarioId(id_inventario) );
    }
    public List<Inventario_X_ItemDTO> findAllByInventarioIn(List<Integer> ids_inventario){
        if(!ids_inventario.isEmpty()){
            return this.converterListaParaDTO( repository.findAllByInventarioIn(ids_inventario) );
        }
        return null;
    }

    public Inventario_X_ItemDTO findByItem(Integer id_item){
        return new Inventario_X_ItemDTO( repository.findByItem(id_item) );
    }
    public List<Inventario_X_ItemDTO> findAllByItem(Integer id_item){
        return this.converterListaParaDTO( repository.findAllByItem(id_item) );
    }
    public List<Inventario_X_ItemDTO> findAllByItemIn(List<Integer> ids_items){
        if(!ids_items.isEmpty()){
            return this.converterListaParaDTO( repository.findAllByItemIn(ids_items) );
        }
        return null;
    }

    public Inventario_X_ItemDTO findByQuantidade(Integer quantidade){
        return new Inventario_X_ItemDTO( repository.findByQuantidade(quantidade) );
    }
    public List<Inventario_X_ItemDTO> findAllByQuantidade(Integer quantidade){
        return this.converterListaParaDTO( repository.findAllByQuantidade(quantidade) );
    }
    public List<Inventario_X_ItemDTO> findAllByQuantidadeIn(List<Integer> quantidades){
        if(!quantidades.isEmpty()){
            return this.converterListaParaDTO( repository.findAllByQuantidadeIn(quantidades) );
        }
        return null;
    }

    private List<Inventario_X_ItemDTO> converterListaParaDTO(List<Inventario_x_Item> inventario_x_items){
        List<Inventario_X_ItemDTO> listaDTO = new ArrayList<>();
        for(Inventario_x_Item invItemAux : inventario_x_items){
            listaDTO.add( new Inventario_X_ItemDTO( invItemAux ) );
        }
        return listaDTO;
    }

    public List<Inventario_X_ItemDTO> unir(List<InventarioEntity> inventarios){
        List<Inventario_x_Item> inventarioXitem1 = repository.findAllByInventarioId( inventarios.get(0).getId() );
        List<Inventario_x_Item> inventarioXitem2 = repository.findAllByInventarioId( inventarios.get(1).getId() );

        InventarioEntity inventarioNovo = new InventarioEntity();
        inventarioRepository.save(inventarioNovo);

        for(Inventario_x_Item inventario1 : inventarioXitem1){
            this.criarESalvarInventarioXItem( inventarioNovo, inventario1 );
        }
        for(Inventario_x_Item inventario2 : inventarioXitem2){
            this.criarESalvarInventarioXItem( inventarioNovo, inventario2 );
        }

        return converterListaParaDTO( repository.findAllByInventarioId(inventarioNovo.getId()) );
    }

    public List<Inventario_X_ItemDTO> diferenciar(List<InventarioEntity> inventarios) {
        List<Inventario_x_Item> inventarioXitem1 = repository.findAllByInventarioId( inventarios.get(0).getId() );
        List<Inventario_x_Item> inventarioXitem2 = repository.findAllByInventarioId( inventarios.get(1).getId() );

        InventarioEntity inventarioNovo = new InventarioEntity();
        inventarioRepository.save(inventarioNovo);

        for (int i = 0; i < inventarioXitem1.size(); i++){
            for ( int j = 0; j < inventarioXitem2.size(); j++){
                if ( !inventarioXitem1.get(i).getItem().equals(inventarioXitem2.get(j).getItem()) ){
                    this.criarESalvarInventarioXItem(inventarioNovo, inventarioXitem1.get(i));
                };
            }
        }

        return converterListaParaDTO( repository.findAllByInventarioId(inventarioNovo.getId()) );
    }

    private void criarESalvarInventarioXItem( InventarioEntity inventarioNovo, Inventario_x_Item inventarioXitem1 ){
        Inventario_x_Item inventarioXitemNovo = new Inventario_x_Item();
        inventarioXitemNovo.setInventario(inventarioNovo);
        inventarioXitemNovo.setItem(inventarioXitem1.getItem());
        inventarioXitemNovo.setQuantidade(inventarioXitem1.getQuantidade());
        inventarioXitemNovo.setId(new Inventario_X_itemid( inventarioNovo.getId(), inventarioXitem1.getId().getId_item() ));

        repository.save(inventarioXitemNovo);
    }
}
