package br.com.dbccompany.lotr.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Inventario_x_Item {
    @EmbeddedId
    private Inventario_X_itemid id;
    private int quantidade;

    @ManyToOne( cascade = CascadeType.ALL )
    @MapsId("id_inventario")
    private InventarioEntity inventario;

    @JsonIgnore
    @ManyToOne( cascade = CascadeType.ALL ) //para o looping, mas q looping?
    @MapsId("id_item")
    private ItemEntity item;

    public Inventario_X_itemid getId() {
        return id;
    }

    public void setId(Inventario_X_itemid id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }

    public ItemEntity getItem() {
        return item;
    }

    public void setItem(ItemEntity item) {
        this.item = item;
    }
}
