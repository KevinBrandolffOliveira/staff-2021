package br.com.dbccompany.lotr.Entity;

public enum PersonagemStatusEnum {
    RECEM_CRIADO, SOFREU_DANO, NAO_SOFREU_DANO, MORTO;
}
