package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;

public class InventarioDTO {

    private Integer id;
    private PersonagemDTO personagem ;

    public InventarioDTO() {
    }

    public InventarioDTO(InventarioEntity inventario) {
        this.id = inventario.getId();

        if(inventario.getPersonagem() instanceof AnaoEntity ){
            this.personagem = new AnaoDTO((AnaoEntity) inventario.getPersonagem());
        }else if(inventario.getPersonagem() instanceof ElfoEntity ){
            this.personagem = new ElfoDTO((ElfoEntity) inventario.getPersonagem());
        }
    }

    public InventarioEntity converter(){
        InventarioEntity inventario = new InventarioEntity();
        inventario.setId(this.id);

        if(this.personagem != null){
        inventario.setPersonagem(this.personagem.converter());
        }

        return inventario;
    }

    public PersonagemDTO getPersonagem() {
        return personagem;
    }

    public void setPersonagem(PersonagemDTO personagem) {
        this.personagem = personagem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
