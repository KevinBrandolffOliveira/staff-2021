package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.DTO.InventariosParaUnirDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_itemid;
import br.com.dbccompany.lotr.Service.Inventario_X_ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/inventario_x_item")
public class Inventario_X_ItemController {

    @Autowired
    private Inventario_X_ItemService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> trazerTodosItens(){
        return service.trazerTodosOsInventarioXItens();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Inventario_X_ItemDTO trazerInventario_x_ItemEspecifico( @PathVariable Inventario_X_itemid id ){
        return service.buscarPorId(id);
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public Inventario_X_ItemDTO salvarInventario_x_Item(@RequestBody Inventario_X_ItemDTO inventario_x_item ){
        return service.salvar( inventario_x_item.converter() );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Inventario_X_ItemDTO editarInventario_x_Item( @RequestBody Inventario_X_ItemDTO inventario_x_item, @PathVariable Inventario_X_itemid id ){
        return service.editar(inventario_x_item.converter(), id);
    }

    @GetMapping( value = "/inventario/{id}" )
    @ResponseBody
    public Inventario_X_ItemDTO buscarPorInventario(@PathVariable Integer id_inventario){
        return service.findByInventario(id_inventario);
    }

    @GetMapping( value = "/inventario/todos/{ids}" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> buscarTodosPorInventario(@PathVariable Integer id_inventario){
        return service.findAllByInventario(id_inventario);
    }

    @GetMapping( value = "/inventario/todos/" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> buscarTodosPorInventarioIn(@RequestBody List<Integer> id_inventario){
        return service.findAllByInventarioIn(id_inventario);
    }


    @GetMapping( value = "/item/{id}" )
    @ResponseBody
    public Inventario_X_ItemDTO buscarPorItem(@PathVariable Integer id_item){
        return service.findByItem(id_item);
    }

    @GetMapping( value = "/item/todos/{ids}" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> buscarTodosPorItem(@PathVariable Integer id_item){
        return service.findAllByItem(id_item);
    }

    @GetMapping( value = "/item/todos/" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> buscarTodosPorItemIn(@RequestBody List<Integer> id_tem){
        return service.findAllByItemIn(id_tem);
    }

    @GetMapping( value = "/quantidade/{id}" )
    @ResponseBody
    public Inventario_X_ItemDTO buscarPorQuantidade(@PathVariable Integer quantidade){
        return service.findByQuantidade(quantidade);
    }

    @GetMapping( value = "/quantidade/todos/{ids}" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> buscarTodosPorQuantidade(@PathVariable Integer quantidade){
        return service.findAllByQuantidade(quantidade);
    }

    @GetMapping( value = "/quantidade/todos/" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> buscarTodosPorQuantidadeIn(@RequestBody List<Integer> quantidade){
        return service.findAllByQuantidadeIn(quantidade);
    }

    @PostMapping( value = "/unir" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> unirInventarios(@RequestBody InventariosParaUnirDTO inventarios){
        return service.unir(inventarios.converter());
    }

    @PostMapping( value = "/diferenciar" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> diferenciarInventarios(@RequestBody InventariosParaUnirDTO inventarios){
        return service.diferenciar(inventarios.converter());
    }
}
