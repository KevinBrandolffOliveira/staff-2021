package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosItem;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    private Logger logger = LoggerFactory.getLogger(LotrApplication.class);

    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8081/salvar/logs";

    public List<ItemDTO> trazerTodosOsItens() {
        return this.converterListaParaDTO(repository.findAll());
    }

    @Transactional( rollbackFor = Exception.class)
    public ItemDTO salvar( ItemEntity item ) throws ArgumentosInvalidosItem{
        try {
            ItemEntity itemNovo = repository.save(item);
            return new ItemDTO(itemNovo);
        } catch (Exception e) {
            throw new ArgumentosInvalidosItem();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public ItemDTO editar( ItemEntity item, int id ){
        item.setId(id);
        ItemEntity itemNovo = this.salvarEEditar( item );
        return new ItemDTO(item);
    }

    private ItemEntity salvarEEditar( ItemEntity item ){
        return repository.save( item );
    }

    public ItemDTO buscarPorId( Integer id ) throws ItemNaoEncontrado{
        try {
            return new ItemDTO( repository.findById(id).get() );
        } catch (Exception e) {
            logger.error("Item teve problema ao buscar especifico: " + e.getMessage());
            Object seila = restTemplate.postForObject(url, e.getMessage(), Object.class);
            throw new ItemNaoEncontrado();
        }
    }

    public ItemDTO trazerPorDescricao(String descricao){
        ItemEntity novoItem =  repository.findByDescricao(descricao);
        return new ItemDTO(novoItem);
    }

    public List<ItemDTO> trazerTodosPorDescricao(String descricao){
        return this.converterListaParaDTO(repository.findAllByDescricao(descricao));
    }

    public List<ItemDTO> trazertTodosPorDescricaoIn(List<String> descricoes){
        if(!descricoes.isEmpty()) {
            return this.converterListaParaDTO(repository.findAllByDescricaoIn(descricoes));
        }
        return null;
    }

    private List<ItemDTO> converterListaParaDTO(List<ItemEntity> item){
        List<ItemDTO> listaDTO = new ArrayList<>();
        for(ItemEntity itemAux : item){
            listaDTO.add( new ItemDTO(itemAux) );
        }
        return listaDTO;
    }

}
