package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InventarioRepository extends CrudRepository<InventarioEntity, Integer> {
    List<InventarioEntity> findAll();
    Optional<InventarioEntity> findById(Integer id);
    List<InventarioEntity> findAllById(Integer id);
    List<InventarioEntity> findAllByIdIn(List<Integer> ids);
    InventarioEntity findByPersonagem(Integer id_personagem);
    List<InventarioEntity> findAllByPersonagem(Integer id_personagem);
    List<InventarioEntity> findAllByPersonagemIn(List<Integer> ids_personagem);
    //InventarioEntity findByInventarioItem(Inventario_x_Item inventarioItem);  -> faz sentido?
    //List<InventarioEntity> findAllByInventarioItemIn(List<Inventario_x_Item> inventarioItem); -> faz sentido?
}
