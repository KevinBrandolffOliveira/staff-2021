package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;

import java.util.ArrayList;
import java.util.List;

public class InventariosParaUnirDTO {

    private InventarioDTO inventario1;
    private InventarioDTO inventario2;

    public InventariosParaUnirDTO() {
    }

    public InventariosParaUnirDTO( InventarioEntity inventario1, InventarioEntity inventario2 ){
        this.inventario1.setId( inventario1.getId() );
        if ( inventario1.getPersonagem() != null ){
            if ( inventario1.getPersonagem() instanceof AnaoEntity){
                this.inventario1.setPersonagem( new AnaoDTO(inventario1.getPersonagem()) );
            }else {
                this.inventario1.setPersonagem( new ElfoDTO(inventario1.getPersonagem()) );
            }
        }
        this.inventario2.setId( inventario2.getId() );
        if ( inventario2.getPersonagem() != null ){
            if ( inventario2.getPersonagem() instanceof AnaoEntity){
                this.inventario2.setPersonagem( new AnaoDTO(inventario2.getPersonagem()) );
            }else {
                this.inventario2.setPersonagem( new ElfoDTO(inventario2.getPersonagem()) );
            }
        }
    }

    public List<InventarioEntity> converter(){
        List<InventarioEntity> novaList = new ArrayList<>();
        novaList.add( this.inventario1.converter() );
        novaList.add( this.inventario2.converter() );
        return novaList;
    }

    public InventarioDTO getInventario1() {
        return inventario1;
    }

    public void setInventario1(InventarioDTO inventario1) {
        this.inventario1 = inventario1;
    }

    public InventarioDTO getInventario2() {
        return inventario2;
    }

    public void setInventario2(InventarioDTO inventario2) {
        this.inventario2 = inventario2;
    }
}
