package br.com.dbccompany.lotr.Exception;

public class ItemNaoEncontrado extends ItemException{

    public ItemNaoEncontrado() {
        super("Esse item não existe!");
    }
}
