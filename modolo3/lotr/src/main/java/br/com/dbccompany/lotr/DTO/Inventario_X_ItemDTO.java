package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_itemid;
import br.com.dbccompany.lotr.Entity.Inventario_x_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;

public class Inventario_X_ItemDTO {

    private Integer quantidade;
    private Integer id_inventario;
    private Integer id_item;

    public Inventario_X_ItemDTO() {
    }

    public Inventario_X_ItemDTO(Inventario_x_Item inventario_x_item) {
        this.quantidade = inventario_x_item.getQuantidade();
        this.id_inventario = inventario_x_item.getInventario().getId();
        this.id_item = inventario_x_item.getItem().getId();
    }

    public Inventario_x_Item converter(){
        Inventario_x_Item inventario_x_item = new Inventario_x_Item();
        inventario_x_item.setId(new Inventario_X_itemid(id_inventario, id_item));
        inventario_x_item.setQuantidade(this.quantidade);
        InventarioEntity inventario = new InventarioEntity();
        ItemEntity item = new ItemEntity();
        //inventario.setId(this.id_inventario);
        //item.setId(this.id_item);
        inventario_x_item.setInventario(inventario);
        inventario_x_item.setItem(item);
        return inventario_x_item;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getId_inventario() {
        return id_inventario;
    }

    public void setId_inventario(Integer id_inventario) {
        this.id_inventario = id_inventario;
    }

    public Integer getId_item() {
        return id_item;
    }

    public void setId_item(Integer id_item) {
        this.id_item = id_item;
    }
}
