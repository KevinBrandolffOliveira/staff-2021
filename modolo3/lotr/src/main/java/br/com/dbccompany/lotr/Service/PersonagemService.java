package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public abstract class PersonagemService <R extends PersonagemRepository<E>, E extends PersonagemEntity> {

    @Autowired
    protected R repository;

    public List<E> trazerTodosOsItens() {
        return (List<E>) repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class)
    public E salvar( E e ){
        return this.salvarEEditar( e );
    }

    @Transactional( rollbackFor = Exception.class)
    public E editar( E e, int id ){
        e.setId(id);
        return this.salvarEEditar( e );
    }

    protected E salvarEEditar( E e){
        return repository.save( e );
    }

    public E buscarPorId( Integer id ){
        Optional<E> e = repository.findById(id);
        if(e.isPresent()) {
            return repository.findById(id).get();
        }
        return null;
    }

    public List<E> buscarTodosPorIdIn(List<Integer> ids){
        if(!ids.isEmpty()){
            return repository.findAllByIdIn(ids);
        }
        return null;
    }

    public E buscarPorVida(Double vida){
        return (E) repository.findByVida(vida);
    }
    public List<E> buscarTodosPorVida(Double vida){
        return (List<E>) repository.findAllByVida(vida);
    }
    public List<E> buscarTodosPorVidaIn(List<Double> vidas){
        if(!vidas.isEmpty()){
            return (List<E>) repository.findAllByVidaIn(vidas);
        }
        return null;
    }

    public E buscarPorExperiencia(Integer experiencia){
        return (E) repository.findByExperiencia(experiencia);
    }
    public List<E> buscarTodosPorExperiencia(Integer experiencia){
        return (List<E>) repository.findAllByExperiencia(experiencia);
    }
    public List<E> buscarTodosPorExperienciaIn(List<Integer> experiencias){
        if(!experiencias.isEmpty()){
            return (List<E>) repository.findAllByExperienciaIn(experiencias);
        }
        return null;
    }

    public E buscarPorNome(String nome){
        return (E) repository.findByNome(nome);
    }
    public List<E> buscarTodosPorNome(String nome){
        return (List<E>) repository.findAllByNome(nome);
    }
    public List<E> buscarTodosPorNomeIn(List<String> nomes){
        if(!nomes.isEmpty()){
            return (List<E>) repository.findAllByNomeIn(nomes);
        }
        return null;
    }

    public E buscarPorQtdDano(Double qtdDano){
        return (E) repository.findByQtdDano(qtdDano);
    }
    public List<E> buscarTodosPorQtdDano(Double qtdDano){
        return (List<E>) repository.findAllByQtdDano(qtdDano);
    }
    public List<E> buscarTodosPorQtdDanoIn(List<Double> qtdDanos){
        if(!qtdDanos.isEmpty()){
            return (List<E>) repository.findAllByQtdDanoIn(qtdDanos);
        }
        return null;
    }

    public E buscarPorQtdXpAtk(Integer qtdXpAtk){
        return (E) repository.findByQtdExperienciaPorAtaque(qtdXpAtk);
    }
    public List<E> buscarTodosPorQtdXpAtk(Integer qtdXpAtk){
        return (List<E>) repository.findAllByQtdExperienciaPorAtaque(qtdXpAtk);
    }
    public List<E> buscarTodosPorQtdXpAtkIn(List<Integer> qtdsXpAtk){
        if(!qtdsXpAtk.isEmpty()){
            return (List<E>) repository.findAllByQtdExperienciaPorAtaqueIn(qtdsXpAtk);
        }
        return null;
    }

    public E buscarPorStatus(String status){
        return (E) repository.findByStatus(status);
    }
    public List<E> buscarTodosPorStatus(String status){
        return (List<E>) repository.findAllByStatus(status);
    }
    public List<E> buscarTodosPorStatusIn(List<String> status){
        if(!status.isEmpty()){
            return (List<E>) repository.findAllByStatusIn(status);
        }
        return null;
    }

    public E buscarPorInventario(Integer idInventario){
        return (E) repository.findByInventario(idInventario);
    }
    public List<E> buscarTodosPorInventario(Integer idInventario){
        return (List<E>) repository.findAllByInventario(idInventario);
    }
    public List<E> buscarTodosPorInventarioIn(List<Integer> idsInventarios){
        if(!idsInventarios.isEmpty()){
            return (List<E>) repository.findAllByInventarioIn(idsInventarios);
        }
        return null;
    }

}
