package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosItem;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Service.ItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/item" )
public class ItemController {

    @Autowired
    private ItemService service;

    private Logger logger = LoggerFactory.getLogger(LotrApplication.class);

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ItemDTO> trazerTodosItens(){
        return service.trazerTodosOsItens();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<Object> trazerItemEspecifico(@PathVariable Integer id ){
        logger.info("comecou a buscar item especifico");
        try{
            ResponseEntity<Object> item = new ResponseEntity<>( service.buscarPorId(id), HttpStatus.ACCEPTED);
            logger.info("deu certo???");
            return item;
        }catch ( ItemNaoEncontrado e ){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public ResponseEntity<Object> salvarItem( @RequestBody ItemDTO item ){
        try {
            return new ResponseEntity<>( service.salvar(item.converter()), HttpStatus.ACCEPTED);
        }catch (ArgumentosInvalidosItem e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ItemDTO editarItem( @RequestBody ItemDTO item, @PathVariable Integer id ){
        return service.editar(item.converter(), id);
    }

    @GetMapping( value = "/descricao/{descricao}" )
    @ResponseBody
    public List<ItemDTO> trazerItensPorDescricao( @PathVariable String descricao ){
        return service.trazerTodosPorDescricao(descricao);
    }

}
