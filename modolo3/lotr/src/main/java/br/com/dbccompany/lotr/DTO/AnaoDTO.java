package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemStatusEnum;

public class AnaoDTO extends PersonagemDTO {

    private Integer id;
    private String nome;
    private Double vida;
    private PersonagemStatusEnum status;
    private Double qtdDano = 0.0;
    private int experiencia = 0;
    private Integer qtdExperienciaPorAtaque = 1;
    private InventarioEntity inventario;

    public AnaoDTO() {
    }

    public AnaoDTO(PersonagemEntity anao) {
        this.id = anao.getId();
        this.nome = anao.getNome();
        this.vida = anao.getVida();
        this.status = anao.getStatus();
        this.qtdDano = anao.getQtdDano();
        this.experiencia = anao.getExperiencia();
        this.qtdExperienciaPorAtaque = anao.getQtdExperienciaPorAtaque();
        this.inventario = anao.getInventario();
    }

    public AnaoEntity converter(){
        AnaoEntity anao = new AnaoEntity(this.nome);
        anao.setVida(this.vida);
        anao.setStatus(this.status);
        anao.setQtdDano(this.qtdDano);
        anao.setExperiencia(this.experiencia);
        anao.setQtdExperienciaPorAtaque(this.qtdExperienciaPorAtaque);
        anao.setInventario(this.inventario);
        return anao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public PersonagemStatusEnum getStatus() {
        return status;
    }

    public void setStatus(PersonagemStatusEnum status) {
        this.status = status;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public int getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public Integer getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(Integer qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
