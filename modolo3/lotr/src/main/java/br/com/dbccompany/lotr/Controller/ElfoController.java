package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Service.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/elfo" )
public class ElfoController {

    @Autowired
    private ElfoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosElfos(){
        return service.trazerTodosOsItensElfo();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ElfoDTO trazerElfosEspecifico( @PathVariable Integer id ){
        return service.buscarPorIdElfo(id);
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public ElfoDTO salvarElfo(@RequestBody ElfoDTO elfo ){
        return service.salvarElfo( elfo.converter() );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ElfoDTO editarElfo( @RequestBody ElfoDTO elfo, @PathVariable Integer id ){
        return service.editarElfo(elfo.converter(), id);
    }

    @GetMapping( value = "/todosElfosEspecificos" )
    @ResponseBody
    public List<ElfoDTO> trazerElfosEspecificos( @RequestBody List<Integer> ids ){
        return service.buscarTodosPorIdInElfo(ids);
    }

    @GetMapping( value = "/vida/{vida}" )
    @ResponseBody
    public ElfoDTO buscarPorVida(@PathVariable Double vida){
        return service.buscarPorVidaElfo(vida);
    }

    @GetMapping( value = "/vida/todos/{vida}" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorVida(@PathVariable Double vida){
        return service.buscarTodosPorVidaElfo(vida);
    }

    @GetMapping( value = "/vida/todos/" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorVidaIn(@RequestBody List<Double> vidas){
        return service.buscarTodosPorVidaInElfo(vidas);
    }

    @GetMapping( value = "/experiencia/{experiencia}" )
    @ResponseBody
    public ElfoDTO buscarPorExperiencia(@PathVariable Integer experiencia){
        return service.buscarPorExperienciaElfo(experiencia);
    }

    @GetMapping( value = "/experiencia/todos/{experiencia}" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorExperiencia(@PathVariable Integer experiencia){
        return service.buscarTodosPorExperienciaElfo(experiencia);
    }

    @GetMapping( value = "/experiencia/todos/" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorExperienciaIn(@RequestBody List<Integer> experiencia){
        return service.buscarTodosPorExperienciaInElfo(experiencia);
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public ElfoDTO buscarPorNome(@PathVariable String nome){
        return service.buscarPorNomeElfo(nome);
    }

    @GetMapping( value = "/nome/todos/{nome}" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorNome(@PathVariable String nome){
        return service.buscarTodosPorNomeElfo(nome);
    }

    @GetMapping( value = "/nome/todos/" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorNOmeIn(@RequestBody List<String> nomes){
        return service.buscarTodosPorNomeInElfo(nomes);
    }

    @GetMapping( value = "/qtddano/{dano}" )
    @ResponseBody
    public ElfoDTO buscarPorQtdDano(@PathVariable Double qtdDano){
        return service.buscarPorQtdDanoElfo(qtdDano);
    }

    @GetMapping( value = "/qtddano/todos/{qtddano}" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorQtdDano(@PathVariable Double qtdDano){
        return service.buscarTodosPorQtdDanoElfo(qtdDano);
    }

    @GetMapping( value = "/qtddano/todos/" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorQtdDanoIn(@RequestBody List<Double> qtdDano){
        return service.buscarTodosPorQtdDanoInElfo(qtdDano);
    }

    @GetMapping( value = "/qtdxpatk/{qtdxp}" )
    @ResponseBody
    public ElfoDTO buscarPorQtdXpAtk(@PathVariable Integer qtdXpAtk){
        return service.buscarPorQtdXpAtkElfo(qtdXpAtk);
    }

    @GetMapping( value = "/qtdxpatk/todos/{qtdxp}" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorQtdXpAtk(@PathVariable Integer qtdXpAtk){
        return service.buscarTodosPorQtdXpAtkElfo(qtdXpAtk);
    }

    @GetMapping( value = "/qtdxpatk/todos/" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorQtdXpAtkIn(@RequestBody List<Integer> qtdXpAtk){
        return service.buscarTodosPorQtdXpAtkInElfo(qtdXpAtk);
    }

    @GetMapping( value = "/status/{status}" )
    @ResponseBody
    public ElfoDTO buscarPorStatus(@PathVariable String status){
        return service.buscarPorStatusElfo(status);
    }

    @GetMapping( value = "/status/todos/{status}" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorStatus(@PathVariable String status){
        return service.buscarTodosPorStatusElfo(status);
    }

    @GetMapping( value = "/status/todos/" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorStatusIn(@RequestBody List<String> status){
        return service.buscarTodosPorStatusInElfo(status);
    }


    @GetMapping( value = "/inventario/{id}" )
    @ResponseBody
    public ElfoDTO buscarPorInventario(@PathVariable Integer id_inventario){
        return service.buscarPorInventarioElfo(id_inventario);
    }

    @GetMapping( value = "/inventario/todos/{ids}" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorInventario(@PathVariable Integer id_inventario){
        return service.buscarTodosPorInventarioElfo(id_inventario);
    }

    @GetMapping( value = "/inventario/todos/" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorInventarioIn(@RequestBody List<Integer> id_inventario){
        return service.buscarTodosPorInventarioInElfo(id_inventario);
    }

}
