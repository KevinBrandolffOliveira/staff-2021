package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Service.AnaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/anao" )
public class AnaoController {

    @Autowired
    private AnaoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosAnoes(){
        return service.trazerTodosOsItensAnao();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public AnaoDTO trazerAnaoEspecifico( @PathVariable Integer id ){
        return service.buscarPorIdAnao(id);
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public AnaoDTO salvarAnao(@RequestBody AnaoDTO anao ){
        return service.salvarAnao( anao.converter() );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public AnaoDTO editarAnao( @RequestBody AnaoDTO anao, @PathVariable Integer id ){
        return service.editarAnao(anao.converter(), id);
    }

    @GetMapping( value = "/todosAnoesEspecificos" )
    @ResponseBody
    public List<AnaoDTO> trazerAnoesEspecificos( @RequestBody List<Integer> ids ){
        return service.buscarTodosPorIdInAnao(ids);
    }

    @GetMapping( value = "/vida/{vida}" )
    @ResponseBody
    public AnaoDTO buscarPorVida(@PathVariable Double vida){
        return service.buscarPorVidaAnao(vida);
    }

    @GetMapping( value = "/vida/todos/{vida}" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorVida(@PathVariable Double vida){
        return service.buscarTodosPorVidaAnao(vida);
    }

    @GetMapping( value = "/vida/todos/" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorVidaIn(@RequestBody List<Double> vidas){
        return service.buscarTodosPorVidaInAnao(vidas);
    }

    @GetMapping( value = "/experiencia/{experiencia}" )
    @ResponseBody
    public AnaoDTO buscarPorExperiencia(@PathVariable Integer experiencia){
        return service.buscarPorExperienciaAnao(experiencia);
    }

    @GetMapping( value = "/experiencia/todos/{experiencia}" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorExperiencia(@PathVariable Integer experiencia){
        return service.buscarTodosPorExperienciaAnao(experiencia);
    }

    @GetMapping( value = "/experiencia/todos/" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorExperienciaIn(@RequestBody List<Integer> experiencia){
        return service.buscarTodosPorExperienciaInAnao(experiencia);
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public AnaoDTO buscarPorNome(@PathVariable String nome){
        return service.buscarPorNomeAnao(nome);
    }

    @GetMapping( value = "/nome/todos/{nome}" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorNome(@PathVariable String nome){
        return service.buscarTodosPorNomeAnao(nome);
    }

    @GetMapping( value = "/nome/todos/" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorNOmeIn(@RequestBody List<String> nomes){
        return service.buscarTodosPorNomeInAnao(nomes);
    }

    @GetMapping( value = "/qtddano/{dano}" )
    @ResponseBody
    public AnaoDTO buscarPorQtdDano(@PathVariable Double qtdDano){
        return service.buscarPorQtdDanoAnao(qtdDano);
    }

    @GetMapping( value = "/qtddano/todos/{qtddano}" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorQtdDano(@PathVariable Double qtdDano){
        return service.buscarTodosPorQtdDanoAnao(qtdDano);
    }

    @GetMapping( value = "/qtddano/todos/" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorQtdDanoIn(@RequestBody List<Double> qtdDano){
        return service.buscarTodosPorQtdDanoInAnao(qtdDano);
    }

    @GetMapping( value = "/qtdxpatk/{qtdxp}" )
    @ResponseBody
    public AnaoDTO buscarPorQtdXpAtk(@PathVariable Integer qtdXpAtk){
        return service.buscarPorQtdXpAtkAnao(qtdXpAtk);
    }

    @GetMapping( value = "/qtdxpatk/todos/{qtdxp}" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorQtdXpAtk(@PathVariable Integer qtdXpAtk){
        return service.buscarTodosPorQtdXpAtkAnao(qtdXpAtk);
    }

    @GetMapping( value = "/qtdxpatk/todos/" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorQtdXpAtkIn(@RequestBody List<Integer> qtdXpAtk){
        return service.buscarTodosPorQtdXpAtkInAnao(qtdXpAtk);
    }

    @GetMapping( value = "/status/{status}" )
    @ResponseBody
    public AnaoDTO buscarPorStatus(@PathVariable String status){
        return service.buscarPorStatusAnao(status);
    }

    @GetMapping( value = "/status/todos/{status}" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorStatus(@PathVariable String status){
        return service.buscarTodosPorStatusAnao(status);
    }

    @GetMapping( value = "/status/todos/" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorStatusIn(@RequestBody List<String> status){
        return service.buscarTodosPorStatusInAnao(status);
    }

    @GetMapping( value = "/inventario/{id}" )
    @ResponseBody
    public AnaoDTO buscarPorInventario(@PathVariable Integer id_inventario){
        return service.buscarPorInventarioAnao(id_inventario);
    }

    @GetMapping( value = "/inventario/todos/{ids}" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorInventario(@PathVariable Integer id_inventario){
        return service.buscarTodosPorInventarioAnao(id_inventario);
    }

    @GetMapping( value = "/inventario/todos/" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorInventarioIn(@RequestBody List<Integer> id_inventario){
        return service.buscarTodosPorInventarioInAnao(id_inventario);
    }

}
