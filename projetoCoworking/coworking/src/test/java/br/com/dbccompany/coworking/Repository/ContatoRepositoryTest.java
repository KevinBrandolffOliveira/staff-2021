package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.Tipo_ContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ContatoRepositoryTest {

    @Autowired
    private ContatoRepository repository;
    @Autowired
    private ClienteRepository repositoryCliente;
    @Autowired
    private Tipo_ContatoRepository repositoryTipoContato;

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @Test
    public void salvarContato() throws ParseException {
        ContatoEntity contato = new ContatoEntity();
        Tipo_ContatoEntity tipoContato = new Tipo_ContatoEntity();
        tipoContato.setNome("telefone");
        repositoryTipoContato.save(tipoContato);
        contato.setTipo_contato(tipoContato);
        contato.setValor("12345678");
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("PedraDura");
        cliente.setCpf("12345678912");
        cliente.setData_nascimento(sdf.parse("25/09/2000"));
        repositoryCliente.save(cliente);
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(contato);
        cliente.setContatos(contatos);
        contato.setCliente(cliente);
        repository.save(contato);
        assertEquals(contato, repository.findAllByValor("12345678").get(0));
        //----- adicionando apenas um contato a lista do cliente -----
        ContatoEntity contato2 = new ContatoEntity();
        contato2.setTipo_contato(tipoContato);
        contato2.setValor("11111111");
        contato2.setCliente(cliente);
        cliente.setUmContato(contato2);
        assertEquals(contato2, repository.findAllByValor("11111111").get(0));
    }

}
