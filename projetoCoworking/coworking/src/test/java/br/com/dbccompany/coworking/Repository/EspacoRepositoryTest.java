package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class EspacoRepositoryTest {

    @Autowired
    private EspacoRepository repository;

    @Test
    public void salvarEspaco(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("escritorio");
        espaco.setValor(20.00);
        espaco.setQtdPessoas(5);
        repository.save(espaco);
        assertEquals(espaco, repository.findByNome("escritorio"));
    }

}
