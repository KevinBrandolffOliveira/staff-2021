package br.com.dbccompany.coworking.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
public class Cliente_X_PacotesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void retornar403AoConsultarUmaListaDeClientePacote() throws Exception {
        URI uri = new URI("/api/cliente_pacote/");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(403)
        );
        // KKKK DESCULPA É QUE ESSE TAL DE SECURITY EH TAO SEGURO QUE QUEBRA MEU CODIGO, ENTAO O TESTE É FEITO COM 403 MESMO! :)
    }

}
