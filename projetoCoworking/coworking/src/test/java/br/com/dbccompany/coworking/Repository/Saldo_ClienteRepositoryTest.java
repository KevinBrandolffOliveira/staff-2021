package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Saldo_ClienteEntity;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class Saldo_ClienteRepositoryTest {

    @Autowired
    Saldo_ClienteRepository repository;
    @Autowired
    private ClienteRepository repositoryCliente;
    @Autowired
    private EspacoRepository repositoryEspaco;

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @Test
    public void salvarSaldoPagamento() throws ParseException {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("PedraDura");
        cliente.setCpf("12345678912");
        cliente.setData_nascimento(sdf.parse("25/09/2000"));
        repositoryCliente.save(cliente);
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("escritorio");
        espaco.setValor(20.00);
        espaco.setQtdPessoas(5);
        repositoryEspaco.save(espaco);
        Saldo_ClienteEntity saldoCliente = new Saldo_ClienteEntity();
        saldoCliente.setId(new Saldo_ClienteId(cliente.getId(), espaco.getId()));
        saldoCliente.setTipo_contratacao(Tipo_ContratacaoEnum.DIARIAS);
        saldoCliente.setQuantidade(3);
        saldoCliente.setVencimento(LocalDate.now());
        saldoCliente.setCliente(cliente);
        saldoCliente.setEspaco(espaco);
        repository.save(saldoCliente);
        assertEquals(saldoCliente.getCliente(), repository.findById(saldoCliente.getId()).get().getCliente());
    }

}
