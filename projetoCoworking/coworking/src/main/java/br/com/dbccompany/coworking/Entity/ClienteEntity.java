package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class ClienteEntity {

    @Id
    @SequenceGenerator( name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue( generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @Column(length = 11, nullable = false, unique = true)
    private String cpf;

    @Column(nullable = false)
    private Date data_nascimento;

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<ContatoEntity> contatos;

    @OneToMany(mappedBy = "cliente")
    private List<ContratacaoEntity> contratacoes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getData_nascimento() {
        return data_nascimento;
    }

    public void setData_nascimento(Date data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatoEntities) {
        this.contatos = contatoEntities;
    }

    public void setUmContato(ContatoEntity contato){
        this.contatos.add(contato);
    }

    public List<ContratacaoEntity> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacaoEntity> contratacoes) {
        this.contratacoes = contratacoes;
    }
}
