package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Espacos_X_PacotesEntity;

import java.util.ArrayList;
import java.util.List;

public class EspacoDTO {

    private Integer id;
    private String nome;
    private Integer qtdPessoas;
    private Double valor;
    private List<ContratacaoDTO> contratacoes;
    private List<Espacos_X_PacotesDTO> espacosPacotes;

    public EspacoDTO() {
    }

    public EspacoDTO(EspacoEntity espaco){
        this.id = espaco.getId();
        this.nome = espaco.getNome();
        this.qtdPessoas = espaco.getQtdPessoas();
        this.valor = espaco.getValor();
        this.contratacoes = converterListaParaDTOContratacao( espaco.getContratacoes() );
        this.espacosPacotes = converterListaParaDTOEspacoPacote( espaco.getEspacosPacote() );
    }

    public EspacoEntity converter(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setId(this.id);
        espaco.setNome(this.nome);
        espaco.setQtdPessoas(this.qtdPessoas);
        espaco.setValor(this.valor);
        espaco.setContratacoes(this.converterListaParaEntityContratacoes());
        espaco.setEspacosPacote(this.converterListaParaEntityEspacoPacote());
        return espaco;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<ContratacaoDTO> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacaoDTO> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<Espacos_X_PacotesDTO> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<Espacos_X_PacotesDTO> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    private List<ContratacaoDTO> converterListaParaDTOContratacao(List<ContratacaoEntity> lista){
        List<ContratacaoDTO> listaDTO = new ArrayList<>();
        if(!lista.isEmpty()){
            for(ContratacaoEntity list : lista){
                listaDTO.add(new ContratacaoDTO(list));
            }
        }
        return listaDTO;
    }

    private List<Espacos_X_PacotesDTO> converterListaParaDTOEspacoPacote(List<Espacos_X_PacotesEntity> lista){
        List<Espacos_X_PacotesDTO> listaDTO = new ArrayList<>();
        if(!lista.isEmpty()){
            for(Espacos_X_PacotesEntity list : lista){
                listaDTO.add(new Espacos_X_PacotesDTO(list));
            }
        }
        return listaDTO;
    }

    private List<ContratacaoEntity> converterListaParaEntityContratacoes(){
        List<ContratacaoEntity> list = new ArrayList<>();
        for(ContratacaoDTO listaDTO : contratacoes){
            list.add(listaDTO.converter());
        }
        return list;
    }

    private List<Espacos_X_PacotesEntity> converterListaParaEntityEspacoPacote(){
        List<Espacos_X_PacotesEntity> list = new ArrayList<>();
        for(Espacos_X_PacotesDTO listaDTO : espacosPacotes){
            list.add(listaDTO.converter());
        }
        return list;
    }

}
