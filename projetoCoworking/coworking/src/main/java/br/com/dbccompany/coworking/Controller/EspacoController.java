package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Service.ClienteService;
import br.com.dbccompany.coworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/espaco" )
public class EspacoController {

    @Autowired
    private EspacoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<EspacoDTO>> trazerTodosEspacos(){
        try{
            return new ResponseEntity<>( service.trazerTodos(), HttpStatus.OK );
        }catch (ObjetoNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<EspacoDTO> trazerPorId(@PathVariable int id){
        try{
            return new ResponseEntity<>( service.trazerPorId(id), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping( value = "/salvar ")
    @ResponseBody
    public ResponseEntity<EspacoDTO> salvarEspaco(@RequestBody EspacoDTO espaco){
        try{
            return new ResponseEntity<>( service.salvar(espaco.converter()), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<EspacoDTO> editarEspaco(@RequestBody EspacoDTO espaco, @PathVariable int idEspaco){
        try{
            return new ResponseEntity<>( service.editar(espaco.converter(), idEspaco), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/delete/{id}" )
    @ResponseBody
    public void deletarEspaco(@PathVariable int id) throws IdNaoEncontrado {
        service.delete(id);
        System.out.println("deletado");
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public ResponseEntity<EspacoDTO> trazerPorNome(@PathVariable String nome){
        try{
            return new ResponseEntity<>( service.trazerPorNome(nome), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

}
