package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContatoEntity;

public class ContatoDTO {

    private Integer id;
    private Tipo_ContatoDTO tipoContato;
    private String valor;
    private ClienteDTO cliente;

    public ContatoDTO() {
    }

    public ContatoDTO(ContatoEntity contato){
        this.id = contato.getId();
        this.tipoContato = new Tipo_ContatoDTO( contato.getTipo_contato() );
        this.valor = contato.getValor();
        this.cliente = new ClienteDTO( contato.getCliente() );
    }

    public ContatoEntity converter(){
        ContatoEntity contato = new ContatoEntity();
        contato.setId(this.id);
        contato.setTipo_contato(this.tipoContato.converter());
        contato.setValor(this.valor);
        contato.setCliente(this.cliente.converter());
        return contato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Tipo_ContatoDTO getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(Tipo_ContatoDTO tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }
}
