package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.PagamentosDTO;
import br.com.dbccompany.coworking.Entity.Clientes_X_PacotesEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.Cliente_X_PacotesRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.PagamentosRepository;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentosService {

    @Autowired
    private PagamentosRepository repository;

    @Autowired
    private Cliente_X_PacotesRepository repositoryClientePacote;

    @Autowired
    private ContratacaoRepository repositoryContratacao;

    @Transactional( rollbackFor = Exception.class)
    public PagamentosDTO salvar(PagamentosEntity pagamento ) throws DadosInvalidos {
        try{
            Clientes_X_PacotesEntity pacote = null;
            ContratacaoEntity contratacao = null;

            if( pagamento.getClientesPacotes() != null && pagamento.getContratacao() == null ) {
                pacote = repositoryClientePacote.save(pagamento.getClientesPacotes());
                pagamento.setClientesPacotes(pacote);
            }

            if( pagamento.getClientesPacotes() == null && pagamento.getContratacao() != null ) {
                contratacao = repositoryContratacao.save(pagamento.getContratacao());
                pagamento.setContratacao(contratacao);
            }

            if( pacote != null || contratacao != null ) {
                return new PagamentosDTO(repository.save(pagamento));
            }

            return null;
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public PagamentosDTO editar( PagamentosEntity pagamento, int id ) throws DadosInvalidos {
        try{
            pagamento.setId(id);
            return new PagamentosDTO(this.salvarEEditar( pagamento ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private PagamentosEntity salvarEEditar( PagamentosEntity pagamento ){
        return repository.save( pagamento );
    }

    public void delete(int id) throws IdNaoEncontrado {
        try{
            repository.deleteById(id);
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<PagamentosDTO> trazerTodos() throws ObjetoNaoEncontrado {
        try{
            return converterListaParaDTO( repository.findAll() );
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new ObjetoNaoEncontrado(e.getMessage());
        }
    }

    public PagamentosDTO trazerPorId(int id) throws IdNaoEncontrado {
        try{
            return new PagamentosDTO( repository.findById(id) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public PagamentosDTO trazerPorIdClientePacote(int id_ClientePacote) throws IdNaoEncontrado {
        try{
            return new PagamentosDTO( repository.findByClientesPacotesId(id_ClientePacote) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public PagamentosDTO trazerPorIdContratacao(int id_Contratacao) throws IdNaoEncontrado {
        try{
            return new PagamentosDTO( repository.findByContratacaoId(id_Contratacao) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    private List<PagamentosDTO> converterListaParaDTO(List<PagamentosEntity> pagamento){
        List<PagamentosDTO> listaDTO = new ArrayList<>();
        for(PagamentosEntity pagamentoAux : pagamento){
            listaDTO.add( new PagamentosDTO(pagamentoAux) );
        }
        return listaDTO;
    }
}
