package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.AcessoSaldoDTO;
import br.com.dbccompany.coworking.DTO.AcessosDTO;
import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Entity.Saldo_ClienteEntity;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.DataNaoEncontrada;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.AcessosRepository;
import br.com.dbccompany.coworking.Repository.Saldo_ClienteRepository;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AcessosService {

    @Autowired
    private AcessosRepository repository;
    @Autowired
    private Saldo_ClienteRepository repositorySaldoCliente;

    @Transactional( rollbackFor = Exception.class)
    public AcessosDTO salvar(AcessosEntity acesso ) throws DadosInvalidos {
        try{
            AcessosEntity acessoNovo = repository.save(acesso);
            return new AcessosDTO(acessoNovo);
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private AcessoSaldoDTO entrar (AcessosEntity acesso ){
        AcessoSaldoDTO response = new AcessoSaldoDTO();
        Integer saldo = acesso.getSaldoCliente().getQuantidade();
        if( saldo > 0 ){
            if( acesso.getData().isBefore(LocalDateTime.now()) ){
                repository.save( acesso );
                response.setSaldo( "Saldo: " + saldo.toString() + acesso.getSaldoCliente().getTipo_contratacao().toString() );
            }else{
                response.setSaldo( "Saldo Vencido" );
            }
        }else{
            response.setSaldo( "Saldo Insuficiente" );
        }
        return response;
    }

    private AcessoSaldoDTO sair ( AcessosEntity acesso ){
        List<AcessosEntity> acessos = repository.findAllBySaldoCliente(acesso.getSaldoCliente());
        AcessosEntity ultimoAcesso = acessos.get(acessos.size() - 1);
        Duration tempoGasto = Duration.between(ultimoAcesso.getData(), acesso.getData());
        Tipo_ContratacaoEnum tipoContratacao = acesso.getSaldoCliente().getTipo_contratacao();
        Integer saldoAnterior = acesso.getSaldoCliente().getQuantidade();
        Integer saldoFinal = saldoAnterior - convertTempoGasto( tipoContratacao,  tempoGasto);
        acesso.getSaldoCliente().setQuantidade( saldoFinal );
        repository.save(acesso);

        AcessoSaldoDTO response = new AcessoSaldoDTO();
        response.setSaldo( "Saldo: " + saldoFinal + acesso.getSaldoCliente().getTipo_contratacao().toString() );
        return response;
    }

    @Transactional( rollbackFor = Exception.class)
    public AcessosDTO editar( AcessosEntity acesso, int id ) throws DadosInvalidos {
        try{
            acesso.setId(id);
            return new AcessosDTO(this.salvarEEditar( acesso ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private AcessosEntity salvarEEditar( AcessosEntity acesso ){
        return repository.save( acesso );
    }

    public void delete(int id) throws IdNaoEncontrado {
        try{
            repository.deleteById(id);
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public AcessosDTO trazerPorId(int id) throws IdNaoEncontrado {
        try {
            return new AcessosDTO(repository.findById(id));
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<AcessosDTO> trazerTodos() throws ObjetoNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAll());
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new ObjetoNaoEncontrado(e.getMessage());
        }
    }

    public List<AcessosDTO> trazerTodosPorSaldoClienteId(Saldo_ClienteId id) throws IdNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAllBySaldoClienteId(id));
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<AcessosDTO> trazerTodosPorData(Date data) throws DataNaoEncontrada {
        try{
            return converterListaParaDTO(repository.findAllByData(data));
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new DataNaoEncontrada();
        }
    }

    public static Integer convertTempoGasto( Tipo_ContratacaoEnum tipoContratacao, Duration tempoGasto ){
        if(tipoContratacao == Tipo_ContratacaoEnum.MINUTOS){
            return (int) tempoGasto.toMinutes();
        }
        else if(tipoContratacao == Tipo_ContratacaoEnum.HORAS){
            return (int) tempoGasto.toHours();
        }
        else if(tipoContratacao == Tipo_ContratacaoEnum.TURNOS){
            return (int) tempoGasto.toHours() / 5;
        }
        else if (tipoContratacao == Tipo_ContratacaoEnum.DIARIAS){
            return (int) tempoGasto.toDays();
        }
        else if(tipoContratacao == Tipo_ContratacaoEnum.SEMANAS){
            return (int) tempoGasto.toDays() / 5;
        }
        else if (tipoContratacao == Tipo_ContratacaoEnum.MESES){
            return (int) tempoGasto.toDays() / 20;
        }
        return null;
    }

    private List<AcessosDTO> converterListaParaDTO(List<AcessosEntity> acesso){
        List<AcessosDTO> listaDTO = new ArrayList<>();
        for(AcessosEntity acessoAux : acesso){
            listaDTO.add( new AcessosDTO(acessoAux) );
        }
        return listaDTO;
    }

}
