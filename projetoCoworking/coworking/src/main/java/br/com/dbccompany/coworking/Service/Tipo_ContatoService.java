package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.Tipo_ContatoDTO;
import br.com.dbccompany.coworking.Entity.Tipo_ContatoEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Repository.Tipo_ContatoRepository;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Tipo_ContatoService {

    @Autowired
    private Tipo_ContatoRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Tipo_ContatoDTO salvar(Tipo_ContatoEntity tipoContato ) throws DadosInvalidos {
        try{
            Tipo_ContatoEntity tipoContatoNovo = repository.save(tipoContato);
            return new Tipo_ContatoDTO(tipoContatoNovo);
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public Tipo_ContatoDTO editar( Tipo_ContatoEntity tipoContato, int id ) throws DadosInvalidos {
        try{
            tipoContato.setId(id);
            return new Tipo_ContatoDTO(this.salvarEEditar( tipoContato ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private Tipo_ContatoEntity salvarEEditar( Tipo_ContatoEntity tipoContato ){
        return repository.save( tipoContato );
    }

    private Tipo_ContatoDTO encontrarPorId(int id) throws IdNaoEncontrado {
        try{
            return new Tipo_ContatoDTO(repository.findById(id));
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }
}
