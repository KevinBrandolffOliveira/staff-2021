package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.Clientes_X_PacotesDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Service.Cliente_X_PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Id;
import java.util.List;

@Controller
@RequestMapping( value = "api/cliente_pacote")
public class Cliente_X_PacotesController {

    @Autowired
    private Cliente_X_PacotesService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<Clientes_X_PacotesDTO>> trazerTodasClientePacote(){
        try{
            return new ResponseEntity<>( service.trazerTodos(), HttpStatus.OK );
        }catch (ObjetoNaoEncontrado e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<Clientes_X_PacotesDTO> trazerPorId(@PathVariable int id){
        try{
            return new ResponseEntity<>( service.trazerPorId(id), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping( value = "/salvar ")
    @ResponseBody
    public ResponseEntity<Clientes_X_PacotesDTO> salvarClientePacote(@RequestBody Clientes_X_PacotesDTO clientePacote){
        try{
            return new ResponseEntity<>( service.salvar(clientePacote.converter()), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Clientes_X_PacotesDTO> editarClientePacote(@RequestBody Clientes_X_PacotesDTO clientePacote, @PathVariable int idClientePacote){
        try{
            return new ResponseEntity<>( service.editar(clientePacote.converter(), idClientePacote), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/delete/{id}" )
    @ResponseBody
    public void deletarClientePacote(@PathVariable int id) throws IdNaoEncontrado {
        service.delete(id);
        System.out.println("deletado");
    }

    @GetMapping( value = "/cliente/{id_cliente}" )
    @ResponseBody
    public ResponseEntity<List<Clientes_X_PacotesDTO>> trazerTodosPorClienteId(@PathVariable int id_cliente){
        try{
            return new ResponseEntity<>( service.trazerTodosPorClienteId(id_cliente), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/cliente/{id_pacote}" )
    @ResponseBody
    public ResponseEntity<List<Clientes_X_PacotesDTO>> trazerTodosPorPacoteId(@PathVariable int id_pacote){
        try{
            return new ResponseEntity<>( service.trazerTodosPorPacoteId(id_pacote), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }
}
