package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClienteDTO {

    private Integer id;
    private String nome;
    private String cpf;
    private Date dataNascimento;
    private List<ContatoDTO> contatos;
    private List<ContratacaoDTO> contratacoes;

    public ClienteDTO() {
    }

    public ClienteDTO(ClienteEntity cliente){
        this.id = cliente.getId();
        this.nome = cliente.getNome();
        this.cpf = cliente.getCpf();
        this.dataNascimento = cliente.getData_nascimento();
        this.contatos = converterListaParaDTOContato(cliente.getContatos());
        this.contratacoes = converterListaParaDTOContratacao( cliente.getContratacoes() );
    }

    public ClienteEntity converter(){
        ClienteEntity cliente = new ClienteEntity();
        cliente.setId(this.id);
        cliente.setNome(this.nome);
        cliente.setCpf(this.cpf);
        cliente.setData_nascimento(this.dataNascimento);
        cliente.setContatos(converterListaParaEntityContato());
        cliente.setContratacoes(converterListaParaEntityContratacoes());
        return cliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoDTO> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoDTO> contatos) {
        this.contatos = contatos;
    }

    public List<ContratacaoDTO> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacaoDTO> contratacoes) {
        this.contratacoes = contratacoes;
    }

    private List<ContatoDTO> converterListaParaDTOContato(List<ContatoEntity> lista){
        List<ContatoDTO> listaDTO = new ArrayList<>();
        if(!lista.isEmpty()){
            for(ContatoEntity list : lista){
                listaDTO.add(new ContatoDTO(list));
            }
        }
        return listaDTO;
    }

    private List<ContratacaoDTO> converterListaParaDTOContratacao(List<ContratacaoEntity> lista){
        List<ContratacaoDTO> listaDTO = new ArrayList<>();
        if(!lista.isEmpty()){
            for(ContratacaoEntity list : lista){
                listaDTO.add(new ContratacaoDTO(list));
            }
        }
        return listaDTO;
    }

    private List<ContatoEntity> converterListaParaEntityContato(){
        List<ContatoEntity> list = new ArrayList<>();
        for(ContatoDTO listaDTO : contatos){
            list.add(listaDTO.converter());
        }
        return list;
    }

    private List<ContratacaoEntity> converterListaParaEntityContratacoes(){
        List<ContratacaoEntity> list = new ArrayList<>();
        for(ContratacaoDTO listaDTO : contratacoes){
            list.add(listaDTO.converter());
        }
        return list;
    }
}
