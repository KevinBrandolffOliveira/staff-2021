package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;

public class UsuarioDTO {

    private Integer id;

    private String nome;

    private String email;

    private String login;

    private String senha;

    public UsuarioDTO(UsuarioEntity user){
        this.nome = user.getNome();
        this.email = user.getEmail();
        this.login = user.getUsername();
        this.senha = user.getPassword();
    }

    public UsuarioDTO(){

    }

    public UsuarioEntity converter(){
        UsuarioEntity user = new UsuarioEntity();
        user.setNome(this.nome);
        user.setEmail(this.email);
        user.setLogin(this.login);
        user.setSenha(this.senha);
        return user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}