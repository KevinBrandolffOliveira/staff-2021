package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacotesEntity {

    @Id
    @SequenceGenerator( name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
    @GeneratedValue( generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column
    private Double valor;

    @OneToMany(mappedBy = "pacote")
    private List<Clientes_X_PacotesEntity> clientesPacotes;

    @OneToMany(mappedBy = "pacote")
    private List<Espacos_X_PacotesEntity> espacosPacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<Clientes_X_PacotesEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<Clientes_X_PacotesEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<Espacos_X_PacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<Espacos_X_PacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }
}
