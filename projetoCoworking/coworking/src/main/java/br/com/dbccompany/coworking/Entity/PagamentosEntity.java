package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Entity.Enum.Tipo_PagamentoEnum;

import javax.persistence.*;
import java.util.List;

@Entity
public class PagamentosEntity {

    @Id
    @SequenceGenerator( name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
    @GeneratedValue( generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne()
    @JoinColumn(name = "id_cliente_pagamento")
    private Clientes_X_PacotesEntity clientesPacotes;

    @ManyToOne()
    @JoinColumn(name = "id_contratacao")
    private ContratacaoEntity contratacao;

    @Enumerated(EnumType.STRING)
    private Tipo_PagamentoEnum tipo_pagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Clientes_X_PacotesEntity getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(Clientes_X_PacotesEntity clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public Tipo_PagamentoEnum getTipo_pagamento() {
        return tipo_pagamento;
    }

    public void setTipo_pagamento(Tipo_PagamentoEnum tipo_pagamento) {
        this.tipo_pagamento = tipo_pagamento;
    }
}
