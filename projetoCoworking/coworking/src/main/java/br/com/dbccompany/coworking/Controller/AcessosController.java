package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.AcessosDTO;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.DataNaoEncontrada;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping( value = "api/acessos")
public class AcessosController {

    @Autowired
    private AcessosService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<AcessosDTO>> trazerTodasAcessos(){
        try{
            return new ResponseEntity<>( service.trazerTodos(), HttpStatus.OK );
        }catch (ObjetoNaoEncontrado e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<AcessosDTO> trazerPorId(@PathVariable int id){
        try{
            return new ResponseEntity<>( service.trazerPorId(id), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping( value = "/salvar ")
    @ResponseBody
    public ResponseEntity<AcessosDTO> salvarAcesso(@RequestBody AcessosDTO acesso){
        try{
            return new ResponseEntity<>( service.salvar(acesso.converter()), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<AcessosDTO> editarAcesso(@RequestBody AcessosDTO acesso, @PathVariable int idAcesso){
        try{
            return new ResponseEntity<>( service.editar(acesso.converter(), idAcesso), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping( value = "/delete/{id}" )
    @ResponseBody
    public void deletarAcesso(@PathVariable int id) throws IdNaoEncontrado {
        service.delete(id);
        System.out.println("deletado");
    }

    @GetMapping( value = "/data/{data}" )
    @ResponseBody
    public ResponseEntity<List<AcessosDTO>> trazerTodosPorData(@PathVariable Date data){
        try{
            return new ResponseEntity<>( service.trazerTodosPorData(data), HttpStatus.OK );
        }catch (DataNaoEncontrada e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping( value = "/saldo_cliente/{id}" )
    @ResponseBody
    public ResponseEntity<List<AcessosDTO>> trazerTodosPorSaldoClienteId(@PathVariable Saldo_ClienteId id){
        try{
            return new ResponseEntity<>( service.trazerTodosPorSaldoClienteId(id), HttpStatus.OK);
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }
}
