package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Entity.Saldo_ClienteEntity;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;

import java.time.LocalDate;
import java.util.Date;

public class Saldo_ClienteDTO {

    private Saldo_ClienteId id;
    private Integer id_cliente;
    private Integer id_espaco;
    private Tipo_ContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private LocalDate vencimento;

    public Saldo_ClienteDTO() {
    }

    public Saldo_ClienteDTO(Saldo_ClienteEntity saldoCliente){
        this.id = saldoCliente.getId();
        this.id_cliente = saldoCliente.getId().getId_cliente();
        this.id_espaco = saldoCliente.getId().getId_espaco();
        this.tipoContratacao = saldoCliente.getTipo_contratacao();
        this.quantidade = saldoCliente.getQuantidade();
        this.vencimento = saldoCliente.getVencimento();
    }

    public Saldo_ClienteEntity converter(){
        Saldo_ClienteEntity saldoCliente = new Saldo_ClienteEntity();
        saldoCliente.setId(new Saldo_ClienteId(id_cliente, id_espaco));
        saldoCliente.setTipo_contratacao(this.tipoContratacao);
        saldoCliente.setQuantidade(this.quantidade);
        saldoCliente.setVencimento(this.vencimento);
        return saldoCliente;
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Integer getId_espaco() {
        return id_espaco;
    }

    public void setId_espaco(Integer id_espaco) {
        this.id_espaco = id_espaco;
    }

    public Tipo_ContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_ContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Saldo_ClienteId getId() {
        return id;
    }

    public void setId(Saldo_ClienteId id) {
        this.id = id;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }
}
