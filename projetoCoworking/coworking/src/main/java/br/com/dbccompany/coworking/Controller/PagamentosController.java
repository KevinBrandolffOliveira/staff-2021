package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PagamentosDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/pagamentos")
public class PagamentosController {

    @Autowired
    private PagamentosService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<PagamentosDTO>> trazerTodosPacotes(){
        try{
            return new ResponseEntity<>( service.trazerTodos(), HttpStatus.OK );
        }catch (ObjetoNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<PagamentosDTO> trazerPorId(@PathVariable int id){
        try{
            return new ResponseEntity<>( service.trazerPorId(id), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping( value = "/salvar ")
    @ResponseBody
    public ResponseEntity<PagamentosDTO> salvarPagamento(@RequestBody PagamentosDTO pagamento){
        try{
            return new ResponseEntity<>( service.salvar(pagamento.converter()), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<PagamentosDTO> editarPagamento(@RequestBody PagamentosDTO pagamento, @PathVariable int idPagamento){
        try{
            return new ResponseEntity<>( service.editar(pagamento.converter(), idPagamento), HttpStatus.OK);
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/delete/{id}" )
    @ResponseBody
    public void deletarPagamento(@PathVariable int id) throws IdNaoEncontrado {
        service.delete(id);
        System.out.println("deletado");
    }

    @GetMapping( value = "/cliente_pacote/{id_cliente_pacote}" )
    @ResponseBody
    public ResponseEntity<PagamentosDTO> trazerPorClientePacoteId(@PathVariable int id_cliente_pacote){
        try{
            return new ResponseEntity<>( service.trazerPorIdClientePacote(id_cliente_pacote), HttpStatus.OK);
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/contratacao/{id_contratacao}" )
    @ResponseBody
    public ResponseEntity<PagamentosDTO> trazerPorContratacaoId(@PathVariable int id_contratacao){
        try{
            return new ResponseEntity<>( service.trazerPorIdContratacao(id_contratacao), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

}
