package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.LogDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

public class LogUtil {

    private static final String URL = "http://localhost:8081/api/logger/salvar";

    private static Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);
    private static RestTemplate restTemplate = new RestTemplate();

    public static void lanca400( Exception e ){
        String mensagemLogger = "Erro na execução";
        System.err.println( e.getMessage() );
        logger.error(mensagemLogger);
        LogDTO log = new LogDTO( "ERROR", e, mensagemLogger, "400" );
        restTemplate.postForObject(URL, log, Object.class);
    }

    public static void lanca404( Exception e ){
        String mensagemLogger = "Objeto não foi encontrado";
        System.err.println( e.getMessage() );
        logger.error(mensagemLogger);
        LogDTO log = new LogDTO( "ERROR", e, mensagemLogger, "404" );
        restTemplate.postForObject(URL, log, Object.class);
    }

}
