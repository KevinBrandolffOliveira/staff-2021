package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Clientes_X_PacotesEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;

import java.util.ArrayList;
import java.util.List;

public class Clientes_X_PacotesDTO {

    private Integer id;
    private ClienteDTO cliente;
    private PacotesDTO pacote;
    private Integer quantidade;
    private List<PagamentosDTO> pagamento;

    public Clientes_X_PacotesDTO() {
    }

    public Clientes_X_PacotesDTO(Clientes_X_PacotesEntity clientePacote){
        this.id = clientePacote.getId();
        this.cliente = new ClienteDTO( clientePacote.getCliente() );
        this.pacote = new PacotesDTO( clientePacote.getPacote() );
        this.quantidade = clientePacote.getQuantidade();
        this.pagamento = converterListaParaDTOPagamento(clientePacote.getPagamento());
    }

    public Clientes_X_PacotesEntity converter(){
        Clientes_X_PacotesEntity clientePacote = new Clientes_X_PacotesEntity();
        clientePacote.setId(this.id);
        clientePacote.setCliente(this.cliente.converter());
        clientePacote.setPacote(this.pacote.converter());
        clientePacote.setQuantidade(this.quantidade);
        clientePacote.setPagamento(converterListaParaEntityPagamento());
        return clientePacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public PacotesDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacotesDTO pacote) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public List<PagamentosDTO> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentosDTO> pagamento) {
        this.pagamento = pagamento;
    }

    private List<PagamentosDTO> converterListaParaDTOPagamento(List<PagamentosEntity> pagamentos){
        List<PagamentosDTO> listaDTO = new ArrayList<>();
        for(PagamentosEntity pagamentosAux : pagamentos){
            listaDTO.add( new PagamentosDTO(pagamentosAux) );
        }
        return listaDTO;
    }

    private List<PagamentosEntity> converterListaParaEntityPagamento(){
        List<PagamentosEntity> list = new ArrayList<>();
        for(PagamentosDTO listaDTO : pagamento){
            list.add(listaDTO.converter());
        }
        return list;
    }
}
