package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "user")
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public UsuarioDTO salvar (@RequestBody UsuarioDTO usuario){
        return service.salvar(usuario.converter());
    }
}