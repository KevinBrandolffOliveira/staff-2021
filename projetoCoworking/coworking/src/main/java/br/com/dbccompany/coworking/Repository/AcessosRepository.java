package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.Saldo_ClienteEntity;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface AcessosRepository extends CrudRepository<AcessosEntity, Integer> {

    List<AcessosEntity> findAll();
    AcessosEntity findById(int id);
    List<AcessosEntity> findAllByData(Date data);
    List<AcessosEntity> findAllBySaldoClienteId(Saldo_ClienteId id_saldoCliente);
    List<AcessosEntity> findAllBySaldoCliente(Saldo_ClienteEntity saldoCliente);

}
