package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
public class AcessosEntity{

    @Id
    @SequenceGenerator( name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue( generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne()
    @JoinColumns({
            @JoinColumn(name = "id_cliente", nullable = false),
            @JoinColumn(name = "id_espaco", nullable = false)
    })
    private Saldo_ClienteEntity saldoCliente;

    private Boolean is_Entrada;

    private LocalDateTime data;

    private Boolean is_Excecao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Saldo_ClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(Saldo_ClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getIs_Entrada() {
        return is_Entrada;
    }

    public void setIs_Entrada(Boolean is_Entrada) {
        this.is_Entrada = is_Entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Boolean getIs_Excecao() {
        return is_Excecao;
    }

    public void setIs_Excecao(Boolean is_Excecao) {
        this.is_Excecao = is_Excecao;
    }
}
