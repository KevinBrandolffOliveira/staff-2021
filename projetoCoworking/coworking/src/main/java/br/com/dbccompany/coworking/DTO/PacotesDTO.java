package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Clientes_X_PacotesEntity;
import br.com.dbccompany.coworking.Entity.Espacos_X_PacotesEntity;
import br.com.dbccompany.coworking.Entity.PacotesEntity;

import java.util.ArrayList;
import java.util.List;

public class PacotesDTO {

    private Integer id;
    private Double valor;
    private List<Clientes_X_PacotesDTO> clientePacotes;
    private List<Espacos_X_PacotesDTO> espacoPacotes;

    public PacotesDTO() {
    }

    public PacotesDTO(PacotesEntity pacote){
        this.id = pacote.getId();
        this.valor = pacote.getValor();
        this.clientePacotes = converterListaParaDTOClientePacote( pacote.getClientesPacotes() );
        this.espacoPacotes = converterListaParaDTOEspacoPacote( pacote.getEspacosPacotes() );
    }

    public PacotesEntity converter(){
        PacotesEntity pacote = new PacotesEntity();
        pacote.setId(this.id);
        pacote.setValor(this.valor);
        pacote.setClientesPacotes(this.converterListaParaEntityClientePacote());
        pacote.setEspacosPacotes(this.converterListaParaEntityEspacoPacote());
        return pacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<Clientes_X_PacotesDTO> getClientePacotes() {
        return clientePacotes;
    }

    public void setClientePacotes(List<Clientes_X_PacotesDTO> clientePacotes) {
        this.clientePacotes = clientePacotes;
    }

    public List<Espacos_X_PacotesDTO> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void setEspacoPacotes(List<Espacos_X_PacotesDTO> espacoPacotes) {
        this.espacoPacotes = espacoPacotes;
    }

    private List<Clientes_X_PacotesDTO> converterListaParaDTOClientePacote(List<Clientes_X_PacotesEntity> lista){
        List<Clientes_X_PacotesDTO> listaDTO = new ArrayList<>();
        if(!lista.isEmpty()){
            for(Clientes_X_PacotesEntity list : lista){
                listaDTO.add(new Clientes_X_PacotesDTO(list));
            }
        }
        return listaDTO;
    }

    private List<Espacos_X_PacotesDTO> converterListaParaDTOEspacoPacote(List<Espacos_X_PacotesEntity> lista){
        List<Espacos_X_PacotesDTO> listaDTO = new ArrayList<>();
        if(!lista.isEmpty()){
            for(Espacos_X_PacotesEntity list : lista){
                listaDTO.add(new Espacos_X_PacotesDTO(list));
            }
        }
        return listaDTO;
    }

    private List<Clientes_X_PacotesEntity> converterListaParaEntityClientePacote(){
        List<Clientes_X_PacotesEntity> list = new ArrayList<>();
        for(Clientes_X_PacotesDTO listaDTO : clientePacotes){
            list.add(listaDTO.converter());
        }
        return list;
    }

    private List<Espacos_X_PacotesEntity> converterListaParaEntityEspacoPacote(){
        List<Espacos_X_PacotesEntity> list = new ArrayList<>();
        for(Espacos_X_PacotesDTO listaDTO : espacoPacotes){
            list.add(listaDTO.converter());
        }
        return list;
    }

}
