package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.Espacos_X_PacotesDTO;
import br.com.dbccompany.coworking.Entity.Espacos_X_PacotesEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.Espacos_X_PacotesRepository;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class Espacos_X_PacotesService {

    @Autowired
    private Espacos_X_PacotesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Espacos_X_PacotesDTO salvar(Espacos_X_PacotesEntity espacoPacote ) throws DadosInvalidos {
        try{
            Espacos_X_PacotesEntity espacoPacoteNovo = repository.save(espacoPacote);
            return new Espacos_X_PacotesDTO(espacoPacoteNovo);
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public Espacos_X_PacotesDTO editar( Espacos_X_PacotesEntity espacoPacote, int id ) throws DadosInvalidos {
        try{
            espacoPacote.setId(id);
            return new Espacos_X_PacotesDTO(this.salvarEEditar( espacoPacote ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private Espacos_X_PacotesEntity salvarEEditar( Espacos_X_PacotesEntity espacoPacote ){
        return repository.save( espacoPacote );
    }

    public void delete(int id) throws IdNaoEncontrado {
        try{
            repository.deleteById(id);
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<Espacos_X_PacotesDTO> trazerTodos() throws ObjetoNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAll());
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new ObjetoNaoEncontrado(e.getMessage());
        }
    }

    public Espacos_X_PacotesDTO trazerPorId(int id) throws IdNaoEncontrado {
        try{
            return new Espacos_X_PacotesDTO( repository.findById(id) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<Espacos_X_PacotesDTO> trazerTodosPorEspacoId(int id_espaco) throws IdNaoEncontrado {
        try {
            return converterListaParaDTO( repository.findAllByEspacoId(id_espaco) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<Espacos_X_PacotesDTO> trazerTodosPorPacoteId(int id_pacote) throws IdNaoEncontrado {
        try{
            return converterListaParaDTO( repository.findAllByPacoteId(id_pacote) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    private List<Espacos_X_PacotesDTO> converterListaParaDTO(List<Espacos_X_PacotesEntity> espacoPacote){
        List<Espacos_X_PacotesDTO> listaDTO = new ArrayList<>();
        for(Espacos_X_PacotesEntity espacoPacoteAux : espacoPacote){
            listaDTO.add( new Espacos_X_PacotesDTO(espacoPacoteAux) );
        }
        return listaDTO;
    }
}
