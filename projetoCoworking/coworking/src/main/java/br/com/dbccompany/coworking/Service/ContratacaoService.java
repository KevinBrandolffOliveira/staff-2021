package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public ContratacaoDTO salvar(ContratacaoEntity contratacao ) throws DadosInvalidos {
        try{
            ContratacaoEntity contratacaoNovo = repository.save(contratacao);
            return new ContratacaoDTO(contratacaoNovo);
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public ContratacaoDTO editar( ContratacaoEntity contratacao, int id ) throws DadosInvalidos {
        try{
            contratacao.setId(id);
            return new ContratacaoDTO(this.salvarEEditar( contratacao ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private ContratacaoEntity salvarEEditar( ContratacaoEntity contratacao ){
        return repository.save( contratacao );
    }

    public void delete(int id) throws IdNaoEncontrado {
        try{
            repository.deleteById(id);
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<ContratacaoDTO> trazerTodos() throws ObjetoNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAll());
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new ObjetoNaoEncontrado(e.getMessage());
        }
    }

    public ContratacaoDTO trazerPorId(int id) throws IdNaoEncontrado {
        try{
            return new ContratacaoDTO( repository.findById(id) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<ContratacaoDTO> trazerTodosPorEspacoId(int id_espaco) throws IdNaoEncontrado {
        try{
            return converterListaParaDTO( repository.findAllByEspacoId(id_espaco) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<ContratacaoDTO> trazerTodosPorClienteId(int id_cliente) throws IdNaoEncontrado {
        try{
            return converterListaParaDTO( repository.findAllByClienteId(id_cliente) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    private List<ContratacaoDTO> converterListaParaDTO(List<ContratacaoEntity> contratacao){
        List<ContratacaoDTO> listaDTO = new ArrayList<>();
        for(ContratacaoEntity contratacaoAux : contratacao){
            listaDTO.add( new ContratacaoDTO(contratacaoAux) );
        }
        return listaDTO;
    }
}
