package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Id;
import java.util.List;

@Controller
@RequestMapping( value = "api/contratacao" )
public class ContratacaoController {

    @Autowired
    private ContratacaoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<ContratacaoDTO>> trazerTodasContratacoes(){
        try{
            return new ResponseEntity<>( service.trazerTodos(), HttpStatus.OK );
        }catch (ObjetoNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<ContratacaoDTO> trazerPorId(@PathVariable int id){
        try{
            return new ResponseEntity<>( service.trazerPorId(id), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping( value = "/salvar ")
    @ResponseBody
    public ResponseEntity<ContratacaoDTO> salvarContratacao(@RequestBody ContratacaoDTO contratacao){
        try{
            return new ResponseEntity<>( service.salvar(contratacao.converter()), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<ContratacaoDTO> editarContratacao(@RequestBody ContratacaoDTO contratacao, @PathVariable int idContratacao){
        try{
            return new ResponseEntity<>( service.editar(contratacao.converter(), idContratacao), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/delete/{id}" )
    @ResponseBody
    public void deletarContratacao(@PathVariable int id) throws IdNaoEncontrado {
        service.delete(id);
        System.out.println("deletado");
    }

    @GetMapping( value = "/espaco/{id_espaco}" )
    @ResponseBody
    public ResponseEntity<List<ContratacaoDTO>> trazerTodasContratacoesPorEspacoId(@PathVariable int id_espaco){
        try{
            return new ResponseEntity<>( service.trazerTodosPorEspacoId(id_espaco), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/cliente/{id_cliente}" )
    @ResponseBody
    public ResponseEntity<List<ContratacaoDTO>> trazerTodasContratacoesPorClienteId(@PathVariable int id_cliente){
        try{
            return new ResponseEntity<>( service.trazerTodosPorClienteId(id_cliente), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }
}
