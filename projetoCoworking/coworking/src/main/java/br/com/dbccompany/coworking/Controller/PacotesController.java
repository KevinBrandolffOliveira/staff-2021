package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.PacotesDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Service.ContatoService;
import br.com.dbccompany.coworking.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/pacotes" )
public class PacotesController {

    @Autowired
    private PacotesService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<PacotesDTO>> trazerTodosPacotes(){
        try{
            return new ResponseEntity<>( service.trazerTodos(), HttpStatus.OK );
        }catch (ObjetoNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<PacotesDTO> trazerPorId(@PathVariable int id){
        try{
            return new ResponseEntity<>( service.trazerPorId(id), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping( value = "/salvar ")
    @ResponseBody
    public ResponseEntity<PacotesDTO> salvarPacotes(@RequestBody PacotesDTO pacote){
        try{
            return new ResponseEntity<>( service.salvar(pacote.converter()), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<PacotesDTO> editarPacotes(@RequestBody PacotesDTO pacote, @PathVariable int idPacote){
        try{
            return new ResponseEntity<>( service.editar(pacote.converter(), idPacote), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/delete/{id}" )
    @ResponseBody
    public void deletarPacotes(@PathVariable int id) throws IdNaoEncontrado {
        service.delete(id);
        System.out.println("deletado");
    }

}
