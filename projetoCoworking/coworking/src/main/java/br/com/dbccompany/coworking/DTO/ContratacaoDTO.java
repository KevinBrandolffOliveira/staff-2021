package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;

import java.util.ArrayList;
import java.util.List;

public class ContratacaoDTO {

    private Integer id;
    private EspacoDTO espaco;
    private ClienteDTO cliente;
    private Tipo_ContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Double desconto;
    private Integer prazo;
    private List<PagamentosDTO> pagamento;

    public ContratacaoDTO() {
    }

    public ContratacaoDTO(ContratacaoEntity contratacao){
        this.id = contratacao.getId();
        this.espaco = new EspacoDTO( contratacao.getEspaco() );
        this.cliente = new ClienteDTO( contratacao.getCliente() );
        this.tipoContratacao = contratacao.getTipo_contratacao();
        this.quantidade = contratacao.getQuantidade();
        this.desconto = contratacao.getDesconto();
        this.prazo = contratacao.getPrazo();
        this.pagamento = converterListaParaDTOPagamento( contratacao.getPagamento() );
    }

    public ContratacaoEntity converter(){
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setId(this.id);
        contratacao.setEspaco(this.espaco.converter());
        contratacao.setCliente(this.cliente.converter());
        contratacao.setTipo_contratacao(this.tipoContratacao);
        contratacao.setQuantidade(this.quantidade);
        contratacao.setDesconto(this.desconto);
        contratacao.setPrazo(this.prazo);
        contratacao.setPagamento(converterListaParaEntityPagamento());
        return contratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public Tipo_ContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_ContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public List<PagamentosDTO> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentosDTO> pagamento) {
        this.pagamento = pagamento;
    }

    private List<PagamentosDTO> converterListaParaDTOPagamento(List<PagamentosEntity> pagamentos){
        List<PagamentosDTO> listaDTO = new ArrayList<>();
        for(PagamentosEntity pagamentosAux : pagamentos){
            listaDTO.add( new PagamentosDTO(pagamentosAux) );
        }
        return listaDTO;
    }

    private List<PagamentosEntity> converterListaParaEntityPagamento(){
        List<PagamentosEntity> list = new ArrayList<>();
        for(PagamentosDTO listaDTO : pagamento){
            list.add(listaDTO.converter());
        }
        return list;
    }

}
