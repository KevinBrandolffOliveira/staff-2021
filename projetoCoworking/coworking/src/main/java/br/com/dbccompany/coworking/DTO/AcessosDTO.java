package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.Saldo_ClienteEntity;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;

import java.time.LocalDateTime;

import java.util.Date;

public class AcessosDTO {

    private Integer id;
    private Integer id_cliente;
    private Integer id_espaco;
    private LocalDateTime data;
    private boolean is_entrada;
    private boolean is_excecao;
    private String aviso;

    public AcessosDTO() {
    }

    public AcessosDTO(AcessosEntity acesso){
        this.id = acesso.getId();
        this.id_cliente = acesso.getSaldoCliente().getId().getId_cliente();
        this.id_espaco = acesso.getSaldoCliente().getId().getId_espaco();
        this.data = acesso.getData();
        this.is_entrada = acesso.getIs_Entrada();
        this.is_excecao = acesso.getIs_Excecao();
    }

    public AcessosEntity converter(){
        AcessosEntity acesso = new AcessosEntity();
        acesso.setId(this.id);
        Saldo_ClienteEntity saldoCliente = new Saldo_ClienteEntity();
        saldoCliente.setId(new Saldo_ClienteId(this.id_cliente, this.id_espaco));
        acesso.setSaldoCliente(saldoCliente);
        acesso.setData(this.data);
        acesso.setIs_Entrada(this.is_entrada);
        acesso.setIs_Excecao(this.is_excecao);
        return acesso;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Integer getId_espaco() {
        return id_espaco;
    }

    public void setId_espaco(Integer id_espaco) {
        this.id_espaco = id_espaco;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public String getAviso() {
        return aviso;
    }

    public void setAviso(String aviso) {
        this.aviso = aviso;
    }

    public boolean isIs_entrada() {
        return is_entrada;
    }

    public void setIs_entrada(boolean is_entrada) {
        this.is_entrada = is_entrada;
    }

    public boolean isIs_excecao() {
        return is_excecao;
    }

    public void setIs_excecao(boolean is_excecao) {
        this.is_excecao = is_excecao;
    }
}
