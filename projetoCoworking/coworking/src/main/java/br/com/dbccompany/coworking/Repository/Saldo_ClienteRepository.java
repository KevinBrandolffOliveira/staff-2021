package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Saldo_ClienteEntity;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface Saldo_ClienteRepository extends CrudRepository<Saldo_ClienteEntity, Saldo_ClienteId> {

    List<Saldo_ClienteEntity> findAll();
    Optional<Saldo_ClienteEntity> findById(Saldo_ClienteId id);
    List<Saldo_ClienteEntity> findAllByVencimento(Date vencimento);

}
