package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Entity.Espacos_X_PacotesEntity;

public class Espacos_X_PacotesDTO {

    private Integer id;
    private EspacoDTO espaco;
    private PacotesDTO pacote;
    private Tipo_ContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Integer prazo;

    public Espacos_X_PacotesDTO() {
    }

    public Espacos_X_PacotesDTO(Espacos_X_PacotesEntity espacoPacote){
        this.id = espacoPacote.getId();
        this.espaco = new EspacoDTO( espacoPacote.getEspaco() );
        this.pacote = new PacotesDTO( espacoPacote.getPacote() );
        this.tipoContratacao = espacoPacote.getTipo_contratacao();
        this.quantidade = espacoPacote.getQuantidade();
        this.prazo = espacoPacote.getPrazo();
    }

    public Espacos_X_PacotesEntity converter(){
        Espacos_X_PacotesEntity espacoPacote = new Espacos_X_PacotesEntity();
        espacoPacote.setId(this.id);
        espacoPacote.setEspaco(this.espaco.converter());
        espacoPacote.setPacote(this.pacote.converter());
        espacoPacote.setTipo_contratacao(this.tipoContratacao);
        espacoPacote.setQuantidade(this.quantidade);
        espacoPacote.setPrazo(this.prazo);
        return espacoPacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public PacotesDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacotesDTO pacote) {
        this.pacote = pacote;
    }

    public Tipo_ContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_ContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

}
