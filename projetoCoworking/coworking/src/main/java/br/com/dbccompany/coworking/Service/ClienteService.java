package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.Tipo_ContatoEntity;
import br.com.dbccompany.coworking.Exception.*;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Repository.Tipo_ContatoRepository;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Autowired
    private Tipo_ContatoRepository repositoryTipoContato;

    @Autowired
    private ContatoRepository repositoryContato;

    @Transactional( rollbackFor = Exception.class)
    public ClienteDTO salvar(ClienteEntity cliente ) throws DadosInvalidos {
        try{
            if(contatosVerificados(cliente)) {
                ClienteEntity clienteNovo = repository.save(cliente);
                for (ContatoEntity contato : clienteNovo.getContatos()) {
                    contato.setCliente(clienteNovo);
                    repositoryContato.save(contato);
                }
                return new ClienteDTO(clienteNovo);
            }
            throw new Precisa2Contatos( "Precisa de no minimo um contato email e um celular" );
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    public Boolean contatosVerificados(ClienteEntity cliente){
        Integer emailVerificacao = 0;
        Integer foneVerificacao = 0;
        List<ContatoEntity> contatos = cliente.getContatos();
        Tipo_ContatoEntity celularTipo = repositoryTipoContato.findByNome("celular");
        Tipo_ContatoEntity emailTipo = repositoryTipoContato.findByNome("email");

        if( contatos == null || contatos.size() == 0 ) {
            return false;
        }

        for( ContatoEntity contato : contatos ){
            if(contato.getTipo_contato().getNome().equalsIgnoreCase(emailTipo.getNome()))
                emailVerificacao++;
            if(contato.getTipo_contato().getNome().equalsIgnoreCase(celularTipo.getNome()))
                foneVerificacao++;
        }

        return emailVerificacao >= 1 && foneVerificacao >= 1;
    }

    @Transactional( rollbackFor = Exception.class)
    public ClienteDTO editar( ClienteEntity cliente, int id ) throws DadosInvalidos {
        try{
            cliente.setId(id);
            return new ClienteDTO(this.salvarEEditar( cliente ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private ClienteEntity salvarEEditar( ClienteEntity cliente ){
        return repository.save( cliente );
    }

    public void delete(int id) throws IdNaoEncontrado {
        try{
            repository.deleteById(id);
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<ClienteDTO> trazerTodos() throws ObjetoNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAll());
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new ObjetoNaoEncontrado(e.getMessage());
        }
    }

    public ClienteDTO trazerPorId(int id) throws IdNaoEncontrado {
        try {
            return new ClienteDTO( repository.findById(id) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public ClienteDTO trazerPorCPF(String cpf) throws CPFNaoEncontrado {
        try{
            return new ClienteDTO( repository.findByCpf(cpf) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            System.err.println("cpf nao encontradooo");
            throw new CPFNaoEncontrado();
        }
    }

    public ClienteDTO trazerPorContato(ContatoEntity contato) throws DadosInvalidos {
        try{
            return new ClienteDTO( repository.findByContatos(contato) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private List<ClienteDTO> converterListaParaDTO(List<ClienteEntity> cliente){
        List<ClienteDTO> listaDTO = new ArrayList<>();
        for(ClienteEntity clienteAux : cliente){
            listaDTO.add( new ClienteDTO(clienteAux) );
        }
        return listaDTO;
    }

}
