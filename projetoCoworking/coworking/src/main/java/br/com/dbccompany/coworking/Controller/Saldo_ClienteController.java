package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.Saldo_ClienteDTO;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Service.Saldo_ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping( value = "api/saldo_cliente")
public class Saldo_ClienteController {

    @Autowired
    private Saldo_ClienteService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<Saldo_ClienteDTO>> trazerTodasSaldoCliente(){
        try{
            return new ResponseEntity<>( service.trazerTodos(), HttpStatus.OK );
        }catch (ObjetoNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<Saldo_ClienteDTO> trazerPorId(@PathVariable Saldo_ClienteId id){
        try{
            return new ResponseEntity<>( service.trazerPorId(id), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping( value = "/salvar ")
    @ResponseBody
    public ResponseEntity<Saldo_ClienteDTO> salvarSaldoCliente(@RequestBody Saldo_ClienteDTO saldoCliente){
        try{
            return new ResponseEntity<>( service.salvar(saldoCliente.converter()), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Saldo_ClienteDTO> editarSaldoCliente(@RequestBody Saldo_ClienteDTO saldoCliente, @PathVariable Saldo_ClienteId idSaldoCliente){
        try{
            return new ResponseEntity<>( service.editar(saldoCliente.converter(), idSaldoCliente), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/delete/{id}" )
    @ResponseBody
    public void deletarSaldoCliente(@PathVariable Saldo_ClienteId id) throws IdNaoEncontrado {
        service.delete(id);
        System.out.println("deletado");
    }

    @GetMapping( value = "/vencimento/{id}" )
    @ResponseBody
    public ResponseEntity<List<Saldo_ClienteDTO>> trazerTodosPorVencimento(@PathVariable Date vencimento){
        try{
            return new ResponseEntity<>( service.trazerTodosPorVencimento(vencimento), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }
}
