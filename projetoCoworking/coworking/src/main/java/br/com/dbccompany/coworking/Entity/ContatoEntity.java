package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class ContatoEntity {

    @Id
    @SequenceGenerator( name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue( generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_contato")
    private Tipo_ContatoEntity tipo_contato;

    @Column(nullable = false)
    private String valor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cliente")
    private ClienteEntity cliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Tipo_ContatoEntity getTipo_contato() {
        return tipo_contato;
    }

    public void setTipo_contato(Tipo_ContatoEntity tipo_contato) {
        this.tipo_contato = tipo_contato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }
}
