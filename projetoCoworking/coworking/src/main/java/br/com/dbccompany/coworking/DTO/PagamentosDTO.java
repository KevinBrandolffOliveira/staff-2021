package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Enum.Tipo_PagamentoEnum;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;

public class PagamentosDTO {

    private Integer id;
    private Clientes_X_PacotesDTO clientePacote;
    private ContratacaoDTO contratacao;
    private Tipo_PagamentoEnum tipoPagamento;

    public PagamentosDTO() {
    }

    public PagamentosDTO(PagamentosEntity pagamentos){
        this.id = pagamentos.getId();
        this.clientePacote = new Clientes_X_PacotesDTO( pagamentos.getClientesPacotes() );
        this.contratacao = new ContratacaoDTO( pagamentos.getContratacao() );
        this.tipoPagamento = pagamentos.getTipo_pagamento();
    }

    public PagamentosEntity converter(){
        PagamentosEntity pagamentos = new PagamentosEntity();
        pagamentos.setId(this.id);
        pagamentos.setClientesPacotes(this.clientePacote.converter());
        pagamentos.setContratacao(this.contratacao.converter());
        pagamentos.setTipo_pagamento(this.tipoPagamento);
        return pagamentos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Clientes_X_PacotesDTO getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(Clientes_X_PacotesDTO clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoDTO getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoDTO contratacao) {
        this.contratacao = contratacao;
    }

    public Tipo_PagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(Tipo_PagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

}
