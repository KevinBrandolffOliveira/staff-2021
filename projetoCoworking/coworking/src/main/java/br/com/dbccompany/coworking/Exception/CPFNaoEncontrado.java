package br.com.dbccompany.coworking.Exception;

public class CPFNaoEncontrado extends DadosInvalidos{

    public CPFNaoEncontrado(){
        super("cpf nao encontrado");
    }

}
