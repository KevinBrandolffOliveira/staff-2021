package br.com.dbccompany.coworking.Entity.ids;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Saldo_ClienteId implements Serializable {

    private Integer id_cliente;
    private Integer id_espaco;

    public Saldo_ClienteId(Integer id_cliente, Integer id_espaco) {
        this.id_cliente = id_cliente;
        this.id_espaco = id_espaco;
    }

    public Saldo_ClienteId() {
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Integer getId_espaco() {
        return id_espaco;
    }

    public void setId_espaco(Integer id_espaco) {
        this.id_espaco = id_espaco;
    }
}
