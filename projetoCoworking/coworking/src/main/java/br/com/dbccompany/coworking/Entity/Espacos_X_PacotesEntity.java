package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;

import javax.persistence.*;

@Entity
public class Espacos_X_PacotesEntity {

    @Id
    @SequenceGenerator( name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
    @GeneratedValue( generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private EspacoEntity espaco;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private PacotesEntity pacote;

    @Enumerated(EnumType.STRING)
    private Tipo_ContratacaoEnum tipo_contratacao;

    @Column(nullable = false)
    private Integer quantidade;

    @Column(nullable = false)
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }

    public Tipo_ContratacaoEnum getTipo_contratacao() {
        return tipo_contratacao;
    }

    public void setTipo_contratacao(Tipo_ContratacaoEnum tipo_contratacao) {
        this.tipo_contratacao = tipo_contratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
