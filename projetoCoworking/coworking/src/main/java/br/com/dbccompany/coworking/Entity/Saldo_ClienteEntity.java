package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
public class Saldo_ClienteEntity {

    @EmbeddedId
    private Saldo_ClienteId id;

    @Enumerated(EnumType.STRING)
    private Tipo_ContratacaoEnum tipo_contratacao;

    @Column(nullable = false)
    private Integer quantidade;

    @Column(nullable = false)
    private LocalDate vencimento;

    @ManyToOne
    @MapsId("id_cliente")
    private ClienteEntity cliente;

    @ManyToOne
    @MapsId("id_espaco")
    private EspacoEntity espaco;

    @OneToMany(mappedBy = "saldoCliente")
    private List<AcessosEntity> acessos;

    public Saldo_ClienteId getId() {
        return id;
    }

    public void setId(Saldo_ClienteId id) {
        this.id = id;
    }

    public Tipo_ContratacaoEnum getTipo_contratacao() {
        return tipo_contratacao;
    }

    public void setTipo_contratacao(Tipo_ContratacaoEnum tipo_contratacao) {
        this.tipo_contratacao = tipo_contratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public List<AcessosEntity> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<AcessosEntity> acessos) {
        this.acessos = acessos;
    }
}
