package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/contato" )
public class ContatoController {

    @Autowired
    private ContatoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<ContatoDTO>> trazerTodosContatos(){
        try{
            return new ResponseEntity<>( service.trazerTodos(), HttpStatus.OK );
        }catch (ObjetoNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<ContatoDTO> trazerPorId(@PathVariable int id){
        try{
            return new ResponseEntity<>( service.trazerPorId(id), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping( value = "/salvar ")
    @ResponseBody
    public ResponseEntity<ContatoDTO> salvarContato(@RequestBody ContatoDTO contato){
        try{
            return new ResponseEntity<>( service.salvar(contato.converter()), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<ContatoDTO> editarContato(@RequestBody ContatoDTO contato, @PathVariable int idContato){
        try {
            return new ResponseEntity<>( service.editar(contato.converter(), idContato), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/delete/{id}" )
    @ResponseBody
    public void deletarContato(@PathVariable int id) throws IdNaoEncontrado {
        service.delete(id);
        System.out.println("deletado");
    }

    @GetMapping( value = "/valor/{valor}" )
    @ResponseBody
    public ResponseEntity<List<ContatoDTO>> trazerPorValor(@RequestBody String valor){
        try{
            return new ResponseEntity<>( service.trazerPorValor(valor), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

}
