package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.Espacos_X_PacotesDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Service.Espacos_X_PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/espaco_pacote")
public class Espacos_X_PacotesController {

    @Autowired
    private Espacos_X_PacotesService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<Espacos_X_PacotesDTO>> trazerTodosEspacoPacote(){
        try{
            return new ResponseEntity<>( service.trazerTodos(), HttpStatus.OK );
        }catch (ObjetoNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<Espacos_X_PacotesDTO> trazerPorId(@PathVariable int id){
        try{
            return new ResponseEntity<>( service.trazerPorId(id), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping( value = "/salvar ")
    @ResponseBody
    public ResponseEntity<Espacos_X_PacotesDTO> salvarEspacoPacote(@RequestBody Espacos_X_PacotesDTO espacoPacote){
        try{
            return new ResponseEntity<>( service.salvar(espacoPacote.converter()), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Espacos_X_PacotesDTO> editarEspacoPacote(@RequestBody Espacos_X_PacotesDTO espacoPacote, @PathVariable int idEspacoPacote){
        try{
            return new ResponseEntity<>( service.editar(espacoPacote.converter(), idEspacoPacote), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/delete/{id}" )
    @ResponseBody
    public void deletarEspacoPacote(@PathVariable int id) throws IdNaoEncontrado {
        service.delete(id);
        System.out.println("deletado");
    }

    @GetMapping( value = "/espaco/{id_espaco}" )
    @ResponseBody
    public ResponseEntity<List<Espacos_X_PacotesDTO>> trazerTodosPorEspacoId(@PathVariable int id_espaco){
        try{
            return new ResponseEntity<>( service.trazerTodosPorEspacoId(id_espaco), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/pacote/{id_pacote}" )
    @ResponseBody
    public ResponseEntity<List<Espacos_X_PacotesDTO>> trazerTodosPorPacoteId(@PathVariable int id_pacote){
        try{
            return new ResponseEntity<>( service.trazerTodosPorPacoteId(id_pacote), HttpStatus.OK );
        }catch (IdNaoEncontrado e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

}
