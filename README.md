Repositorio de Kevin Brandolff projeto VemSer 2021.

PROJETO COWORKING:

-tests dos controllers esta usando como status o 403, pq né! esse tal de security quebra meu codigo.

-tive que comentar alguns tests dos repositorys para que eu conseguisse buildar o projeto gradlew para fazer o docker, individualmente eles estao funcionando, porem no build dao erro.

-os dois containers testados e funcionando, porem a conexao entre o container de logs e o container de mongo n ta bombando.

PROJETO COWORKING 2:

-no projetoCoworking que foi entregue no prazo, tive a brilhante ideia de deixar uma parte importante das regras de negócio por último, devido a isso, fiz com muita pressa e com ajuda dos colegas. Isso anda me tirando o sono, então fiz o projetoCoworking2, que dai estou refazendo a parte das regras de negocio nos services e ajeitando alguns detalhes, porém, sem ajuda.

Objetivos:
	- deixar os testes mais completos.
	- deixar todas as regras de negocios funcionando e depois refatorar com padroes clean code.

Status:
	- salvar um tipoContato - OK
	
	- salvar um cliente com 2 contatos - OK
	
	- salvar um espaco - OK
	
	- salvar uma contratacao com cliente e espaco - OK
	
	- salvar um pagamento com os respectivos dados - OK
	
	- salvar um acesso - OK
	
	- salvar acesso de saida e descontar saldo - OK
	
	- salvar um pacote - OK
	
	- salvar um espacoXpacote - OK
	
	- salvar um clienteXpacote - OK
	
	- salvar um pagamento clienteXpacote - OK
	
	- salvar acesso entreda e saida com saldo de clienteXpacote - OK

Obs.: 
	-adicionei dentro um arquivo da collection do postman, nele tem todas as requisicoes 	que fiz pelo postman no app do coworking2.
	
	-o coworking2 está sem a camada de security.


