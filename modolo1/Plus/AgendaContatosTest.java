
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest{
    
    @Test
    public void adicionando2ContatosNaAgenda() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarUmContato( new Contato("Locao", 90284567) );
        agenda.adicionarUmContato( new Contato("Doidao", 32052376) );
        assertEquals( 2, agenda.getTamanho() );
    }
    
    @Test
    public void obterTelefoneAPartirDoNome() {
        AgendaContatos agenda = new AgendaContatos();
        Contato locao = new Contato("Locao",90284567);
        agenda.adicionarUmContato( locao );
        agenda.adicionarUmContato( new Contato("Doidao", 32052376) );
        assertEquals( (Integer)90284567 , agenda.obterTelefonePeloNome("Locao") );
    }
    
    @Test
    public void obterNomePeloTelefone() {
        AgendaContatos agenda = new AgendaContatos();
        Contato locao = new Contato("Locao",90284567);
        agenda.adicionarUmContato( locao );
        agenda.adicionarUmContato( new Contato("Doidao", 32052376) );
        assertEquals( "Locao" , agenda.obterContatoPeloTelefone(90284567) );
    }
    
}
