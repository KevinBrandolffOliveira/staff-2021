import java.util.*;

public class AgendaContatos{
    
    HashMap<String, Integer> agenda;
    
    public AgendaContatos() {
        this.agenda = new HashMap<>();
    }
    
    public void adicionarUmContato(Contato contato) {
        this.agenda.put(contato.getNome(), contato.getNumeroDeTelefone());
    }
    
    private boolean estaVazia() {
        return agenda.isEmpty();
    }
    
    public int getTamanho() {
        return agenda.size();
    }
    
    public Integer obterTelefonePeloNome(String nome) {
        if(estaVazia()) {
            return null;
        }
        
        if( agenda.containsKey(nome) ) {
            return agenda.get(nome);
        }else{
            return null;
        }
    }
    
    public String obterContatoPeloTelefone(Integer number) {
        if(estaVazia()) {
            return null;
        }
        
        if( agenda.containsValue(number) ) {
            return getKeyByValue(agenda, number);
        }else{
            return null;
        }
    }
    
    private String getKeyByValue(final Map<String, Integer> map, final Integer value) {
        return map.entrySet()
            .stream()
            .filter(e -> e.getValue().equals(value))
            .findFirst()
            .map(Map.Entry::getKey)
            .orElse(null);
    }
    
    public String csv() {
        
    }
}
