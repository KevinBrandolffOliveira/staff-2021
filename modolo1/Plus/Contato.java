public class Contato{
    
    private String nome;
    private Integer numeroDeTelefone;
    
    public Contato( String nome, Integer numeroDeTelefone ) {
        this.nome = nome;
        this.numeroDeTelefone = numeroDeTelefone;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNumeroDeTelefone( Integer numero ) {
        this.numeroDeTelefone = numero;
    }
    
    public Integer getNumeroDeTelefone() {
        return this.numeroDeTelefone;
    }
    
}
