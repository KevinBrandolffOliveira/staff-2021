import java.util.*;

public class Elfo extends Personagem implements ElfoAtaque{
    private int indiceFlecha;
    private static int qtdElfos;
    
    {
        this.indiceFlecha = 1;
        Elfo.qtdElfos = 0;
    }
    
    public Elfo( String nome ) {
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item( 1, "Arco" ));
        this.inventario.adicionar(new Item( 2, "Flecha" ));
        Elfo.qtdElfos++;
    }
    
    public static int getQuantidade() {
        return Elfo.qtdElfos;
    }
    
    public void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
    
    public Item getFlecha() {
        return this.inventario.obter(indiceFlecha);
    }
    
    protected int getQtdFlecha() {
        return this.getFlecha().getQuantidade();
    }
    
    private boolean podeAtirar() {
        return this.getQtdFlecha() > 0;
    }
    
    protected boolean podeAtacar(){
        return super.getStatus() != Status.MORTO && this.podeAtirar();
    }
    
    protected boolean isElfoNoturno( Elfo elfo ){
        return (elfo instanceof ElfoNoturno);
    }
    
    public void atirarFlecha( Anao anao ) {
        if( this.podeAtirar() ){
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            this.aumentarXP();
            super.sofrerDano();
            anao.sofrerDano();
        }
    }
    
    public void atirarMultiplasFlechas( int vezes, Anao anao ) {
        for( int i = 0; i < vezes; i++ ){
            this.atirarFlecha(anao);
        }
    }
    
    public void atacar(Anao anao) {
        this.atirarFlecha(anao);
    }
     
}