
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test{
    
    @Test
    public void testarODadoParaFicarEntre1E6(){
        for(int i = 0; i <1000; i++) {
            DadoD6 dado = new DadoD6();
            int teste = dado.sortear();
            boolean testeNumero = true;
            testeNumero = ( teste<0 || teste>6 ) ? false : true;
            assertTrue( testeNumero );
        }
    }
    
}
