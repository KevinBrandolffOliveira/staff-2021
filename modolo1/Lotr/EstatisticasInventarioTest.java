import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest {

    @Test
    public void calcularMediaInventarioVazio() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        double resultado = estatisticas.calcularMedia();
        assertTrue( Double.isNaN( resultado ) );
    }
    
    @Test
    public void calcularMediaUmItem() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 1, "Escudo de Madeira" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        double resultado = estatisticas.calcularMedia();
        assertEquals( 1, resultado, 1e-8 );
    }
    
    @Test
    public void calcularMediaQtdsIguais() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 3, "Espada de Madeira" ));
        inventario.adicionar(new Item( 3, "Escudo de Madeira" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        double resultado = estatisticas.calcularMedia();
        assertEquals( 3, resultado, 1e-8 ); //0.00000001
    }
    
    @Test
    public void calcularMediaQtdsDiferentes() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 2, "Espada de Madeira" ));
        inventario.adicionar(new Item( 4, "Escudo de Madeira" ));
        inventario.adicionar(new Item( 3, "Botas" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        double resultado = estatisticas.calcularMedia();
        assertEquals( 3, resultado, 1e-8 ); //0.00000001
    }
    
    @Test
    public void calcularMedianaUmItem() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 6, "Escudo de Madeira" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        double resultado = estatisticas.calcularMediana();
        assertEquals( 6, resultado, 1e-8 );
    }
    
    @Test
    public void calcularMedianaQtdsImpar() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 5, "Espada de Madeira" ));
        inventario.adicionar(new Item( 10, "Escudo de Madeira" ));
        inventario.adicionar(new Item( 20, "Botas de gelo" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        double resultado = estatisticas.calcularMediana();
        assertEquals( 10, resultado, 1e-8 ); //0.00000001
    }
    
    @Test
    public void calcularMedianaQtdsPares() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 5, "Espada de Madeira" ));
        inventario.adicionar(new Item( 10, "Escudo de Madeira" ));
        inventario.adicionar(new Item( 20, "Botas" ));
        inventario.adicionar(new Item( 20, "Adaga" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        double resultado = estatisticas.calcularMediana();
        assertEquals( 15, resultado, 1e-8 ); //0.00000001
    }
    
    @Test
    public void qtdItensAcimaDaMediaUmItem() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 5, "Escudo de Madeira" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals( 0, resultado );
    }
    
    @Test
    public void qtdItensAcimaDaMediaVariosItens() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 5, "Espada de Madeira" ));
        inventario.adicionar(new Item( 10, "Escudo de Madeira" ));
        inventario.adicionar(new Item( 20, "Botas de gelo" ));
        inventario.adicionar(new Item( 30, "Adaga" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals( 2, resultado );
    }
    
    @Test
    public void qtdItensAcimaDaMediaComItensIgualMedia() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 1, "Espada de Madeira" ));
        inventario.adicionar(new Item( 2, "Escudo de Madeira" ));
        inventario.adicionar(new Item( 3, "Botas" ));
        inventario.adicionar(new Item( 4, "Adaga" ));
        inventario.adicionar(new Item( 5, "Luva" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals( 2, resultado );
    }
}