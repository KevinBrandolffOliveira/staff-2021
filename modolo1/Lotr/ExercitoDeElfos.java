import java.util.*;

public class ExercitoDeElfos implements EstrategiasElfos{
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    private ArrayList<Elfo> elfos = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    
    public void alistar( Elfo elfo ) {
        boolean podeAlistar = TIPOS_PERMITIDOS.contains( elfo.getClass() );
        
        if( podeAlistar ) {
            elfos.add( elfo );
            
            ArrayList<Elfo> elfoDeUmStatus = porStatus.get( elfo.getStatus() );
            if( elfoDeUmStatus == null ) {
                elfoDeUmStatus = new ArrayList<>();
                porStatus.put( elfo.getStatus(), elfoDeUmStatus );
            }
            elfoDeUmStatus.add( elfo );
        }
    }
    
    public ArrayList<Elfo> buscar( Status status ) {
        return this.porStatus.get( status );
    }
    
    public ArrayList<Elfo> getElfos() {
        return this.elfos;
    }
    
    private boolean estaVivo(Elfo elfo) {
        return elfo.getStatus() != Status.MORTO;
    }
    
    public ArrayList<Elfo> getEstrategia_NoturnosPorUltimo(){
        ArrayList<Elfo> noturnosPorUltimo = new ArrayList<>();
        
        for(Elfo elfo : this.elfos) {
            if (this.estaVivo(elfo) && elfo instanceof ElfoVerde){
                noturnosPorUltimo.add(elfo);
            } 
        }
        
        for(Elfo elfo : this.elfos) {
            if (this.estaVivo(elfo) && elfo instanceof ElfoNoturno){
                noturnosPorUltimo.add(elfo);
            } 
        }
        
        return noturnosPorUltimo;
    }
    
    public ArrayList<Elfo> getEstrategia_AtaqueIntercalado(){
        ArrayList<Elfo> ataqueIntercalado = new ArrayList<>();
        ArrayList<Elfo> ataque = new ArrayList<>();
        
        int quantidadeVerde = 0, quantidadeNoturno = 0;
        
        for (Elfo elfo : this.elfos) {
            if (this.estaVivo(elfo) && elfo instanceof ElfoVerde){
                ataque.add(elfo);
                quantidadeVerde++;
            } 
        }
        
        for (Elfo elfo : this.elfos) {
            if (this.estaVivo(elfo) && elfo instanceof ElfoNoturno){
                ataque.add(elfo);
                quantidadeNoturno++;
            } 
        }
        
        int quantidadeARemover = Math.abs(quantidadeVerde - quantidadeNoturno);
        
        for (int i = 0; i < quantidadeARemover; i++) {
            ataque.remove(ataque.size()-1);
        }
        
        for (int i = 0; i < ataque.size()/2; i++) {
            ataqueIntercalado.add(ataque.get(i));
            ataqueIntercalado.add(ataque.get(i+ataque.size()/2));
        }
        
        return ataqueIntercalado;
    }
    
    public ArrayList<Elfo> getEstrategia_TercoNoturno(){
        ArrayList<Elfo> elfosOrdenados = new ArrayList<>();

        int qtdNoturnos = 0, qtdElfosVerdes = 0, qtdNoturnosPermitida, noturnosAMais;        

        for( Elfo atacante : this.elfos ){
            if ( atacante.podeAtacar() ){
                elfosOrdenados.add(atacante);
                if ( atacante instanceof ElfoNoturno ){
                    qtdNoturnos++;
                } else {
                    qtdElfosVerdes++;
                }       
            }    
        }
        
        qtdNoturnosPermitida = (qtdElfosVerdes++ / 3 );
        noturnosAMais = qtdNoturnos - qtdNoturnosPermitida;       
        elfosOrdenados = this.ordenarPorQtdFlecha( elfosOrdenados );        

        for ( int i = elfosOrdenados.size() - 1 ; noturnosAMais > 0 ; i-- ){
            if ( elfosOrdenados.get(i).isElfoNoturno ( elfosOrdenados.get(i) ) ) {
                elfosOrdenados.remove( i );
                noturnosAMais--;
            }    
        }
        
        return elfosOrdenados;
    }          
    
    protected ArrayList<Elfo> ordenarPorQtdFlecha ( ArrayList<Elfo> elfos) {     

        for( int i = 0; i < elfos.size(); i++ ) {
            for( int j = 0; j < elfos.size() - 1; j++ ) {
                int atual = elfos.get(j).getQtdFlecha();
                int proximo = elfos.get(j+1).getQtdFlecha();

                if( proximo > atual ) {
                    Elfo elfoTrocado = elfos.get( atual );
                    elfos.set(j, elfos.get( proximo ) );
                    elfos.set(j + 1, elfoTrocado);
                }
            }
        }

        return elfos;

    } 
    
    public void atacar(ArrayList<Elfo> estrategia, ArrayList<Anao> inimigoAnao){
        int numeroDeAnoes = inimigoAnao.size();
        for(int i = 0; i < estrategia.size(); i++) {
            if (i < numeroDeAnoes){
                estrategia.get(i).atacar(inimigoAnao.get(i));
            }else{
                estrategia.get(i).atacar(inimigoAnao.get(numeroDeAnoes - 1));
                numeroDeAnoes--;
            }
        }
    }
}