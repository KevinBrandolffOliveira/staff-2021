public class Personagem {
    protected String nome;
    protected int experiencia, qtdExperienciaPorAtaque;
    protected Inventario inventario;
    protected double vida;
    protected Status status;
    protected double qtdDano;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.inventario = new Inventario();
        this.qtdExperienciaPorAtaque = 1;
        this.experiencia = 0;
        this.qtdDano = 0.0;
    }
    
    public Personagem( String nome ) {
        this.nome = nome;
    }
    
    //get & set
    public String getNome() {
        return this.nome;
    }
    
    public void setNome( String nome ) {
        this.nome = nome;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public void ganharItem( Item item ) {
        this.inventario.adicionar( item );
    }
    
    public void perderItem( Item item ) {
        this.inventario.remover( item );
    }
    
    protected void aumentarXP() {
        this.experiencia += qtdExperienciaPorAtaque;
    }
    
    protected boolean podeSofrerDano( ) {
        return this.vida > 0;
    }
    
    protected Status validacaoStatus() {
        return this.vida == 0 ? Status.MORTO : Status.SOFREU_DANO;
    }
    
    protected Status validacaoStatus(Status status) {
        return this.vida == 0 ? Status.MORTO : status;
    }
    
    protected void sofrerDano() {
        if( this.podeSofrerDano() ) {
            this.vida -= this.vida >= this.qtdDano ? this.qtdDano : 0;
            this.status = this.validacaoStatus();
        }
        
    }
}