

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class ExercitoDeElfosTest{

    @Test
    public void podeAlistarElfoVerde() {
        Elfo elfoVerde = new ElfoVerde("Green Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoVerde);
        assertTrue(exercito.getElfos().contains(elfoVerde));
    }

    

    @Test
    public void podeAlistarElfoNoturno() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoNoturno);
        assertTrue(exercito.getElfos().contains(elfoNoturno));
    }

    

    @Test
    public void naoPodeAlistarElfoDeLuz() {
        Elfo elfoLuz = new ElfoDeLuz("Light Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoLuz);
        assertFalse(exercito.getElfos().contains(elfoLuz));
    }

    

    @Test
    public void buscarElfosRecemCriadosExistindo() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoNoturno);
        ArrayList<Elfo> esperado = new ArrayList<>(
        Arrays.asList(elfoNoturno)
        );
        assertEquals(esperado, exercito.buscar(Status.RECEM_CRIADO));

    }
        
    @Test
    public void verRetornoEstrategiaNoturnosPorUltimo() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(new ElfoNoturno( "LocaoPreto1" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto2" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde2" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto3" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto4" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto5" ));
        assertEquals( "LocaoVerde", exercito.getEstrategia_NoturnosPorUltimo().get(0).getNome());
        assertEquals( "LocaoVerde2", exercito.getEstrategia_NoturnosPorUltimo().get(1).getNome() );
        assertEquals( "LocaoPreto1", exercito.getEstrategia_NoturnosPorUltimo().get(2).getNome());
        assertEquals( "LocaoPreto2", exercito.getEstrategia_NoturnosPorUltimo().get(3).getNome());
        assertEquals( "LocaoPreto3", exercito.getEstrategia_NoturnosPorUltimo().get(4).getNome());
    }
    
    @Test
    public void verRetornoEstrategiaAtaqueIntercalado() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(new ElfoNoturno( "LocaoPreto1" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto2" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde2" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto3" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto4" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto5" ));
        assertEquals( "LocaoVerde", exercito.getEstrategia_AtaqueIntercalado().get(0).getNome());
        assertEquals( "LocaoPreto1", exercito.getEstrategia_AtaqueIntercalado().get(1).getNome() );
        assertEquals( "LocaoVerde2", exercito.getEstrategia_AtaqueIntercalado().get(2).getNome());
        assertEquals( "LocaoPreto2", exercito.getEstrategia_AtaqueIntercalado().get(3).getNome());
    }
    
    @Test 
    public void verSeEstrategiaAtaqueIntercaladoEstaCom50PorcentoDeCada() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(new ElfoNoturno( "LocaoPreto1" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto2" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde2" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto3" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto4" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto5" ));
        int tamanhoExercitoAtaqueIntercalado = exercito.getEstrategia_AtaqueIntercalado().size();
        assertEquals(4, tamanhoExercitoAtaqueIntercalado);
    }
    
    @Test
    public void exercitoAtacando(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(new ElfoNoturno( "LocaoPreto1" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto2" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde2" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto3" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto4" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto5" ));
        ArrayList<Anao> exercitoAnao = new ArrayList<>();
        exercitoAnao.add(new Anao("Anao1"));
        exercitoAnao.add(new Anao("Anao2"));
        exercitoAnao.add(new Anao("Anao3"));
        exercitoAnao.add(new Anao("Anao4"));
        exercito.atacar(exercito.getEstrategia_AtaqueIntercalado(), exercitoAnao);
        
        for (int i = 0; i < 4; i++) {
            assertEquals(100.00, exercitoAnao.get(i).getVida(), 0d);
        }
    }
    
    @Test
    public void exercitoAtacandoEstrategiaNoturnoPorUltimo(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(new ElfoNoturno( "LocaoPreto1" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto2" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde2" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto3" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto4" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto5" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto6" ));
        ArrayList<Anao> exercitoAnao = new ArrayList<>();
        exercitoAnao.add(new Anao("Anao1"));
        exercitoAnao.add(new Anao("Anao2"));
        exercitoAnao.add(new Anao("Anao3"));
        exercitoAnao.add(new Anao("Anao4"));
        exercito.atacar(exercito.getEstrategia_NoturnosPorUltimo(), exercitoAnao);
        
        for (int i = 0; i < 4; i++) {
            assertEquals(90.00, exercitoAnao.get(i).getVida(), 0d);
        }
    }
    
    @Test
    public void exercitoTercoNoturno() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(new ElfoNoturno( "LocaoPreto1" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto2" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde2" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde3" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde4" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde5" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde6" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde7" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde8" ));
        exercito.alistar(new ElfoVerde( "LocaoVerde9" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto3" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto4" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto5" ));
        exercito.alistar(new ElfoNoturno( "LocaoPreto6" ));
        assertEquals(12, exercito.getEstrategia_TercoNoturno().size());
    }
}
