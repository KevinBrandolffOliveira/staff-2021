import java.util.*;

public interface EstrategiasElfos{
    
    public ArrayList<Elfo> getEstrategia_NoturnosPorUltimo();
    
    public ArrayList<Elfo> getEstrategia_AtaqueIntercalado();
    
    public ArrayList<Elfo> getEstrategia_TercoNoturno();
}
