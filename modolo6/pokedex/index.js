const pokeApi = new PokeApi();

function limparListas() {
  const lis = document.querySelectorAll( '.tipo li' );
  for ( let i = 0; i < lis.length; i += 1 ) {
    const li = lis[i];
    li.parentNode.removeChild( li );
  }
  const lis2 = document.querySelectorAll( '.estatisticas li' );
  for ( let i = 0; i < lis2.length; i += 1 ) {
    const li = lis2[i];
    li.parentNode.removeChild( li );
  }
}

function gerarListaTipo( array, lista ) {
  for ( let i = 0; i < array.length; i += 1 ) {
    const item = document.createElement( 'li' );
    item.appendChild( document.createTextNode( array[i].type.name ) );
    lista.appendChild( item );
  }
}

function gerarListaEstatistica( array, lista ) {
  for ( let i = 0; i < array.length; i += 1 ) {
    const item = document.createElement( 'li' );
    item.appendChild( document.createTextNode( `${ array[i].stat.name }: ${ array[i].base_stat }` ) );
    lista.appendChild( item );
  }
}

const renderizar = ( pokemon ) => {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );
  const dadosPokemonImagem = document.getElementById( 'dadosPokemonImagem' );

  limparListas();

  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = pokemon.nome;

  const imagem = dadosPokemonImagem.querySelector( '.thumb' );
  imagem.src = pokemon.imagem;

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = pokemon.altura;

  const idPokemon = dadosPokemon.querySelector( '.idPokemon' );
  idPokemon.innerHTML = pokemon.idPokemon;

  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = pokemon.peso;

  const tipo = dadosPokemon.querySelector( '.tipo' );
  gerarListaTipo( pokemon.tipo, tipo );

  const estatisticas = dadosPokemon.querySelector( '.estatisticas' );
  gerarListaEstatistica( pokemon.estatisticas, estatisticas );
}

function limparTudo() {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );

  limparListas();

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = '';

  const idPokemon = dadosPokemon.querySelector( '.idPokemon' );
  idPokemon.innerHTML = '';

  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = '';
}

function imprimirErroELimparDados( mensagemErro ) {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );
  const dadosPokemonImagem = document.getElementById( 'dadosPokemonImagem' );
  const nome = dadosPokemon.querySelector( '.nome' );
  const imagem = dadosPokemonImagem.querySelector( '.thumb' );
  imagem.src = './img/padrao.jpg';
  nome.innerHTML = mensagemErro;
  limparTudo();
}

let capturando = '';
let capturadoAnteriormente = 0;

function capturar() { // eslint-disable-line no-unused-vars
  capturando = document.getElementById( 'valor' ).value;

  if ( capturando === '' ) {
    imprimirErroELimparDados( 'coloque um id' );
  } else if ( capturadoAnteriormente === capturando ) {
    return null;
  } else {
    pokeApi.buscarEspecifico( capturando )
      .then( pokemon => {
        const poke = new Pokemon( pokemon );
        renderizar( poke );
      } ).catch( imprimirErroELimparDados( 'coloque um id válido' ) );
    capturadoAnteriormente = capturando;
  }
  return null
}

function getRandomInt( min, max ) {
  return Math.floor( Math.random() * ( max - min ) ) + min;
}

function verificarSeTotalCacheENulo() {
  if ( localStorage.getItem( 'total' ) === null ) {
    return true;
  }
  return false;
}

function capturarSorte() { // eslint-disable-line no-unused-vars
  const numeroAleatorio = getRandomInt( 1, 894 );

  if ( localStorage.getItem( numeroAleatorio ) === null ) {
    pokeApi.buscarEspecifico( numeroAleatorio )
      .then( pokemon => {
        const poke = new Pokemon( pokemon );
        renderizar( poke );
      } );
    localStorage.setItem( numeroAleatorio, numeroAleatorio );
    if ( verificarSeTotalCacheENulo() ) {
      localStorage.setItem( 'total', 1 );
    } else {
      localStorage.setItem( 'total', parseInt( localStorage.getItem( 'total' ), 10 ) + 1 );
    }
  } else if ( parseInt( localStorage.getItem( 'total' ), 10 ) === 893 ) {
    const dadosPokemon = document.getElementById( 'dadosPokemon' );
    const dadosPokemonImagem = document.getElementById( 'dadosPokemonImagem' );
    const nome = dadosPokemon.querySelector( '.nome' );
    const imagem = dadosPokemonImagem.querySelector( '.thumb' );
    imagem.src = './img/alerta.jpg';
    nome.innerHTML = 'LIMPE O LOCALSTORAGE!';
    limparTudo();
  } else {
    capturarSorte();
  }
}

function capturarBotao() { // eslint-disable-line no-unused-vars
  const botao = document.querySelector( '.buttonFechado' );
  console.log( botao.textContent );
  if ( botao.textContent === 'Abrir' ) {
    botao.innerHTML = 'Fechar';
  } else {
    botao.innerHTML = 'Abrir';
  }
}
