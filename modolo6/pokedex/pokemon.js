class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objDaApi ) {
    this._nome = objDaApi.name;
    this._imagem = objDaApi.sprites.front_default;
    this._altura = objDaApi.height;
    this._idPokemon = objDaApi.id;
    this._peso = objDaApi.weight;
    this._tipo = objDaApi.types;
    this._estatisticas = objDaApi.stats;
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `Altura: ${ this._altura * 10 } cm`;
  }

  get idPokemon() {
    return `Id: ${ this._idPokemon }`;
  }

  get peso() {
    return `Peso: ${ this._peso / 10 } kg`;
  }

  get tipo() {
    return this._tipo;
  }

  get estatisticas() {
    return this._estatisticas;
  }
}
