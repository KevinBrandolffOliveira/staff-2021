export default class Serie {
  constructor({ titulo, anoEstreia, diretor, genero, elenco, numeroEpisodios, temporadas, distribuidora }) {
    this.titulo = titulo;
    this.estreia = anoEstreia;
    this.diretor = diretor;
    this.genero = genero;
    this.elenco = elenco;
    this.numeroEpisodios = numeroEpisodios;
    this.temporadas = temporadas;
    this.distribuidora = distribuidora;
  }
}