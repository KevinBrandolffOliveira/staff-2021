
function calcularCirculo(circulo){
    
    if(circulo.tipoCalculo === "A"){
        return Math.PI * (circulo.raio * circulo.raio);
    }else if(circulo.tipoCalculo === "C"){
        return (circulo.raio*2) * Math.PI;
    }

}

const circulo = {
    raio: 4,
    tipoCalculo: "A"
}

const circulo1 = {
    raio: 4,
    tipoCalculo: "C"
}

console.log(calcularCirculo(circulo));
console.log(calcularCirculo(circulo1));


function naoBissexto(ano){
    if (ano%4 == 0){
        if(ano%100 != 0){
            return false
        }else{
            return true;
        }
    }else if (ano%400 == 0){
        return false
    }else{
        return true
    }
}

console.log(naoBissexto(2016));
console.log(naoBissexto(2015));
console.log(naoBissexto(2020));

function somarPares(array = []){
    let soma = 0;
    for(let i = 0; i < array.length; i++){
        if(i % 2 == 0){
            soma += array[i];
        }
    }
    return soma;
}

console.log(somarPares([1,56,4.34,6,-2]));


function adicionarModoUm( number1 ){

    function somar(number2) {
        return number1 + number2;
    }

    return somar;

}

function adicionarModoDois( number1 ){

    return function somar(number2) {
        return number1 + number2;
    }

}

let adicionarModoTres = valor1 => valor2 => valor1 + valor2;

console.log(adicionarModoUm(1)(2));
console.log(adicionarModoUm(5642)(8749));

console.log(adicionarModoDois(1)(2));
console.log(adicionarModoDois(5642)(8749));

console.log(adicionarModoTres(1)(2));
console.log(adicionarModoTres(5642)(8749));

function arredondarParaCima(value){
    while (value > 100){
        value = value / 10;
    }
    return Math.ceil(value);
}

function imprimirBRL(value){
    if(value >= 0){
        let numeroParcialmenteFormatado =  value.toLocaleString('pt-BR');
        let array = numeroParcialmenteFormatado.split(",");
        let aux = 0;
    
        if (parseInt(array[1], 10) > 99){
            aux = arredondarParaCima(parseInt(array[1], 10));
            return "R$ " + array[0] + "," + aux;
        }

        return "R$ " + numeroParcialmenteFormatado;
    }else{
        let numeroParcialmenteFormatado =  Math.abs(value).toLocaleString('pt-BR');
        let array = numeroParcialmenteFormatado.split(",");
        let aux = 0;
    
        if (parseInt(array[1], 10) > 99){
            aux = arredondarParaCima(parseInt(array[1], 10));
            return "-R$ " + array[0] + "," + aux;
        }

    return "-R$ " + numeroParcialmenteFormatado;
    }
}

console.log(imprimirBRL(4.651));
console.log(imprimirBRL(0));
console.log(imprimirBRL(3498.99));
console.log(imprimirBRL(-3498.99));
console.log(imprimirBRL(2313477.0135));