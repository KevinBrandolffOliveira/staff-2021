import React, { Component } from 'react';

import EpisodioAPI from '../../models/EpisodioAPI';
import ListaEpisodios from '../../models/ListaEpisodios';

import BotaoUi from '../../Components/BotaoUi';
import { Link } from 'react-router-dom';

export default class TodosEpisodios extends Component {
  constructor(props) {
    super(props)
    this.episodioAPI = new EpisodioAPI();
    this.state = {};
  }

  componentDidMount() {
    this.episodioAPI.buscar()
      .then(episodio => {
        this.setState(state => { return { ...state, listaEpisodios: new ListaEpisodios(episodio) } })
      }).then( () => {
        this.setState(state => { return { ...state, listaEpisodiosOrdenados: this.state.listaEpisodios.ordenar() } })
      } )
  }


  render() {

    const { listaEpisodiosOrdenados } = this.state;

    return (
      !listaEpisodiosOrdenados ?
        (<h2>Carregando...</h2>) :
        (<React.Fragment>
          <BotaoUi classe="preto" link="/" nome="Página Inicial" />
          {
            listaEpisodiosOrdenados && (
              listaEpisodiosOrdenados.map((e, i) => {
                return <div> <Link to={`episodio/${ e.id }`}>{`${e.nome} - ${e.nota}`}</Link> </div>
            })
          )
        }
        </React.Fragment>)
    );
  }
}