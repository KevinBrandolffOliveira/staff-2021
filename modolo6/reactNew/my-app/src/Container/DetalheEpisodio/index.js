import React, { Component } from 'react';

import EpisodioAPI from '../../models/EpisodioAPI';

import BotaoUi from '../../Components/BotaoUi';
import EpisodioDetalhes from '../../models/EpisodioDetalhes';

export default class EpisodioDetalhe extends Component {
  constructor(props) {
    super(props)
    this.episodioAPI = new EpisodioAPI();
    //const { id } = props.match.params;
    this.id = props.match.params['id']
    this.state = {};
  }

  componentDidMount() {
    const requisicoes = [
      this.episodioAPI.buscarEpisodioEspecifico( this.id ),
      this.episodioAPI.buscarDetalhesEpisodio( this.id ),
      this.episodioAPI.buscarEpisodioAvaliadoEspecifico( this.id )
    ]

    Promise.all( requisicoes )
      .then( requisicoes => {
        console.log( requisicoes[2][0] )
        if ( requisicoes[2][0] != null ) {
          this.setState(state => { return { ...state, episodioDetalhes: new EpisodioDetalhes( requisicoes[0] , requisicoes[1][0], requisicoes[2][0]) } })
        }else{
          this.setState(state => { return { ...state, episodioDetalhes: new EpisodioDetalhes( requisicoes[0] , requisicoes[1][0], { nota: "nao avaliado" }) } })
        }
        
      } )
  }

  render() {

    const { episodioDetalhes } = this.state;

    return (
      !episodioDetalhes ?
        (<h2>Carregando...</h2>) :
        (<React.Fragment>
          <BotaoUi classe="preto" link="/" nome="Página Inicial" />
          <div>{episodioDetalhes.nome}</div>
          <img src={ episodioDetalhes.imagem } alt={ episodioDetalhes.nome } />
          <div>Temporada/Episódio: { episodioDetalhes.temporadaEpisodio }</div>
          <div>Duracao: { `${episodioDetalhes.duracao} min` }</div>
          <div>Sinopse: { episodioDetalhes.sinopse }</div>
          <div>Data de estreia: { episodioDetalhes.dataEstreia } </div>
          <div>Nota: { episodioDetalhes.nota }</div>
          <div>Nota do Imdb: { episodioDetalhes.notaImdb }</div>
        </React.Fragment>)
    );
  }
}