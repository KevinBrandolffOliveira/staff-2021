import React, { Component } from 'react';

import EpisodioAPI from '../../models/EpisodioAPI';
import ListaEpisodios from '../../models/ListaEpisodios';
import ListaEpisodiosAvaliados from '../../models/ListaEpisodiosAvaliados'; 

import BotaoUi from '../../Components/BotaoUi';
import { Link } from 'react-router-dom';

export default class ListaAvaliacoes extends Component {
  constructor(props) {
    super(props)
    this.episodioAPI = new EpisodioAPI();
    this.state = {};
  }

  componentDidMount() {
    this.episodioAPI.buscar()
      .then(episodio => {
        this.listaEpisodios = new ListaEpisodios(episodio);
      }).then( () => {
        this.episodioAPI.buscarEpisodiosAvaliados()
        .then(episodio => {
        this.listaEpisodiosAvaliados = new ListaEpisodiosAvaliados(episodio);
        this.setState(state => { return { ...state, episodiosAvaliados: this.listaEpisodios.epsAvaliados(this.listaEpisodiosAvaliados) } })
      })
     } )
  }


  render() {

    const { episodiosAvaliados } = this.state;

    return (
      !episodiosAvaliados ? 
      ( <h2>Carregando...</h2> ) :
      (<React.Fragment>
        <BotaoUi classe="preto" link="/" nome="Página Inicial" />
        {
          episodiosAvaliados && (
            episodiosAvaliados.map((e, i) => {
              return <div> <Link to={`episodio/${ e.id }`}>{`${e.nome} - ${e.nota}`}</Link> </div>
            })
          )
        }
      </React.Fragment>)
    );
  }
}