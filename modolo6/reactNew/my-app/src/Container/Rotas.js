import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import App from './App';
import ListagemAvaliacoes from './Avaliacoes';
import TodosEps from './TodosEpisodios';
import EpDetalhado from './DetalheEpisodio';

export default class Rotas extends Component {
  render() {
    return (
      <Router>
        <Route path="/" exact component={ App } />
        <Route path="/avaliacoes" component={ ListagemAvaliacoes } />
        <Route path="/todosEpisodios" component={ TodosEps } />
        <Route path="/episodio/:id" component={ EpDetalhado } />
      </Router>
    );
  }
}
