import EpisodioAvaliado from "./EpisodioAvaliado";

export default class ListaEpisodios {
  constructor( episodioAPI ) {
    this.todos = episodioAPI.map( episodio => new EpisodioAvaliado( episodio ) );
  }

}