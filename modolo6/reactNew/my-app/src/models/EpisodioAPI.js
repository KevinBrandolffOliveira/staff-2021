import axios from 'axios';

const endereco = 'http://localhost:9000/api';

export default class EpisodioAPI {
  buscar() {
    return axios.get( `${ endereco }/episodios` ).then( e => e.data );
  }

  registrarNota( { nota, episodioId } ) {
    return axios.post( `${ endereco }/notas`, { nota, episodioId } );
  }

  buscarEpisodiosAvaliados() {
    return axios.get( `${endereco}/notas` ).then( e => e.data );
  }

  buscarEpisodioAvaliadoEspecifico( id ) {
    return axios.get( `${endereco}/notas/?episodioId=${id}` ).then( e => e.data );
  }

  buscarEpisodioEspecifico( id ) {
    return axios.get( `${endereco}/episodios/${id}` ).then( e => e.data );
  }

  buscarDetalhesEpisodio( id ) {
    return axios.get( `${endereco}/episodios/${id}/detalhes` ).then( e => e.data );
  }
}