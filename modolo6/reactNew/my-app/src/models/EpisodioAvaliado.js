export default class EpisodioAvaliado {
  constructor( { nota, episodioId } ) {
    this.nota = nota;
    this.id = episodioId;
  }

  get idEP(){
    return this.id;
  }

}