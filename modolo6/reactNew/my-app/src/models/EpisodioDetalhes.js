import EpisodioAPI from "./EpisodioAPI";

export default class EpisodioDetalhes {
  constructor( { id, nome, duracao, temporada, ordemEpisodio, thumbUrl }, {sinopse, notaImdb, dataEstreia}, {nota}  ) {
    this.id = id;
    this.nome = nome;
    this.duracao = duracao;
    this.temporada = temporada;
    this.ordem = ordemEpisodio;
    this.imagem = thumbUrl;
    this.qtdVezesAssistido = 0;
    this.sinopse = sinopse;
    this.notaImdb = notaImdb;
    this.dataEstreia = dataEstreia;
    this.nota = nota;
    this.episodioApi = new EpisodioAPI();
  }

  get duracaoEmMin() {
    return `${ this.duracao } min`; 
  }

  get temporadaEpisodio() {
    return `${ this.temporada.toString().padStart( 2, 0 ) }/${ this.ordem.toString().padStart( 2, 0 ) }`;
  }

  avaliar( nota ) {
    return this.episodioApi.registrarNota( { nota: nota, episodioId: this.id } );
  }

  assistido() {
    this.foiAssistido = true;
    this.qtdVezesAssistido += 1;
  }

  validarNota( nota ) {
    return (nota >= 1 && nota <= 5);
  }
 

}