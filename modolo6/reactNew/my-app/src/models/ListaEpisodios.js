import Episodio from "./Episodio";

function sortear( min, max ) {
  min = Math.ceil( min );
  max = Math.floor( max );
  return Math.floor( Math.random() * ( max - min ) ) + min;
}

export default class ListaEpisodios {
  constructor( episodioAPI ) {
    this.todos = episodioAPI.map( episodio => new Episodio( episodio ) );
  }

  get episodioAleatorio() {
    const indice = sortear( 0, this.todos.length );
    return this.todos[ indice ];
  }

  ordenarAvaliados( arrayDeAvaliados ) {
    return arrayDeAvaliados.sort( ( a, b ) => a.temporada - b.temporada || a.ordem - b.ordem );
  }

  ordenar() {
    return this.todos.sort( ( a, b ) => a.temporada - b.temporada || a.ordem - b.ordem );
  }

  epsAvaliados( arrayDeAvaliados ) {
    let arrayRetorno = [];
    let arrayDosQueJaForam = []
    for ( let i = 0; i < arrayDeAvaliados.todos.length; i++ ) {
      for ( let j = 0; j < this.todos.length; j++ ) {
        if ( this.todos[j].id === arrayDeAvaliados.todos[i].id ) {
          if ( this.validaEpisodio( this.todos[j].id, arrayDosQueJaForam ) ) {
            arrayRetorno.push( this.todos[j] );
            arrayDosQueJaForam.push( this.todos[j].id );
          }
        }
      }
    }
    return this.ordenarAvaliados(arrayRetorno);
  }

  validaEpisodio( idEpisodio, arrayDosQueJaForam ) {
    for ( let i = 0; i < arrayDosQueJaForam.length; i++ ) {
      if ( idEpisodio === arrayDosQueJaForam[i] ){
        return false
      }
    }
    return true;
  }

}

