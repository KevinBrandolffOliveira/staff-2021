class Time {

    constructor( nome, tipo, status, liga ){
        this._nome = nome;
        this._tipo = tipo;
        this._status = status;
        this._liga = liga;
        this._jogadores = [];
    }

    get nome() {
        return this._nome;
    }

    get tipo() {
        return this._tipo;
    }

    get status() {
        return this._status;
    }

    set status( status ) {
        this._status = status;
    }

    get liga() {
        return this._liga;
    }

    set liga( liga ) {
        this._liga = liga;
    }

    adicionarJogador( jogador ){
        this._jogadores.push(jogador);
    }

    buscarJogadorPorNome( nome ){
        for(let i = 0; i < this._jogadores.length; i++){
            if( this._jogadores[i].nome === nome ){
                return this._jogadores[i];
            }
        }
        return null;
    }

    buscarJogadorPorNumero( numero ){
        for(let i = 0; i < this._jogadores.length; i++){
            if( this._jogadores[i].numero === numero ){
                return this._jogadores[i];
            }
        }
        return null;
    }

    buscarJogadorPorNomeENumero( nome, numero ){
        for(let i = 0; i < this._jogadores.length; i++){
            if( this._jogadores[i].numero === numero ){
                if( this._jogadores[i].nome === nome ){
                    return this._jogadores[i];
                }
            }
        }
        return null;
    }

}


class Jogador{

    constructor(nome, numero){
        this._nome = nome;
        this._numero = numero;
    }

    get nome(){
        return this._nome;
    }

    get numero(){
        return this._numero;
    }

}

let time = new Time( "inter", "eltronico", "ativo", "adulta");

let jogador1 = new Jogador("kevin", 10);
let jogador2 = new Jogador("jose", 9);
let jogador3 = new Jogador("joao", 8);

time.adicionarJogador(jogador1);
time.adicionarJogador(jogador2);
time.adicionarJogador(jogador3);

console.log(time);

console.log(time.buscarJogadorPorNome("jose"));
console.log(time.buscarJogadorPorNumero(8));
console.log(time.buscarJogadorPorNomeENumero("kevin", 10));
console.log(time.buscarJogadorPorNome("adriano"));