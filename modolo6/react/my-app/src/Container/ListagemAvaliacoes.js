import React, { Component } from 'react';
import BotaoUi from '../Components/BotaoUi';
import EpisodioAvaliacaoUi from '../Components/EpisodioAvaliacaoUi';

import './Css/ListagemAvaliacoes.css';
import './Css/grid.css'
import ListaEpisodios from '../models/ListaEpisodios';

export default class ListagemAvaliacoes extends Component {
  constructor(props) {
    super(props);
    this.listaEpisodios = this.props.location.state || new ListaEpisodios();
    this.listaEpisodiosOrdenados = this.listaEpisodios.ordenar;
  }

  render() {

    return (
      <React.Fragment>
        <section className="container">
        <div>
            <BotaoUi
              link="/"
              nameButton="Home"
            />
          </div >
          <div className="row">
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[0]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[1]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[2]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[3]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[4]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[5]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[6]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[7]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[8]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[9]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[10]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[11]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[12]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[13]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[14]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[15]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[16]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[17]}
            />
            <EpisodioAvaliacaoUi
              episodio={this.listaEpisodiosOrdenados[18]}
            />
          </div>
        </section>
      </React.Fragment>
    );
  }

}