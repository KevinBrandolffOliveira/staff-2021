import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import App from './App';
import ListagemAvaliacoes from './ListagemAvaliacoes';

export default class Rotas extends Component {
  render() {
    return (
      <Router>
        <Route path="/" exact component={ App } />
        <Route path="/avaliacoes" component={ ListagemAvaliacoes } />
      </Router>
    );
  }
}
