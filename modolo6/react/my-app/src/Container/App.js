import React, { Component } from 'react';

import ListaEpisodios from '../models/ListaEpisodios';
import EpisodioUi from '../Components/EpisodioUi';
import MensagemFlash from '../Components/MensagemFlash';
import MeuInputNumero from '../Components/MeuInputNumero';

import './Css/App.css';
import SerieUi from '../Components/SerieUi';
import BotaoUi from '../Components/BotaoUi';
import ApiEpisodios from '../Api/ApiEpisodios';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.apiEps = new ApiEpisodios();
    this.sortear = this.sortear.bind(this);
    this.marcarComoAssistido = this.marcarComoAssistido.bind(this);
    this.state = {
      deveExibirMensagem: false,
      deveExibirErro: false
    }
  }

  componentDidMount() {

    this.apiEps.buscarTodosEps().then(resposta => {
      const episodiosDB = resposta;
      this.listaEpisodios = new ListaEpisodios(episodiosDB);
      this.setState((state) => {
        return {
          ...state,
          episodio: this.listaEpisodios.episodioAleatorio
        }
      });
    })

    console.log("cheguei2")
    console.log(this.state.episodio)

  }

  sortear() {
    const episodio = this.listaEpisodios.episodioAleatorio;

    this.setState((state) => {
      return {
        ...state,
        episodio
      }
    });
  }

  marcarComoAssistido() {
    const { episodio } = this.state;
    episodio.assistido();
    this.setState((state) => {
      return {
        ...state,
        episodio
      }
    });
  }

  registrarNota({ erro, nota }) {
    this.setState((state) => {
      return {
        ...state,
        deveExibirErro: erro
      }
    });
    if (erro) {
      return;
    }

    const { episodio } = this.state;
    let mensagem, corMensagem;

    if (episodio.validarNota(nota)) {
      this.apiEps.salvarNota({ epId: episodio.idEpisodio, notaEnvio: nota });
      episodio.avaliar(nota);
      corMensagem = 'verde';
      mensagem = 'Nota registrada com sucesso!';
    } else {
      corMensagem = 'vermelho';
      mensagem = 'Informar uma nota válida (entre 1 e 5)!';
    }

    this.exibirMensagem({ corMensagem, mensagem });
  }

  exibirMensagem = ({ corMensagem, mensagem }) => {
    this.setState((state) => {
      return {
        ...state,
        deveExibirMensagem: true,
        mensagem,
        corMensagem
      }
    });
  }

  atualizarMensagem = devoExibir => {
    this.setState((state) => {
      return {
        ...state,
        deveExibirMensagem: devoExibir
      }
    });
  }

  render() {
    const { episodio, deveExibirMensagem, corMensagem, mensagem, deveExibirErro } = this.state;

    return (!episodio ? (
      <h2>Aguarde</h2>
    ) : (
      <React.Fragment>
        <div className="App">
          <MensagemFlash atualizar={this.atualizarMensagem} exibir={deveExibirMensagem} mensagem={mensagem} cor={corMensagem} />
          <header className="App-header">
            <EpisodioUi episodio={episodio} />
            <div className="botoes">
              <BotaoUi
                nameButton="Próximo"
                className="btn verde"
                onClick={this.sortear}
              />
              <BotaoUi
                nameButton="Já Assisti!"
                className="btn azul"
                onClick={this.marcarComoAssistido}
              />
            </div>
            <span>Nota: {episodio.nota}</span>
            <MeuInputNumero
              placeholder="Nota de 1 a 5"
              mensagem="Qual a sua nota para esse episódio?"
              visivel={episodio.foiAssistido || false}
              obrigatorio
              exibirErro={deveExibirErro}
              atualizarValor={this.registrarNota.bind(this)} />
            <SerieUi
            />
            <BotaoUi
              link={{ pathname: "/avaliacoes", state: this.listaEpisodios }}
              nameButton="Avaliacões"
              className="btn verde"
            />
          </header>
        </div>
      </React.Fragment>
    )
    );
  }
}