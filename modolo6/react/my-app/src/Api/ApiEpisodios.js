export default class ApiEpisodios { // eslint-disable-line no-unused-vars
  constructor() {
    this._url = 'http://localhost:9000/api/';
  }

  buscarTodosEps(){
    return fetch( `${ this._url }episodios` )
      .then( data =>  data.json() );
  }

  buscarEspecifico( id ) {
    return fetch( `${ this._url }episodios/${ id }` )
      .then( data => data.json() )
  }

  buscarEspecificoComDetalhe( id ) {
    return fetch( `${ this._url }episodios/${ id }/detalhes` )
      .then( data => data.json() );
  }

  salvarNota( {epId, notaEnvio} ){
    const body = { epId: epId, notaEnvio: notaEnvio }
    fetch( `${ this._url }notas`, {
      method: 'POST',
      body: JSON.stringify(body)
    } ).then((res) => res.json())
    .then((response) => console.log(response));
  }
}