import React, { Component } from 'react';
import {Link} from 'react-router-dom'

import './General.css';
import '../Container/Css/App.css';

export default class BotaoUi extends Component { 
  
  render() {
    
    const  { nameButton, className, onClick} = this.props;

    if ( this.props.link != null  ){
      return(
        <React.Fragment>
          <Link to={this.props.link}>{nameButton}</Link>
        </React.Fragment>
      );
    }else {
      return(
        <React.Fragment>
          <button className={className} onClick={onClick}>{nameButton}</button>
        </React.Fragment>
      );
    }
  }
}