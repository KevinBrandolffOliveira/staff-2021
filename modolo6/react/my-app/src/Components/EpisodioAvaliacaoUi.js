import React, { Component } from 'react';

export default class EpisodioAvaliacaoUi extends Component {
  render() {
    const { episodio } = this.props;

    if (episodio.nota != null) {
      return (
        <React.Fragment>
          <div className="col col-12 col-md-4">
            <article className="cardEp">
              <h2>{episodio.nome}</h2>
              <img src={episodio.imagem} alt={episodio.nome} className="cardImg" />
              <span>Duração: {episodio.duracaoEmMin}</span>
              <span>Temporada/Episódio: {episodio.temporadaEpisodio}</span>
            </article>
          </div>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
        </React.Fragment>
      )
    }
  }
}