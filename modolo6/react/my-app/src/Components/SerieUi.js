import React, { Component } from 'react';

import ListaSeries from '../models/ListaSeries'

export default class SerieUi extends Component {
  render() {

    this.listaSeries1 = new ListaSeries();

    return(
      <React.Fragment>
        {console.log(this.listaSeries1.invalidas())}
        {console.log(this.listaSeries1.filtrarPorAno(2017))}
        {console.log(this.listaSeries1.procurarPorNome("Anna Gunn"))}
        {console.log(this.listaSeries1.mediaDeEpisodios())}
        {console.log(this.listaSeries1.totalSalarios(1))}
        {console.log(this.listaSeries1.queroGenero("Drama"))}
        {console.log(this.listaSeries1.queroTitulo("Things"))}
        {console.log(this.listaSeries1.mostrarCreditos(1))}
        {console.log(this.listaSeries1.hashtagSecreta())}
      </React.Fragment>
    );
  }
}