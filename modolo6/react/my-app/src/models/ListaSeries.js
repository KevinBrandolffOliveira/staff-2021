import Serie from './Serie';
import PropTypes from 'prop-types';

export default class ListaSeries {
  constructor() {
    this.series = [
      {
        titulo: "Stranger Things",
        anoEstreia: 2016,
        diretor: [
          "Matt Duffer",
          "Ross Duffer"
        ],
        genero: [
          "Suspense",
          "Ficcao Cientifica",
          "Drama"
        ],
        elenco: [
          "Winona Ryder",
          "David Harbour",
          "Finn Wolfhard",
          "Millie Bobby Brown",
          "Gaten Matarazzo",
          "Caleb McLaughlin",
          "Natalia Dyer",
          "Charlie Heaton",
          "Cara Buono",
          "Matthew Modine",
          "Noah Schnapp"
        ],
        temporadas: 2,
        numeroEpisodios: 17,
        distribuidora: "Netflix"
      },
      {
        titulo: "Game Of Thrones",
        anoEstreia: 2011,
        diretor: [
          "David Benioff",
          "D. B. Weiss",
          "Carolyn Strauss",
          "Frank Doelger",
          "Bernadette Caulfield",
          "George R. R. Martin"
        ],
        genero: [
          "Fantasia",
          "Drama"
        ],
        elenco: [
          "Peter Dinklage",
          "Nikolaj Coster-Waldau",
          "Lena Headey",
          "Emilia Clarke",
          "Kit Harington",
          "Aidan Gillen",
          "Iain Glen ",
          "Sophie Turner",
          "Maisie Williams",
          "Alfie Allen",
          "Isaac Hempstead Wright"
        ],
        temporadas: 7,
        numeroEpisodios: 67,
        distribuidora: "HBO"
      },
      {
        titulo: "The Walking Dead",
        anoEstreia: 2010,
        diretor: [
          "Jolly Dale",
          "Caleb Womble",
          "Paul Gadd",
          "Heather Bellson"
        ],
        genero: [
          "Terror",
          "Suspense",
          "Apocalipse Zumbi"
        ],
        elenco: [
          "Andrew Lincoln",
          "Jon Bernthal",
          "Sarah Wayne Callies",
          "Laurie Holden",
          "Jeffrey DeMunn",
          "Steven Yeun",
          "Chandler Riggs ",
          "Norman Reedus",
          "Lauren Cohan",
          "Danai Gurira",
          "Michael Rooker ",
          "David Morrissey"
        ],
        temporadas: 9,
        numeroEpisodios: 122,
        distribuidora: "AMC"
      },
      {
        titulo: "Band of Brothers",
        anoEstreia: 20001,
        diretor: [
          "Steven Spielberg",
          "Tom Hanks",
          "Preston Smith",
          "Erik Jendresen",
          "Stephen E. Ambrose"
        ],
        genero: [
          "Guerra"
        ],
        elenco: [
          "Damian Lewis",
          "Donnie Wahlberg",
          "Ron Livingston",
          "Matthew Settle",
          "Neal McDonough"
        ],
        temporadas: 1,
        numeroEpisodios: 10,
        distribuidora: "HBO"
      },
      {
        titulo: "The JS Mirror",
        anoEstreia: 2017,
        diretor: [
          "Lisandro",
          "Jaime",
          "Edgar"
        ],
        genero: [
          "Terror",
          "Caos",
          "JavaScript"
        ],
        elenco: [
          "Amanda de Carli",
          "Alex Baptista",
          "Gilberto Junior",
          "Gustavo Gallarreta",
          "Henrique Klein",
          "Isaias Fernandes",
          "João Vitor da Silva Silveira",
          "Arthur Mattos",
          "Mario Pereira",
          "Matheus Scheffer",
          "Tiago Almeida",
          "Tiago Falcon Lopes"
        ],
        temporadas: 5,
        numeroEpisodios: 40,
        distribuidora: "DBC"
      },
      {
        titulo: "Mr. Robot",
        anoEstreia: 2018,
        diretor: [
          "Sam Esmail"
        ],
        genero: [
          "Drama",
          "Techno Thriller",
          "Psychological Thriller"
        ],
        elenco: [
          "Rami Malek",
          "Carly Chaikin",
          "Portia Doubleday",
          "Martin Wallström",
          "Christian Slater"
        ],
        temporadas: 3,
        numeroEpisodios: 32,
        distribuidora: "USA Network"
      },
      {
        titulo: "Narcos",
        anoEstreia: 2015,
        diretor: [
          "Paul Eckstein",
          "Mariano Carranco",
          "Tim King",
          "Lorenzo O Brien"
        ],
        genero: [
          "Documentario",
          "Crime",
          "Drama"
        ],
        elenco: [
          "Wagner Moura",
          "Boyd Holbrook",
          "Pedro Pascal",
          "Joann Christie",
          "Mauricie Compte",
          "André Mattos",
          "Roberto Urbina",
          "Diego Cataño",
          "Jorge A. Jiménez",
          "Paulina Gaitán",
          "Paulina Garcia"
        ],
        temporadas: 3,
        numeroEpisodios: 30,
        distribuidora: null
      },
      {
        titulo: "Westworld",
        anoEstreia: 2016,
        diretor: [
          "Athena Wickham"
        ],
        genero: [
          "Ficcao Cientifica",
          "Drama",
          "Thriller",
          "Acao",
          "Aventura",
          "Faroeste"
        ],
        elenco: [
          "Anthony I. Hopkins",
          "Thandie N. Newton",
          "Jeffrey S. Wright",
          "James T. Marsden",
          "Ben I. Barnes",
          "Ingrid N. Bolso Berdal",
          "Clifton T. Collins Jr.",
          "Luke O. Hemsworth"
        ],
        temporadas: 2,
        numeroEpisodios: 20,
        distribuidora: "HBO"
      },
      {
        titulo: "Breaking Bad",
        anoEstreia: 2008,
        diretor: [
          "Vince Gilligan",
          "Michelle MacLaren",
          "Adam Bernstein",
          "Colin Bucksey",
          "Michael Slovis",
          "Peter Gould"
        ],
        genero: [
          "Acao",
          "Suspense",
          "Drama",
          "Crime",
          "Humor Negro"
        ],
        elenco: [
          "Bryan Cranston",
          "Anna Gunn",
          "Aaron Paul",
          "Dean Norris",
          "Betsy Brandt",
          "RJ Mitte"
        ],
        temporadas: 5,
        numeroEpisodios: 62,
        distribuidora: "AMC"
      }
    ].map(serie => new Serie(serie));
  }

  invalidas() {
    const invalidas1 = this.series.filter( serie => (this.todosCamposValidos(serie) === false) );
    const invalidas2 = this.series.filter(serie => (serie.anoEstreia > 2021));
    const invalidas = invalidas1.concat(invalidas2);
    let string = 'Séries inválidas: ';
    for (let i = 0; i < invalidas.length; i++){
      if ( i === invalidas.length-1){
        string += invalidas[i].titulo;  
      }else{
        string += invalidas[i].titulo + " - ";
      }
    }
    return string;
  }

  todosCamposValidos(serie) {
    if (serie.titulo === null || serie.titulo === undefined) {
      return false
    }
    if (serie.anoEstreia === null || serie.anoEstreia === undefined) {
      return false
    }
    if (serie.diretor === null || serie.diretor === undefined) {
      return false
    }
    if (serie.genero === null || serie.genero === undefined) {
      return false
    }
    if (serie.elenco === null || serie.elenco === undefined) {
      return false
    }
    if (serie.temporadas === null || serie.temporadas === undefined) {
      return false
    }
    if (serie.numeroEpisodios === null || serie.numeroEpisodios === undefined) {
      return false
    }
    if (serie.distribuidora === null || serie.distribuidora === undefined) {
      return false
    }
    return true;

  }

  filtrarPorAno(ano){
    const seriesPorAno = this.series.filter( serie => ( serie.anoEstreia >= ano ));
    return seriesPorAno;
  }

  procurarPorNome(nome){
    for( let i = 0; i< this.series.length; i++){
      for( let j = 0; j < this.series[i].elenco.length; j++ ){
        if ( this.series[i].elenco[j] === nome ){
          return true
        }
      }
    }
    return false
  }

  mediaDeEpisodios(){
    let media = 0;
    for (let i = 0; i < this.series.length; i++){
      media += this.series[i].numeroEpisodios;
    }
    return media/this.series.length;
  }

  totalSalarios(indice){
    console.log(this.series[indice].diretor.length)
    let somaSalarios = this.series[indice].diretor.length * 100000;
    somaSalarios += this.series[indice].elenco.length * 40000;
    return somaSalarios;
  }

  queroGenero(genero){
    let filmesComGenero = this.series.filter( serie => ( this.verificaSeTemOGenero(serie.genero, genero) === true ) );
    return filmesComGenero;
  }

  verificaSeTemOGenero(array, genero){
    for (let i = 0; i < array.length; i++){
      if ( array[i] === genero ){
        return true;
      }
    }
    return false
  }

  queroTitulo(pesquisa){
    let arrayPesquisa = [];
    for (let i = 0; i < this.series.length; i++){
      let arrayNome = this.series[i].titulo.split(" ");
      for ( let j = 0; j < arrayNome.length; j++ ){
        if ( arrayNome[j] === pesquisa ){
          arrayPesquisa.push(this.series[i]);
        }
      }
    }
    return arrayPesquisa;
  }

  mostrarCreditos(indice){
    let serie = this.series[indice];
    let string = `${serie.titulo}
      Diretores: 
      ${this.organizarAlfabeticoAoContrario(serie.diretor)}
      Elenco: 
      ${this.organizarAlfabeticoAoContrario(serie.elenco)}`;
    return string;
  }

  organizarAlfabeticoAoContrario(array){
    array.sort();
    let arrayOrdenado = [];
    for ( let i = array.length-1; i >= 0; i-- ){
      arrayOrdenado.push(array[i]);
    }
    return arrayOrdenado;
  }

  hashtagSecreta(){
    let hashtag = "";
    for( let i = 0; i < this.series.length; i++ ){
      if ( this.verificaArrayNomesTemAbreviacao(this.series[i].elenco) ){
        hashtag = this.fazAbreviacao(this.series[i].elenco);
      }
    }
    return hashtag;
  }

  verificaArrayNomesTemAbreviacao(array){
    let temAbreviacao = 0;
    for( let i = 0; i < array.length; i++ ){
      let arrayDePalavras = array[i].split(" ");
      let palavraComPonto = arrayDePalavras[1].split('.');
      if ( palavraComPonto[0].length === 1 ){
        temAbreviacao++;
      }
    }
    return (temAbreviacao === array.length)? true : false ; 
  }

  fazAbreviacao(array){
    let hashtag = "";
    for( let i = 0; i < array.length; i++ ){
      let arrayDePalavras = array[i].split(" ");
      let palavraComPonto = arrayDePalavras[1].split(".");
      if ( palavraComPonto[0].length === 1 ){
        hashtag += palavraComPonto[0];
      }
    }
    return hashtag ; 
  }

}

ListaSeries.propTypes = {
  invalidas: PropTypes.array
}
