export default class Episodio {
  constructor( { nome, id, duracao, temporada, ordemEpisodio, thumbUrl } ) {
    this.nome = nome;
    this.duracao = duracao;
    this.temporada = temporada;
    this.ordem = ordemEpisodio;
    this.imagem = thumbUrl;
    this.id = id;
    this.qtdVezesAssistido = 0;
    this.nota = null;
  }

  get duracaoEmMin() {
    return `${ this.duracao } min`; 
  }

  get temporadaEpisodio() {
    return `${ this.temporada.toString().padStart( 2, 0 ) }/${ this.ordem.toString().padStart( 2, 0 ) }`;
  }

  get idEpisodio() {
    return `${this.id}`;
  }

  avaliar( nota ) {
    this.nota = parseInt( nota );
  }

  assistido() {
    this.foiAssistido = true;
    this.qtdVezesAssistido += 1;
  }

  validarNota( nota ) {
    return (nota >= 1 && nota <= 5);
  }

}