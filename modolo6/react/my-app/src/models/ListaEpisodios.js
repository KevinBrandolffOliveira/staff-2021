import Episodio from "./Episodio";

function sortear(min, max) {
  min = Math.ceil( min );
  max = Math.floor( max );
  return Math.floor( Math.random() * ( max - min ) ) + min;
}

export default class ListaEpisodios {
  constructor( todosEpsDb = [] ) {
    console.log(todosEpsDb)
    this.todos = todosEpsDb.map( episodio => new Episodio( episodio ) );
  }

  get episodioAleatorio() {
    const indice = sortear( 0, this.todos.length ); 
    return this.todos[ indice ];
  }

  get ordenar() {
     return this.todos.sort( function (ep1, ep2) {
       if (ep1.temporada === ep2.temporada){
          if ( ep1.ordemEpisodio < ep2.ordemEpisodio ) {
            return -1
          }else{
            return 1
          }
       }else if ( ep1.temporada < ep2.temporada ) {
         return 0
       }else{
         return 1
       }
     } )
  }

}

