function cardapioIFood(veggie = true, comLactose = true) {
    let cardapio = [
        'enroladinho de salsicha',
        'cuca de uva'
    ];

    if (comLactose) {
        cardapio.push('pastel de queijo');
    }

    cardapio.push('pastel de carne');
    cardapio.push('empada de legumes marabijosa');

    let k = 0;
    while ( k < cardapio.length ) {
        console.log(cardapio[k]);
        k++;
    }

    if (veggie) {
        // TODO: remover alimentos com carne (é obrigatório usar splice!)
        const indiceEnroladinho = cardapio.indexOf('enroladinho de salsicha');
        arr = cardapio.splice(indiceEnroladinho, 1);
        const indicePastelCarne = cardapio.indexOf('pastel de carne');
        arr = cardapio.splice(indicePastelCarne, 1);
    }

    const resultadoFinal = cardapio.map( alimento => alimento.toUpperCase() );
    return resultadoFinal;

}

cardapioIFood() // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]