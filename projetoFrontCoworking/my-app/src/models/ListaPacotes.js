import Pacote from "./Pacote";

export default class ListaPacotes {
  constructor( arrayDePacotes ) {
    this.pacotes = arrayDePacotes.map( pacote => new Pacote( pacote ) );
  }

}