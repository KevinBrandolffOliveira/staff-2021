export default class ContratacaoEspacoParaPersistir {
  constructor( espaco, cliente, contratacao ){
    this.espaco = espaco;
    this.cliente = cliente;
    this.tipoContratacao = contratacao.tipoContratacao;
    this.quantidade = contratacao.quantidade;
    this.desconto = contratacao.desconto;
    this.prazo = contratacao.prazo;
  }
}