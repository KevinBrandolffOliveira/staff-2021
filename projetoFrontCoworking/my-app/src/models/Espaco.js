export default class Espaco {
  constructor( { id, nome, qtdPessoas, valor } ) {
    this.id = id;
    this.nome = nome;
    this.qtdPessoas = qtdPessoas;
    this.valor = valor;
  }

}