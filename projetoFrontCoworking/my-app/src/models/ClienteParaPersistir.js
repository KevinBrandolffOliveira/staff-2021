export default class ClienteParaPersistir {
  constructor( { nome, cpf, dataNascimento, email, telefone } ) {
    this.nome = nome;
    this.cpf = cpf;
    this.dataNascimento = ((dataNascimento._d.getDate() )) + "/" + ((dataNascimento._d.getMonth() + 1)) + "/" + dataNascimento._d.getFullYear();
    this.contatos = [ { tipoContato: {id:1, nome:"email"}, valor: email }, { tipoContato: {id:2, nome:"celular"}, valor: telefone } ];
  }

}