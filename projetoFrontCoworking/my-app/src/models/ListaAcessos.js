import Acesso from './Acesso'

export default class ListaAcessos{
  constructor( arrayDeAcessos ){
    this.acessos = arrayDeAcessos.map( acesso => new Acesso( acesso ) );
  }
}