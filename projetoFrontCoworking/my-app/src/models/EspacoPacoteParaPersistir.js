export default class EspacoPacoteParaPersistir{
  constructor( espaco, pacote, espacoPacote ){
    this.espaco = espaco;
    this.pacote = pacote;
    this.tipoContratacao = espacoPacote.tipoContratacao;
    this.quantidade = espacoPacote.quantidade;
    this.prazo = espacoPacote.prazo;
  }
}