export default class Acesso {
  constructor( { id, id_cliente, id_espaco, data, is_entrada } ) {
    this.id = id;
    this.idCliente = id_cliente;
    this.idEspaco = id_espaco;
    this.data = data;
    this.isEntrada = is_entrada;
  }
}