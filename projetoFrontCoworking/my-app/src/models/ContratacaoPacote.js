export default class ContratacaoPacote {
  constructor( { id, cliente, pacote, quantidade } ){
    this.id = id;
    this.cliente = cliente;
    this.pacote = pacote;
    this.quantidade = quantidade;
  }
}