export default class AcessoParaPersistir {
  constructor( { idCliente, idEspaco, isEntrada } ){
    this.id_cliente = idCliente;
    this.id_espaco = idEspaco;
    this.is_entrada = ((isEntrada === "true" ) ? true : false); 
  }
}