export default class ContratacaoContrato {
  constructor( { id, espaco, cliente, tipoContratacao, quantidade, desconto, prazo, valor } ){
    this.id = id;
    this.espaco = espaco;
    this.cliente = cliente;
    this.tipoContratacao = tipoContratacao;
    this.quantidade = quantidade;
    this.desconto = desconto;
    this.prazo = prazo;
    this.valor = valor;
  }
}