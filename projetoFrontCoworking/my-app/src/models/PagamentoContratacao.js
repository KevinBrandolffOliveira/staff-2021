export default class PagamentoContratacao {
  constructor( { id, contratacao, tipoPagamento } ){
    this.id = id;
    this.contratacao = contratacao;
    this.tipoPagamento = tipoPagamento;
  }
}