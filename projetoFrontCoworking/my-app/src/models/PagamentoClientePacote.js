export default class PagamentoClientePacote {
  constructor( { id, clientePacote, tipoPagamento } ) {
    this.id = id;
    this.clientePacote = clientePacote;
    this.tipoPagamento = tipoPagamento;
  }
}