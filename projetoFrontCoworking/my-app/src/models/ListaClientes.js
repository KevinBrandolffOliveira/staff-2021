import Cliente from "./Cliente";

export default class ListaPacotes {
  constructor( arrayDeClientes ) {
    this.clientes = arrayDeClientes.map( cliente => new Cliente( cliente ) );
  }

}