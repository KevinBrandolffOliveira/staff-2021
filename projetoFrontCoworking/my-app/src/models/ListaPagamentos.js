import PagamentoContratacao from "./PagamentoContratacao";
import PagamentoClientePacote from "./PagamentoClientePacote"

export default class ListaPagamentos {
  constructor( arrayDePagamentos ) {
    this.pagamentosContratacao = arrayDePagamentos.filter( pagamento => pagamento.contratacao != null ).map( pagamento => new PagamentoContratacao( pagamento ) );
    this.pagamentosClientePacote = arrayDePagamentos.filter( pagamento => pagamento.clientePacote != null ).map( pagamento => new PagamentoClientePacote( pagamento ) );
  }

}