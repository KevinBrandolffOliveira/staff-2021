export default class ClienteParaPersistir {
  constructor( {id, nome, cpf, dataNascimento, contatos } ) {
    this.id = id;
    this.nome = nome;
    this.cpf = cpf;
    this.dataNascimento = dataNascimento;
    this.email = contatos[0].valor;
    this.celular = contatos[1].valor;
  }

}