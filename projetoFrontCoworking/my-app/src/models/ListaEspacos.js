import Espaco from "./Espaco";

export default class ListaPacotes {
  constructor( arrayDeEspaco ) {
    this.espacos = arrayDeEspaco.map( espaco => new Espaco( espaco ) );
  }

}