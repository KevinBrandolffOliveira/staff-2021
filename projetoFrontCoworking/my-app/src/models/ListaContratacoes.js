import ContratacaoContrato from "./ContratacaoContrato";
import ContratacaoPacote from "./ContratacaoPacote";

export default class ListaContratacoes {
  constructor( arrayDeContratacoesContratos, arrayDeContratacoesPacotes ) {
    this.contratacoesContratos = arrayDeContratacoesContratos.map( contrato => new ContratacaoContrato( contrato ) );
    this.contratacoesPacotes = arrayDeContratacoesPacotes.map( pacote => new ContratacaoPacote( pacote ) );
  }

}