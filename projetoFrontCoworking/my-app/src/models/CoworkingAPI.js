import axios from 'axios';
import { getToken } from "../Utils/auth";
import { login } from '../Utils/auth';

const api = axios.create({
  baseURL: "http://localhost:8080"
});

api.interceptors.request.use(async config => {
  const token = getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default class CoworkingAPI {

  async efetuarLogin({ username, password }) {
    const response = await api.post( "/login", { username, password } );
    try{
      const headerAuthorization = response.headers.authorization.split(' ');
      const token = headerAuthorization[1];
      login(token);
    } catch( err ) {
      console.log(`Não foi possível fazer a autenticação.`);
    }
  }

  buscarTodosPacotes() {
    return api.get( `/api/pacotes/` ).then( e => e.data );
  }

  buscarPacoteEspeficio( id ) {
    return api.get( `api/pacotes/${id}` ).then( e => e.data );
  }

  cadastrarPacote( pacote ) {
    return api.post( `api/pacotes/salvar`, pacote );
  }

  buscarTodosEspacos() {
    return api.get( `api/espaco/` ).then( e => e.data );
  }

  buscarEspacoEspecifico( id ) {
    return api.get( `api/espaco/${id}` ).then( e => e.data );
  }

  cadastrarEspaco( espaco ) {
    return api.post( `api/espaco/salvar`, espaco );
  }

  buscarTodosClientes() {
    return api.get( `api/cliente/` ).then( e => e.data );
  }

  buscarClienteEspecifico( id ) {
    return api.get( `api/cliente/${id}` ).then( e => e.data );
  }

  cadastrarCliente( cliente ) {
    return api.post( `api/cliente/salvar`, cliente );
  }

  alocarEspacoAoPacote( espacoPacote ) {
    return api.post( `api/espaco_pacote/salvar`, espacoPacote );
  }

  buscarEspacoPacoteEspecifico( idPacote ) {
    return api.get( `api/espaco_pacote/pacote/${idPacote}`).then( e => e.data );
  }

  cadastrarContratacao( contratacao ) {
    return api.post( `api/contratacao/salvar`, contratacao );
  }

  cadastrarClientePacote( clientePacote ) {
    return api.post( `api/cliente_pacote/salvar`, clientePacote );
  }

  registrarPagamento( pagamento ) {
    return api.post( `api/pagamentos/salvar`, pagamento );
  }

  buscarContratacaoEspecifica( idContratacao ) {
    return api.get( `api/contratacao/${idContratacao}`).then( e => e.data );
  }

  buscarClientePacoteEspecifico( idClientePacote ) {
    return api.get( `api/cliente_pacote/${idClientePacote}`).then( e => e.data );
  }

  buscarTodasContratacoesContrato() {
    return api.get( `api/contratacao/` ).then( e => e.data );
  }

  buscarTodasContratacoesPacote() {
    return api.get( `api/cliente_pacote/` ).then( e => e.data );
  }

  buscarTodosPagamentos() {
    return api.get( `api/pagamentos/` ).then( e => e.data );
  }

  registrarAcesso( acesso ) {
    return api.post( `api/acessos/salvar`, acesso );
  }

  buscarTodosAcessos() {
    return api.get( `api/acessos/` ).then( e => e.data );
  }

}