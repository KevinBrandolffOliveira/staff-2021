import React from 'react';
import ReactDOM from 'react-dom';
import Rotas from './Container/Rotas';

import 'antd/dist/antd.css';

ReactDOM.render( <Rotas />, document.getElementById('root') );
