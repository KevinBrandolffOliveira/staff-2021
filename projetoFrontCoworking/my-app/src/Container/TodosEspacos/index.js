import React, { Component } from 'react';

import CoworkingApi from '../../models/CoworkingAPI.js'
import ListaEspacos from '../../models/ListaEspacos';

import Layout from '../../Middler/Components/Layout'
import Header from '../../Middler/Components/Header'
import Content from '../../Middler/Components/Content'
import Col from '../../Middler/Components/Col'
import Row from '../../Middler/Components/Row'

import '../Css/BackGroundColor.css'
import '../Css/Content-css.css'
import '../Css/Header-css.css'
import '../Css/PosicaoNoMeio.css'

import CardEspacoUi from '../../Components/CardEspacoUi/index.js';
import { Link } from 'react-router-dom';

import { logout } from '../../Utils/auth.js';

export default class TodosEspacos extends Component {
  constructor(props) {
    super(props)
    this.coworkingApi = new CoworkingApi();
    this.state = {}
  }

  componentDidMount() {
    this.coworkingApi.buscarTodosEspacos()
      .then(arrayDeEspaco => {
        this.setState(state => { return { ...state, listaEspacos: new ListaEspacos(arrayDeEspaco) } })
      })
  }

  render() {

    const { listaEspacos } = this.state;

    return (
      !listaEspacos ?
        (<h2>Carregando...</h2>) :
        (
          <React.Fragment>
            <Layout className="BackGroundColor">
              <Header className="Header-css">
                Brandolff Coworking
              </Header>
              <Header className="Header-css">
                <Link className="LinkHeader" to={"/"}>Home</Link>
                <Link className="LinkHeader" to={"/cadastro"}>Cadastrar</Link>
                <Link className="LinkHeader" to={"/pacotes"}>Pacotes</Link>
                <Link className="LinkHeader" to={"/espacos"}>Espaços</Link>
                <Link className="LinkHeader" to={"/clientes"}>Clientes</Link>
                <Link className="LinkHeader" to={"/contratacao"}>Contratacão</Link>
                <Link className="LinkHeader" to={"/pagamento"}>Pagamentos</Link>
                <Link className="LinkHeader" to={"/acesso"}>Acesso</Link>
                <Link className="LinkHeader" onClick={logout}>Logout</Link>
              </Header>
              <Content className="Content-css">
                <Row>
                  <Col span={8}></Col>
                  <Col span={8}>
                    {
                      listaEspacos.espacos && (
                        listaEspacos.espacos.map((espaco, i) => {
                          return <CardEspacoUi
                            key={i}
                            nome={espaco.nome}
                            id={espaco.id}
                            valor={espaco.valor}
                            qtdPessoas={espaco.qtdPessoas}
                          />
                        })
                      )
                    }
                  </Col>
                  <Col span={8}></Col>
                </Row>
              </Content>
            </Layout>
          </React.Fragment>
        )
    )
  }


}
