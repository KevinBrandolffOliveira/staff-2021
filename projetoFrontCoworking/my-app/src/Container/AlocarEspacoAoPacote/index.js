import React, { Component } from 'react';

import Layout from '../../Middler/Components/Layout'
import Header from '../../Middler/Components/Header'
import Content from '../../Middler/Components/Content'
import Col from '../../Middler/Components/Col'
import Row from '../../Middler/Components/Row'

import CoworkingAPI from '../../models/CoworkingAPI';

import '../Css/Content-css.css'
import '../Css/Header-css.css'
import FormAlocacaoEspacoPacoteUi from '../../Components/FormAlocacaoEspacoPacoteUi';
import EspacoPacoteParaPersistir from '../../models/EspacoPacoteParaPersistir';

export default class AlocarEspacoAoPacote extends Component {
  constructor(props) {
    super(props)
    this.coworkingAPI = new CoworkingAPI();
  }

  AlocarEspacoAoPacote(espacoPacote) {
    const requisicoes = [
      this.coworkingAPI.buscarEspacoEspecifico(espacoPacote.idEspaco),
      this.coworkingAPI.buscarPacoteEspeficio(espacoPacote.idPacote),
    ]

    Promise.all(requisicoes).then(requisicoes => {
      let espacoPacoteParaPersistir = new EspacoPacoteParaPersistir(requisicoes[0], requisicoes[1], espacoPacote);
      console.log(requisicoes[0])
      console.log(requisicoes[1])
      console.log(espacoPacoteParaPersistir)
      this.coworkingAPI.alocarEspacoAoPacote(espacoPacoteParaPersistir);
    })
  }


  render() {

    return (
      <React.Fragment>
        <Layout>
          <Header className="Header-css">
            Brandolff Coworking
          </Header>
          <Content className="Content-css">
            <Row>
              <Col span={8}></Col>
              <Col span={8}>
                <FormAlocacaoEspacoPacoteUi
                  metodoCadastrar={this.AlocarEspacoAoPacote.bind(this)}
                />
              </Col>
              <Col span={8}></Col>
            </Row>
          </Content>
        </Layout>
      </React.Fragment >
    );
  }
}