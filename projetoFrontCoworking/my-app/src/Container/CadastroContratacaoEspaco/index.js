import React, { Component } from 'react';

import Layout from '../../Middler/Components/Layout'
import Header from '../../Middler/Components/Header'
import Content from '../../Middler/Components/Content'
import Col from '../../Middler/Components/Col'
import Row from '../../Middler/Components/Row'

import CoworkingAPI from '../../models/CoworkingAPI';

import '../Css/BackGroundColor.css'
import '../Css/Content-css.css'
import '../Css/Header-css.css'
import FormContratacaoEspacoUi from '../../Components/FormContratacaoEspacoUi';
import ContratacaoEspacoParaPersistir from '../../models/ContratacaoEspacoParaPersistir';
import { Link } from 'react-router-dom';

import { logout } from '../../Utils/auth';

export default class CadastroContratacaoEspaco extends Component {
  constructor(props) {
    super(props)
    this.coworkingAPI = new CoworkingAPI();
  }

  cadastrarContratacaoEspaco(contratacao) {
    const requisicoes = [
      this.coworkingAPI.buscarEspacoEspecifico(contratacao.idEspaco),
      this.coworkingAPI.buscarClienteEspecifico(contratacao.idCliente)
    ]

    Promise.all(requisicoes)
      .then(requisicoes => {
        const contratacaoParaPersistir = new ContratacaoEspacoParaPersistir(requisicoes[0], requisicoes[1], contratacao)
        this.coworkingAPI.cadastrarContratacao(contratacaoParaPersistir);
      })
  }


  render() {

    return (
      <React.Fragment>
        <Layout className="BackGroundColor">
          <Header className="Header-css">
            Brandolff Coworking
          </Header>
          <Header className="Header-css">
            <Link className="LinkHeader" to={"/"}>Home</Link>
            <Link className="LinkHeader" to={"/cadastro"}>Cadastrar</Link>
            <Link className="LinkHeader" to={"/pacotes"}>Pacotes</Link>
            <Link className="LinkHeader" to={"/espacos"}>Espaços</Link>
            <Link className="LinkHeader" to={"/clientes"}>Clientes</Link>
            <Link className="LinkHeader" to={"/contratacao"}>Contratacão</Link>
            <Link className="LinkHeader" to={"/pagamento"}>Pagamentos</Link>
            <Link className="LinkHeader" to={"/acesso"}>Acesso</Link>
            <Link className="LinkHeader" onClick={logout}>Logout</Link>
          </Header>
          <Content className="Content-css">
            <Row>
              <Col span={8}></Col>
              <Col span={8}>
                <FormContratacaoEspacoUi
                  metodoCadastrar={this.cadastrarContratacaoEspaco.bind(this)}
                />
              </Col>
              <Col span={8}></Col>
            </Row>
          </Content>
        </Layout>
      </React.Fragment >
    );
  }
}