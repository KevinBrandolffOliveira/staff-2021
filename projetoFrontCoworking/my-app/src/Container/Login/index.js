import React, { Component } from 'react';

import Layout from '../../Middler/Components/Layout'
import Header from '../../Middler/Components/Header'
import Content from '../../Middler/Components/Content'
import Col from '../../Middler/Components/Col'
import Row from '../../Middler/Components/Row'

import CoworkingAPI from '../../models/CoworkingAPI';

import '../Css/BackGroundColor.css'
import '../Css/Content-css.css'
import '../Css/Header-css.css'
import FormLoginUi from '../../Components/FormLoginUi';
import { isAuthenticated } from '../../Utils/auth';

export default class Login extends Component {
  constructor(props){
    super(props)
    this.coworkingAPI = new CoworkingAPI();
  }

  efetuarLogin( credenciais ) {
    console.log(credenciais)
    this.coworkingAPI.efetuarLogin( credenciais )
      .then( () => {
        if( isAuthenticated() ) {
          this.props.history.push('/');
        }
      } )
  }


  render() {

    return (
      <React.Fragment>
        <Layout className="BackGroundColor">
          <Header className="Header-css">
            Brandolff Coworking
          </Header>
          <Content className="Content-css">
            <Row>
              <Col span={8}></Col>
              <Col span={8}>
                <FormLoginUi
                  metodoCadastrar={this.efetuarLogin.bind(this)}
                />
              </Col>
              <Col span={8}></Col>
            </Row>
          </Content>
        </Layout>
      </React.Fragment >
    );
  }
}