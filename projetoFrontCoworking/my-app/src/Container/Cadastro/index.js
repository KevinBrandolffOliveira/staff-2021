import React, { Component } from 'react';

import ButtonUi from '../../Components/ButtonBlockUi'

import Layout from '../../Middler/Components/Layout'
import Header from '../../Middler/Components/Header'
import Content from '../../Middler/Components/Content'
import Col from '../../Middler/Components/Col'
import Row from '../../Middler/Components/Row'

import '../Css/BackGroundColor.css'
import '../Css/Content-css.css'
import '../Css/Header-css.css'
import { Link } from 'react-router-dom';

import { logout } from '../../Utils/auth';

export default class Cadastro extends Component {

  render() {

    return (
      <React.Fragment>
        <Layout className="BackGroundColor">
          <Header className="Header-css">
            Brandolff Coworking
          </Header>
          <Header className="Header-css">
            <Link className="LinkHeader" to={"/"}>Home</Link>
            <Link className="LinkHeader" to={"/cadastro"}>Cadastrar</Link>
            <Link className="LinkHeader" to={"/pacotes"}>Pacotes</Link>
            <Link className="LinkHeader" to={"/espacos"}>Espaços</Link>
            <Link className="LinkHeader" to={"/clientes"}>Clientes</Link>
            <Link className="LinkHeader" to={"/contratacao"}>Contratacão</Link>
            <Link className="LinkHeader" to={"/pagamento"}>Pagamentos</Link>
            <Link className="LinkHeader" to={"/acesso"}>Acesso</Link>
            <Link className="LinkHeader" onClick={logout}>Logout</Link>
          </Header>
          <Content className="Content-css">
            <Row>
              <Col span={8}></Col>
              <Col span={8}>
                <div>
                  <ButtonUi
                    nome={"Cadastrar Cliente"}
                    link={"/cadastro/cliente"}
                  />
                </div>
                <br></br>
                <div>
                  <ButtonUi
                    nome={"Cadastrar Espaço"}
                    link={"/cadastro/espaco"}
                  />
                </div>
                <br></br>
                <div>
                  <ButtonUi
                    nome={"Cadastrar Pacote"}
                    link={"/cadastro/pacote"}
                  />
                </div>
                <br></br>
                <div>
                  <ButtonUi
                    nome={"Alocar espaço ao pacote"}
                    link={"/cadastro/espacoPacote"}
                  />
                </div>
              </Col>
              <Col span={8}></Col>
            </Row>
          </Content>
        </Layout>
      </React.Fragment>
    );
  }
}