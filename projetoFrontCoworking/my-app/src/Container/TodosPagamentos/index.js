import React, { Component } from 'react';

import CoworkingApi from '../../models/CoworkingAPI.js'
import ListaPagamentos from '../../models/ListaPagamentos.js';

import Layout from '../../Middler/Components/Layout'
import Header from '../../Middler/Components/Header'
import Content from '../../Middler/Components/Content'
import Col from '../../Middler/Components/Col'
import Row from '../../Middler/Components/Row'

import ButtonUi from '../../Components/ButtonUi'

import '../Css/BackGroundColor.css'
import '../Css/Content-css.css'
import '../Css/Header-css.css'
import CardPagamentoContratacaoUi from '../../Components/CardPagamentoContratacaoUi/index.js';
import CardPagamentoClientePacoteUi from '../../Components/CardPagamentoClientePacoteUi/index.js';
import { Link } from 'react-router-dom';

import { logout } from '../../Utils/auth.js';

export default class TodosPagamentos extends Component {
  constructor(props) {
    super(props)
    this.coworkingApi = new CoworkingApi();
    this.state = {}
  }

  componentDidMount() {

    this.coworkingApi.buscarTodosPagamentos()
      .then(arrayDePagamentos => {
        this.setState(state => { return { ...state, listaPagamentos: new ListaPagamentos(arrayDePagamentos) } })
      })
  }

  render() {

    const { listaPagamentos } = this.state;

    return (
      !listaPagamentos ?
        (<h2>Carregando...</h2>) :
        (
          <React.Fragment>
            <Layout className="BackGroundColor">
              <Header className="Header-css">
                Brandolff Coworking
              </Header>
              <Header className="Header-css">
                <Link className="LinkHeader" to={"/"}>Home</Link>
                <Link className="LinkHeader" to={"/cadastro"}>Cadastrar</Link>
                <Link className="LinkHeader" to={"/pacotes"}>Pacotes</Link>
                <Link className="LinkHeader" to={"/espacos"}>Espaços</Link>
                <Link className="LinkHeader" to={"/clientes"}>Clientes</Link>
                <Link className="LinkHeader" to={"/contratacao"}>Contratacão</Link>
                <Link className="LinkHeader" to={"/pagamento"}>Pagamentos</Link>
                <Link className="LinkHeader" to={"/acesso"}>Acesso</Link>
                <Link className="LinkHeader" onClick={logout}>Logout</Link>
              </Header>
              <Content className="Content-css">
                <Row>
                  <Col span={8}></Col>
                  <Col span={8}>
                    {
                      listaPagamentos.pagamentosContratacao && (
                        listaPagamentos.pagamentosContratacao.map((pagamentoContratacao, i) => {
                          return <CardPagamentoContratacaoUi
                            key={i}
                            id={pagamentoContratacao.id}
                            contratacao={pagamentoContratacao.contratacao}
                            tipoPagamento={pagamentoContratacao.tipoPagamento}
                          />
                        })
                      )
                    }
                    {
                      listaPagamentos.pagamentosClientePacote && (
                        listaPagamentos.pagamentosClientePacote.map((pagamentoPacote, i) => {
                          return <CardPagamentoClientePacoteUi
                            key={i}
                            id={pagamentoPacote.id}
                            clientePacote={pagamentoPacote.clientePacote}
                            tipoPagamento={pagamentoPacote.tipoPagamento}
                          />
                        })
                      )
                    }
                    <div className="PosicaoNoMeio">
                      <ButtonUi
                        nome={"Voltar"}
                        link={"/pagamento"}
                      />
                    </div>
                  </Col>
                  <Col span={8}></Col>
                </Row>
              </Content>
            </Layout>
          </React.Fragment>
        )
    )
  }


}
