import React, { Component } from 'react';

import CoworkingApi from '../../models/CoworkingAPI.js'
import ListaClientes from '../../models/ListaClientes';

import Layout from '../../Middler/Components/Layout'
import Header from '../../Middler/Components/Header'
import Content from '../../Middler/Components/Content'
import Col from '../../Middler/Components/Col'
import Row from '../../Middler/Components/Row'

import '../Css/BackGroundColor.css'
import '../Css/Content-css.css'
import '../Css/Header-css.css'
import '../Css/PosicaoNoMeio.css'
import CardClienteUi from '../../Components/CardClienteUi/index.js';
import { Link } from 'react-router-dom';

import { logout } from '../../Utils/auth.js';

export default class TodosClientes extends Component {
  constructor(props) {
    super(props)
    this.coworkingApi = new CoworkingApi();
    this.state = {}
  }

  componentDidMount() {
    this.coworkingApi.buscarTodosClientes()
      .then(arrayDeClientes => {
        this.setState(state => { return { ...state, listaClientes: new ListaClientes(arrayDeClientes) } })
      })
  }

  render() {

    const { listaClientes } = this.state;

    return (
      !listaClientes ?
        (<h2>Carregando...</h2>) :
        (
          <React.Fragment>
            <Layout className="BackGroundColor">
              <Header className="Header-css">
                Brandolff Coworking
              </Header>
              <Header className="Header-css">
                <Link className="LinkHeader" to={"/"}>Home</Link>
                <Link className="LinkHeader" to={"/cadastro"}>Cadastrar</Link>
                <Link className="LinkHeader" to={"/pacotes"}>Pacotes</Link>
                <Link className="LinkHeader" to={"/espacos"}>Espaços</Link>
                <Link className="LinkHeader" to={"/clientes"}>Clientes</Link>
                <Link className="LinkHeader" to={"/contratacao"}>Contratacão</Link>
                <Link className="LinkHeader" to={"/pagamento"}>Pagamentos</Link>
                <Link className="LinkHeader" to={"/acesso"}>Acesso</Link>
                <Link className="LinkHeader" onClick={logout}>Logout</Link>
              </Header>
              <Content className="Content-css">
                <Row>
                  <Col span={8}></Col>
                  <Col span={8}>
                    {
                      listaClientes.clientes && (
                        listaClientes.clientes.map((cliente, i) => {
                          return <CardClienteUi
                            key={i}
                            nome={cliente.nome}
                            id={cliente.id}
                            cpf={cliente.cpf}
                            dataNascimento={cliente.dataNascimento}
                            email={cliente.email}
                            celular={cliente.celular}
                          />
                        })
                      )
                    }
                  </Col>
                  <Col span={8}></Col>
                </Row>
              </Content>
            </Layout>
          </React.Fragment>
        )
    )
  }


}