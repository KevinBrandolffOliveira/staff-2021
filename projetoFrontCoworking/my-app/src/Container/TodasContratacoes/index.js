import React, { Component } from 'react';

import CoworkingApi from '../../models/CoworkingAPI.js'
import ListaContratacoes from '../../models/ListaContratacoes.js';

import CardContratacaoContratoUi from '../../Components/CardContratacaoContratoUi'
import CardContratacaoPacoteUi from '../../Components/CardContratacaoPacoteUi'

import Layout from '../../Middler/Components/Layout'
import Header from '../../Middler/Components/Header'
import Content from '../../Middler/Components/Content'
import Col from '../../Middler/Components/Col'
import Row from '../../Middler/Components/Row'

import ButtonUi from '../../Components/ButtonUi'

import '../Css/BackGroundColor.css'
import '../Css/Content-css.css'
import '../Css/Header-css.css'
import '../Css/PosicaoNoMeio.css'
import { Link } from 'react-router-dom';

import { logout } from '../../Utils/auth.js';

export default class TodasContratacoes extends Component {
  constructor(props) {
    super(props)
    this.coworkingApi = new CoworkingApi();
    this.state = {}
  }

  componentDidMount() {
    const requisicoes = [
      this.coworkingApi.buscarTodasContratacoesContrato(),
      this.coworkingApi.buscarTodasContratacoesPacote()
    ]

    Promise.all(requisicoes)
      .then(requisicoes => {
        console.log(requisicoes[0])
        console.log(requisicoes[1])
        let listaDeContratacoesParaMostrar = new ListaContratacoes(requisicoes[0], requisicoes[1])
        console.log(listaDeContratacoesParaMostrar)
        this.setState(state => { return { ...state, listaDeContratacoes: listaDeContratacoesParaMostrar } })
      })
  }

  render() {

    const { listaDeContratacoes } = this.state;
    console.log(listaDeContratacoes)

    return (
      !listaDeContratacoes ?
        (<h2>Carregando...</h2>) :
        (
          <React.Fragment>
            <Layout className="BackGroundColor">
              <Header className="Header-css">
                Brandolff Coworking
              </Header>
              <Header className="Header-css">
                <Link className="LinkHeader" to={"/"}>Home</Link>
                <Link className="LinkHeader" to={"/cadastro"}>Cadastrar</Link>
                <Link className="LinkHeader" to={"/pacotes"}>Pacotes</Link>
                <Link className="LinkHeader" to={"/espacos"}>Espaços</Link>
                <Link className="LinkHeader" to={"/clientes"}>Clientes</Link>
                <Link className="LinkHeader" to={"/contratacao"}>Contratacão</Link>
                <Link className="LinkHeader" to={"/pagamento"}>Pagamentos</Link>
                <Link className="LinkHeader" to={"/acesso"}>Acesso</Link>
                <Link className="LinkHeader" onClick={logout}>Logout</Link>
              </Header>
              <Content className="Content-css">
                <Row>
                  <Col span={8}></Col>
                  <Col span={8}>
                    {
                      listaDeContratacoes.contratacoesContratos && (
                        listaDeContratacoes.contratacoesContratos.map((contratacao, i) => {
                          return <CardContratacaoContratoUi
                            key={i}
                            id={contratacao.id}
                            espaco={contratacao.espaco}
                            cliente={contratacao.cliente}
                            tipoContratacao={contratacao.tipoContratacao}
                            quantidade={contratacao.quantidade}
                            desconto={contratacao.desconto}
                            prazo={contratacao.prazo}
                            valor={contratacao.valor}
                          />
                        })
                      )
                    }
                    {
                      listaDeContratacoes.contratacoesPacotes && (
                        listaDeContratacoes.contratacoesPacotes.map((contratacao, i) => {
                          return <CardContratacaoPacoteUi
                            key={i}
                            id={contratacao.id}
                            cliente={contratacao.cliente}
                            pacote={contratacao.pacote}
                            quantidade={contratacao.quantidade}
                          />
                        })
                      )
                    }
                    <div className="PosicaoNoMeio">
                      <ButtonUi
                        nome={"Voltar"}
                        link={"/contratacao"}
                      />
                    </div>
                  </Col>
                  <Col span={8}></Col>
                </Row>
              </Content>
            </Layout>
          </React.Fragment>
        )
    )
  }


}