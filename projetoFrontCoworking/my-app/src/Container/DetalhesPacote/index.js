import React, { Component } from 'react';

import CoworkingAPI from '../../models/CoworkingAPI'

import Layout from '../../Middler/Components/Layout'
import Header from '../../Middler/Components/Header'
import Content from '../../Middler/Components/Content'
import Col from '../../Middler/Components/Col'
import Row from '../../Middler/Components/Row'

import ButtonUi from '../../Components/ButtonUi'

import '../Css/BackGroundColor.css'
import '../Css/Content-css.css'
import '../Css/Header-css.css'
import CardEspacoUi from '../../Components/CardEspacoUi/index.js';
import { Link } from 'react-router-dom';

import { logout } from '../../Utils/auth';

export default class DetalhesEpisodio extends Component {
  constructor(props) {
    super(props)
    this.coworkingAPI = new CoworkingAPI();
    this.id = props.match.params['id']
    this.state = {};
  }

  componentDidMount() {
    this.coworkingAPI.buscarEspacoPacoteEspecifico(this.id)
      .then(arrayDeEspacoPacote => {
        console.log(arrayDeEspacoPacote)
        this.setState(state => { return { ...state, listaEspacoPacote: arrayDeEspacoPacote } })
      })
  }

  render() {

    const { listaEspacoPacote } = this.state;

    return (
      !listaEspacoPacote ?
        (<h2>Carregando...</h2>) :
        (<React.Fragment>
          <Layout className="BackGroundColor">
            <Header className="Header-css">
              Brandolff Coworking
            </Header>
            <Header className="Header-css">
              <Link className="LinkHeader" to={"/"}>Home</Link>
              <Link className="LinkHeader" to={"/cadastro"}>Cadastrar</Link>
              <Link className="LinkHeader" to={"/pacotes"}>Pacotes</Link>
              <Link className="LinkHeader" to={"/espacos"}>Espaços</Link>
              <Link className="LinkHeader" to={"/clientes"}>Clientes</Link>
              <Link className="LinkHeader" to={"/contratacao"}>Contratacão</Link>
              <Link className="LinkHeader" to={"/pagamento"}>Pagamentos</Link>
              <Link className="LinkHeader" to={"/acesso"}>Acesso</Link>
              <Link className="LinkHeader" onClick={logout}>Logout</Link>
            </Header>
            <Content className="Content-css">
              <Row>
                <Col span={8}></Col>
                <Col span={8}>
                  {
                    listaEspacoPacote && (
                      listaEspacoPacote.map((espacoPacote, i) => {
                        return <CardEspacoUi
                          key={i}
                          nome={espacoPacote.espaco.nome}
                          id={espacoPacote.espaco.id}
                          valor={espacoPacote.espaco.valor}
                          qtdPessoas={espacoPacote.espaco.qtdPessoas}
                        />
                      })
                    )
                  }
                  <div className="PosicaoNoMeio">
                    <ButtonUi
                      nome={"Voltar"}
                      link={"/pacotes"}
                    />
                  </div>
                </Col>
                <Col span={8}></Col>
              </Row>
            </Content>
          </Layout>
        </React.Fragment>)
    );
  }
}