import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Home from './Home';
import CadastroCliente from './CadastroCliente';
import CadastroEspaco from './CadastroEspaco';
import CadastroPacote from './CadastroPacote';
import Cadastro from './Cadastro';
import TodosPacotes from './TodosPacotes';
import TodosEspacos from './TodosEspacos';
import MensagemCadastroRealizado from './MensagemCadastroRealizado';
import TodosClientes from './TodosClientes';
import AlocarEspacoAoPacote from './AlocarEspacoAoPacote';
import DetalhesEpisodio from './DetalhesPacote';
import CadastroContratacaoEspaco from './CadastroContratacaoEspaco';
import CadastroContratacaoPacote from './CadastroContratacaoPacote';
import Contratacao from './Contratacao';
import Pagamento from './Pagameno';
import RegistroPagamentoPacote from './RegistroPagamentoPacote'
import RegistroPagamentoContratacao from './RegistroPagamentoContratacao';
import TodasContratacoes from './TodasContratacoes';
import TodosPagamentos from './TodosPagamentos';
import Acesso from './Acesso';
import RegistrarAcesso from './RegistrarAcesso';
import TodosAcessos from './TodosAcessos';
import Login from './Login';
import PrivateRoute from '../Components/PrivateRoute';
import MensagemContratacaoRealizada from './MensagemContratacaoRealizada';
import MensagemPagamentoRealizado from './MensagemPagamentoRealizado';
import MensagemAcessoRealizado from './MensagemAcessoRealizado';

export default class Rotas extends Component {
  render() {

    return (
      <Router>
        <Route path="/login" exact component={ Login } />
        <PrivateRoute path="/" exact component={ Home } />
        <PrivateRoute path="/cadastro" exact component={ Cadastro } />
        <PrivateRoute path="/cadastro/cliente" exact component={ CadastroCliente } />
        <PrivateRoute path="/cadastro/espaco" exact component={ CadastroEspaco } />
        <PrivateRoute path="/cadastro/pacote" exact component={ CadastroPacote } />
        <PrivateRoute path="/cadastro/espacoPacote" exact component={ AlocarEspacoAoPacote } />
        <PrivateRoute path="/pacotes" exact component={ TodosPacotes } />
        <PrivateRoute path="/espacos" exact component={ TodosEspacos } />
        <PrivateRoute path="/clientes" exact component={ TodosClientes } />
        <PrivateRoute path="/mensagemCadastroRealizado" exact component={ MensagemCadastroRealizado } />
        <PrivateRoute path="/mensagemContratacaoRealizada" exact component={ MensagemContratacaoRealizada } />
        <PrivateRoute path="/mensagemPagamentoRealizado" exact component={ MensagemPagamentoRealizado } />
        <PrivateRoute path="/mensagemAcessoRealizado" exact component={ MensagemAcessoRealizado } />
        <PrivateRoute path="/pacotes/:id" exact component={ DetalhesEpisodio } />
        <PrivateRoute path="/contratacao/pacote" exact component={ CadastroContratacaoPacote } />
        <PrivateRoute path="/contratacao/espaco" exact component={ CadastroContratacaoEspaco } />
        <PrivateRoute path="/contratacao" exact component={ Contratacao } />
        <PrivateRoute path="/contratacao/todas" exact component={ TodasContratacoes } />
        <PrivateRoute path="/pagamento" exact component={ Pagamento } />
        <PrivateRoute path="/pagamento/pacote" exact component={ RegistroPagamentoPacote } />
        <PrivateRoute path="/pagamento/contratacao" exact component={ RegistroPagamentoContratacao } />
        <PrivateRoute path="/pagamento/todos" exact component={ TodosPagamentos } />
        <PrivateRoute path="/acesso" exact component={ Acesso } />
        <PrivateRoute path="/acesso/registrar" exact component={ RegistrarAcesso } />
        <PrivateRoute path="/acesso/todos" exact component={ TodosAcessos } />
      </Router>
    );
  }
}
