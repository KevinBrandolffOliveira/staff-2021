import React, { Component } from 'react';

import CoworkingApi from '../../models/CoworkingAPI.js'
import ListaAcessos from '../../models/ListaAcessos.js';

import Layout from '../../Middler/Components/Layout'
import Header from '../../Middler/Components/Header'
import Content from '../../Middler/Components/Content'
import Col from '../../Middler/Components/Col'
import Row from '../../Middler/Components/Row'

import ButtonUi from '../../Components/ButtonUi'

import '../Css/BackGroundColor.css'
import '../Css/Content-css.css'
import '../Css/Header-css.css'
import '../Css/PosicaoNoMeio.css'
import CardAcessoUi from '../../Components/CardAcessoUi/index.js';
import { Link } from 'react-router-dom';

import { logout } from '../../Utils/auth.js';

export default class TodosAcessos extends Component {
  constructor(props) {
    super(props)
    this.coworkingApi = new CoworkingApi();
    this.state = {}
  }

  componentDidMount() {
    this.coworkingApi.buscarTodosAcessos()
      .then(arrayDeAcessos => {
        this.setState(state => { return { ...state, listaAcessos: new ListaAcessos(arrayDeAcessos) } })
      })
  }

  render() {

    const { listaAcessos } = this.state;

    return (
      !listaAcessos ?
        (<h2>Carregando...</h2>) :
        (
          <React.Fragment>
            <Layout className="BackGroundColor">
              <Header className="Header-css">
                Brandolff Coworking
              </Header>
              <Header className="Header-css">
                <Link className="LinkHeader" to={"/"}>Home</Link>
                <Link className="LinkHeader" to={"/cadastro"}>Cadastrar</Link>
                <Link className="LinkHeader" to={"/pacotes"}>Pacotes</Link>
                <Link className="LinkHeader" to={"/espacos"}>Espaços</Link>
                <Link className="LinkHeader" to={"/clientes"}>Clientes</Link>
                <Link className="LinkHeader" to={"/contratacao"}>Contratacão</Link>
                <Link className="LinkHeader" to={"/pagamento"}>Pagamentos</Link>
                <Link className="LinkHeader" to={"/acesso"}>Acesso</Link>
                <Link className="LinkHeader" onClick={logout}>Logout</Link>
              </Header>
              <Content className="Content-css">
                <Row>
                  <Col span={8}></Col>
                  <Col span={8}>
                    {
                      listaAcessos.acessos && (
                        listaAcessos.acessos.map((acesso, i) => {
                          return <CardAcessoUi
                            key={i}
                            id={acesso.id}
                            idCliente={acesso.idCliente}
                            idEspaco={acesso.idEspaco}
                            isEntrada={acesso.isEntrada}
                            data={acesso.data}
                          />
                        })
                      )
                    }
                    <div className="PosicaoNoMeio">
                      <ButtonUi
                        nome={"Voltar"}
                        link={"/acesso"}
                      />
                    </div>
                  </Col>
                  <Col span={8}></Col>
                </Row>
              </Content>
            </Layout>
          </React.Fragment>
        )
    )
  }


}
