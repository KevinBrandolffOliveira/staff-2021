import React, { Component } from 'react';

import CoworkingApi from '../../models/CoworkingAPI.js'
import ListaPacotes from '../../models/ListaPacotes';

import Layout from '../../Middler/Components/Layout'
import Header from '../../Middler/Components/Header'
import Content from '../../Middler/Components/Content'
import Col from '../../Middler/Components/Col'
import Row from '../../Middler/Components/Row'

import '../Css/BackGroundColor.css'
import '../Css/Content-css.css'
import '../Css/Header-css.css'
import CardPacoteUi from '../../Components/CardPacoteUi/index.js';
import { Link } from 'react-router-dom';

import { logout } from '../../Utils/auth.js';

export default class TodosPacotes extends Component {
  constructor(props) {
    super(props)
    this.coworkingApi = new CoworkingApi();
    this.state = {}
  }

  componentDidMount() {
    this.coworkingApi.buscarTodosPacotes()
      .then(arrayDeEp => {
        this.setState(state => { return { ...state, listaPacotes: new ListaPacotes(arrayDeEp) } })
      })
  }

  render() {

    const { listaPacotes } = this.state;

    return (
      !listaPacotes ?
        (<h2>Carregando...</h2>) :
        (
          <React.Fragment>
            <Layout className="BackGroundColor">
              <Header className="Header-css">
                Brandolff Coworking
              </Header>
              <Header className="Header-css">
                <Link className="LinkHeader" to={"/"}>Home</Link>
                <Link className="LinkHeader" to={"/cadastro"}>Cadastrar</Link>
                <Link className="LinkHeader" to={"/pacotes"}>Pacotes</Link>
                <Link className="LinkHeader" to={"/espacos"}>Espaços</Link>
                <Link className="LinkHeader" to={"/clientes"}>Clientes</Link>
                <Link className="LinkHeader" to={"/contratacao"}>Contratacão</Link>
                <Link className="LinkHeader" to={"/pagamento"}>Pagamentos</Link>
                <Link className="LinkHeader" to={"/acesso"}>Acesso</Link>
                <Link className="LinkHeader" onClick={logout}>Logout</Link>
              </Header>
              <Content className="Content-css">
                <Row>
                  <Col span={8}></Col>
                  <Col span={8}>
                    {
                      listaPacotes.pacotes && (
                        listaPacotes.pacotes.map((pacote, i) => {
                          return <CardPacoteUi
                            key={i}
                            id={pacote.id}
                            valor={pacote.valor}
                          />
                        })
                      )
                    }
                  </Col>
                  <Col span={8}></Col>
                </Row>
              </Content>
            </Layout>
          </React.Fragment>
        )
    )
  }


}
