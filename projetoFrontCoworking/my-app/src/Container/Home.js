import React, { Component } from 'react';

import Layout from '../Middler/Components/Layout'
import Header from '../Middler/Components/Header'
import Content from '../Middler/Components/Content'
import Col from '../Middler/Components/Col'
import Row from '../Middler/Components/Row'

import './Css/BackGroundColor.css'
import './Css/LogoHome.css'
import './Css/LinksHeader.css'
import './Css/Content-css.css'
import './Css/Header-css.css'
import './Home.css';
import { Link } from 'react-router-dom';

import { logout } from '../Utils/auth';

import image from "../Img/test4.png"

export default class Home extends Component {

  render() {

    return (
      <React.Fragment>
        <Layout className="BackGroundColorHome">
          <Header className="Header-css">
            Brandolff Coworking
          </Header>
          <Header className="Header-css">
            <Link className="LinkHeader" to={"/"}>Home</Link>
            <Link className="LinkHeader" to={"/cadastro"}>Cadastrar</Link>
            <Link className="LinkHeader" to={"/pacotes"}>Pacotes</Link>
            <Link className="LinkHeader" to={"/espacos"}>Espaços</Link>
            <Link className="LinkHeader" to={"/clientes"}>Clientes</Link>
            <Link className="LinkHeader" to={"/contratacao"}>Contratacão</Link>
            <Link className="LinkHeader" to={"/pagamento"}>Pagamentos</Link>
            <Link className="LinkHeader" to={"/acesso"}>Acesso</Link>
            <Link className="LinkHeader" onClick={logout}>Logout</Link>
          </Header>
          <Content className="Content-css">
            <Row>
              <Col span={24} className="escritaHome">
                <h2>Sistema de gerenciamento de coworking</h2>
                <h2>Desenvolvido por Kevin Brandolff</h2>
              </Col>
            </Row>
            <Row>
              <Col span={24} className="positionLogo">
                <img src={image} className="LogoHome"></img>
              </Col>
            </Row>
          </Content>
        </Layout>
      </React.Fragment>
    );
  }
}