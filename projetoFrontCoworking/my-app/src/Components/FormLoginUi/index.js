import React, { useState } from 'react';

import Form from '../../Middler/Components/Form';
import Input from '../../Middler/Components/Input';
import BotaoSubmitUi from '../ButtonSubmitFormUi';

import '../../Container/Css/BotaoCadastrar-css.css'
import { Redirect } from 'react-router-dom';

const FormLoginUi = ({ metodoCadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    console.log(values)
    metodoCadastrar(values);
    setRedirect(true);
  };

  return (
    (redirect === false) ?
      (<React.Fragment>
        <Form onFinish={onFinish}>
          <Form.Item label="Username" name="username" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Password" name="password" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item className="BotaoCadastrar-css">
            <BotaoSubmitUi
              nome={"Login"}
            />
          </Form.Item>
        </Form>
      </React.Fragment >)
      :
      (<Redirect to='/mensagemCadastroRealizado' />)
  )

}

export default FormLoginUi;