import React from 'react';

import Card from '../../Middler/Components/Card'
import Meta from '../../Middler/Components/Meta'
import Avatar from '../../Middler/Components/Avatar'

import { AntDesignOutlined } from '@ant-design/icons';

import '../../Container/Css/CardsCenter.css'
import '../../Container/Css/PosicaoNoMeio.css'

const CardContratacaoContratoUi = ({ id, espaco, cliente, tipoContratacao, quantidade, desconto, prazo, valor }) =>
  <React.Fragment>
    <Card className="CardCenter" style={{ width: 300, marginTop: 16 }}>
      <Meta
        avatar={
          <Avatar 
          size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100 }}
          icon={<AntDesignOutlined />} 
          />
        }
        title= {id}
        description={`espaço: ${espaco.nome}
          espaco id: ${espaco.id}
          cliente: ${cliente.nome}
          cliente cpf: ${cliente.cpf}
          tipo contratação: ${tipoContratacao}
          quantidade: ${quantidade}
          desconto: ${desconto}
          prazo: ${prazo}
          valor: ${valor}`}
      />
    </Card>
  </React.Fragment>

export default CardContratacaoContratoUi;