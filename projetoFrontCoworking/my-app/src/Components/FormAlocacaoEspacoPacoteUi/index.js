import React, { useState } from 'react';

import Form from '../../Middler/Components/Form';
import Input from '../../Middler/Components/Input';
import BotaoSubmitUi from '../ButtonSubmitFormUi';
import BotaoUi from '../ButtonUi';
import Select from '../../Middler/Components/Select'
import Option from '../../Middler/Components/Option'

import '../../Container/Css/BotaoCadastrar-css.css'
import { Redirect } from 'react-router-dom';

const FormAlocacaoEspacoPacoteUi = ({ metodoCadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    console.log(values)
    metodoCadastrar(values);
    setRedirect(true);
  };

  return (
    (redirect === false) ?
      (<React.Fragment>
        <Form onFinish={onFinish}>
          <Form.Item label="Id Pacote" name="idPacote" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Id Espaço" name="idEspaco" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Tipo Contratacao" name="tipoContratacao" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Select
              placeholder="Selecione uma opção"
              allowClear
            >
              <Option value="minutos">Minutos</Option>
              <Option value="horas">Horas</Option>
              <Option value="turnos">Turnos</Option>
              <Option value="diarias">Diárias</Option>
              <Option value="semanas">Semanas</Option>
              <Option value="meses">Mensal</Option>
            </Select>
          </Form.Item>
          <Form.Item label="Quantidade" name="quantidade" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Prazo" name="prazo" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <BotaoUi
            nome={"Voltar"}
            link={"/cadastro"}
          />
          <Form.Item className="BotaoCadastrar-css">
            <BotaoSubmitUi
              nome={"Alocar"}
            />
          </Form.Item>
        </Form>
      </React.Fragment >)
      :
      (<Redirect to='/mensagemCadastroRealizado' />)
  )

}

export default FormAlocacaoEspacoPacoteUi;