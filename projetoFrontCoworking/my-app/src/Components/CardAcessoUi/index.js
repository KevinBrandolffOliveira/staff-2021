import React from 'react';

import Card from '../../Middler/Components/Card'
import Meta from '../../Middler/Components/Meta'
import Avatar from '../../Middler/Components/Avatar'

import { AntDesignOutlined } from '@ant-design/icons';

import '../../Container/Css/CardsCenter.css'
import '../../Container/Css/PosicaoNoMeio.css'

const CardAcessoUi = ({ id, idCliente, idEspaco, isEntrada, data,  }) =>
  <React.Fragment>
    <Card className="CardCenter" style={{ width: 300, marginTop: 16 }}>
      <Meta
        avatar={
          <Avatar 
          size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100 }}
          icon={<AntDesignOutlined />} 
          />
        }
        title= {`id acesso: ${id}`}
        description={`id cliente: ${idCliente}
          id espaço: ${idEspaco}
          É Entrada: ${isEntrada}
          Data: ${data}`}
      />
    </Card>
  </React.Fragment>

export default CardAcessoUi;