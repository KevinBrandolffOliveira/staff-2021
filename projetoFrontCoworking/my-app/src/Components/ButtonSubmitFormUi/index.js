import React from 'react';

import { Link } from 'react-router-dom';

import Button from '../../Middler/Components/Button'

const ButtonSubmitFormUi = ({ metodo, nome, link }) => 
  <React.Fragment>
    <Button type="primary" htmlType="submit" >
      { link ? <Link to={ link } >{ nome }</Link> : nome }
    </Button>
  </React.Fragment>

export default ButtonSubmitFormUi;