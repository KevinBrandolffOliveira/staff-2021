import React, { useState } from 'react';

import Form from '../../Middler/Components/Form';
import Input from '../../Middler/Components/Input';
import BotaoSubmitUi from '../ButtonSubmitFormUi';
import BotaoUi from '../ButtonUi';
import Radio from '../../Middler/Components/Radio'

import '../../Container/Css/BotaoCadastrar-css.css'
import { Redirect } from 'react-router-dom';

const FormRegistroAcessoUi = ({ metodoCadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    console.log(values)
    metodoCadastrar(values);
    setRedirect(true);
  };

  return (
    (redirect === false) ?
      (<React.Fragment>
        <Form onFinish={onFinish}>
          <Form.Item label="Id Cliente" name="idCliente" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Id Espaço" name="idEspaco" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item 
            name="isEntrada"
            rules={[{ required: true, message: 'selecione uma opção!' }]}
          >
            <Radio.Group>
              <Radio.Button value="true">Entrada</Radio.Button>
              <Radio.Button value="false">Saída</Radio.Button>
            </Radio.Group>
          </Form.Item>
          <BotaoUi
            nome={"Voltar"}
            link={"/acesso"}
          />
          <Form.Item className="BotaoCadastrar-css">
            <BotaoSubmitUi
              nome={"Registrar"}
            />
          </Form.Item>
        </Form>
      </React.Fragment >)
      :
      (<Redirect to='/mensagemAcessoRealizado' />)
  )

}

export default FormRegistroAcessoUi;