import React from 'react';

import Card from '../../Middler/Components/Card'
import Meta from '../../Middler/Components/Meta'
import Avatar from '../../Middler/Components/Avatar'

import { AntDesignOutlined } from '@ant-design/icons';

import '../../Container/Css/CardsCenter.css'
import '../../Container/Css/PosicaoNoMeio.css'

const CardContratacaoPacoteUi = ({ id, cliente, pacote, quantidade }) =>
  <React.Fragment>
    <Card className="CardCenter" style={{ width: 300, marginTop: 16 }}>
      <Meta
        avatar={
          <Avatar 
          size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100 }}
          icon={<AntDesignOutlined />} 
          />
        }
        title= {id}
        description={`
          cliente: ${cliente.nome}
          cliente cpf: ${cliente.cpf}
          pacote id: ${pacote.id}
          pacote valor: ${pacote.valor}
          quantidade: ${quantidade}`}
      />
    </Card>
  </React.Fragment>

export default CardContratacaoPacoteUi;