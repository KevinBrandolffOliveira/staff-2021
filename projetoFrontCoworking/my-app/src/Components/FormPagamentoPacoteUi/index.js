import React, { useState } from 'react';

import Form from '../../Middler/Components/Form';
import Input from '../../Middler/Components/Input';
import BotaoSubmitUi from '../ButtonSubmitFormUi';
import BotaoUi from '../ButtonUi';
import Select from '../../Middler/Components/Select'
import Option from '../../Middler/Components/Option'

import '../../Container/Css/BotaoCadastrar-css.css'
import { Redirect } from 'react-router-dom';

const FormPagamentoPacoteUi = ({ metodoCadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    console.log(values)
    metodoCadastrar(values);
    setRedirect(true);
  };

  return (
    (redirect === false) ?
      (<React.Fragment>
        <Form onFinish={onFinish}>
          <Form.Item label="Id ClientePacote" name="idClientePacote" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Tipo Pagamento" name="tipoPagamento" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Select
              placeholder="Selecione uma opção"
              allowClear
            >
              <Option value="debito">Débito</Option>
              <Option value="credito">Crédito</Option>
              <Option value="dinheiro">Dinheiro</Option>
              <Option value="transferencia">Transferência</Option>
              </Select>
          </Form.Item>
          <BotaoUi
            nome={"Voltar"}
            link={"/pagamento"}
          />
          <Form.Item className="BotaoCadastrar-css">
            <BotaoSubmitUi
              nome={"Registrar Pagamento"}
            />
          </Form.Item>
        </Form>
      </React.Fragment >)
      :
      (<Redirect to='/mensagemPagamentoRealizado' />)
  )

}

export default FormPagamentoPacoteUi;