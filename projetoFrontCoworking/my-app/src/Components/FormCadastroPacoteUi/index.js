import React, { useState } from 'react';

import Form from '../../Middler/Components/Form'
import Input from '../../Middler/Components/Input'
import BotaoSubmitUi from '../ButtonSubmitFormUi';
import BotaoUi from '../ButtonUi';

import '../../Container/Css/BotaoCadastrar-css.css'
import { Redirect } from 'react-router-dom';

const FormCadastroPacoteUi = ({ metodoCadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    metodoCadastrar( values );
    setRedirect(true);
  };

  return (
    (redirect===false) ?
    ( <React.Fragment>
      <Form onFinish={onFinish}>
        <Form.Item label="Valor" name="valor" rules={[{ required: true, message: 'campo obrigatório!' }]}>
          <Input />
        </Form.Item>
        <BotaoUi
          nome={"Voltar"}
          link={"/cadastro"}
        />
        <Form.Item className="BotaoCadastrar-css">
          <BotaoSubmitUi
            nome={"Cadastrar"}
          />
        </Form.Item>
      </Form>
    </React.Fragment> ) 
    :
    ( <Redirect to='/mensagemCadastroRealizado'/> )
  )
}

export default FormCadastroPacoteUi;