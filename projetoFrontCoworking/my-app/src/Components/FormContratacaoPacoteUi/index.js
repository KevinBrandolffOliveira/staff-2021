import React, { useState } from 'react';

import Form from '../../Middler/Components/Form';
import Input from '../../Middler/Components/Input';
import BotaoSubmitUi from '../ButtonSubmitFormUi';
import BotaoUi from '../ButtonUi';

import '../../Container/Css/BotaoCadastrar-css.css'
import { Redirect } from 'react-router-dom';

const FormContratacaoPacoteUi = ({ metodoCadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    console.log(values)
    metodoCadastrar(values);
    setRedirect(true);
  };

  return (
    (redirect === false) ?
      (<React.Fragment>
        <Form onFinish={onFinish}>
          <Form.Item label="Id Cliente" name="idCliente" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Id Pacote" name="idPacote" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Quantidade" name="quantidade" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <BotaoUi
            nome={"Voltar"}
            link={"/contratacao"}
          />
          <Form.Item className="BotaoCadastrar-css">
            <BotaoSubmitUi
              nome={"Contratar"}
            />
          </Form.Item>
        </Form>
      </React.Fragment >)
      :
      (<Redirect to='/mensagemContratacaoRealizada' />)
  )

}

export default FormContratacaoPacoteUi;