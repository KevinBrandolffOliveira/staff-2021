import React, { useState } from 'react';

import Form from '../../Middler/Components/Form';
import Input from '../../Middler/Components/Input';
import BotaoSubmitUi from '../ButtonSubmitFormUi';
import BotaoUi from '../ButtonUi';
import DatePicker from '../../Middler/Components/DatePicker';

import '../../Container/Css/BotaoCadastrar-css.css'
import { Redirect } from 'react-router-dom';

const FormCadastroClienteUi = ({ metodoCadastrar }) => {

  const [redirect, setRedirect] = useState(false);

  const onFinish = (values) => {
    metodoCadastrar(values);
    setRedirect(true);
  };

  return (
    (redirect === false) ?
      (<React.Fragment>
        <Form onFinish={onFinish}>
          <Form.Item label="Nome" name="nome" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="CPF" name="cpf" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Email" name="email" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Telefone" name="telefone" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Data de nascimento" name="dataNascimento" rules={[{ required: true, message: 'campo obrigatório!' }]}>
            <DatePicker />
          </Form.Item>
          <BotaoUi
            nome={"Voltar"}
            link={"/cadastro"}
          />
          <Form.Item className="BotaoCadastrar-css">
            <BotaoSubmitUi
              nome={"Cadastrar"}
            />
          </Form.Item>
        </Form>
      </React.Fragment>)
      :
      (<Redirect to='/mensagemCadastroRealizado' />)
  )
}

export default FormCadastroClienteUi;