import React from 'react';

import Card from '../../Middler/Components/Card'
import Meta from '../../Middler/Components/Meta'
import Avatar from '../../Middler/Components/Avatar'

import { UserOutlined } from '@ant-design/icons';

import '../../Container/Css/CardsCenter.css'
import '../../Container/Css/PosicaoNoMeio.css'

const CardClienteUi = ({ nome, id, cpf, dataNascimento, celular, email }) =>
  <React.Fragment>
    <Card className="CardCenter" style={{ width: 300, marginTop: 16 }}>
      <Meta
        avatar={
          <Avatar 
          size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100 }}
          icon={<UserOutlined />} 
          />
        }
        title= {nome}
        description={`id: ${id}
          cpf: ${cpf}             
          data de nascimento: ${dataNascimento}
          celular: ${celular}
          email: ${email}`}
      />
    </Card>
  </React.Fragment>

export default CardClienteUi;