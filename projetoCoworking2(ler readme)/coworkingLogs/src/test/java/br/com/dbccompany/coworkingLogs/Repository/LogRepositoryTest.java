package br.com.dbccompany.coworkingLogs.Repository;

import br.com.dbccompany.coworkingLogs.Collection.LogCollection;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import static org.junit.jupiter.api.Assertions.*;

@DataMongoTest
public class LogRepositoryTest {

    @Autowired
    private LogRepository repository;

    @Test
    public void salvarLog(){
        LogCollection log = new LogCollection();
        log.setCodigo("232133");
        log.setDate("21/23/2113");
        log.setTipo("tipadoo");
        log.setDescricao("nada q faco da certo");
        repository.insert(log);
        assertTrue( repository.findAllByCodigo("232133").size() > 0);
        assertTrue( repository.findAllByTipo("tipadoo").size() > 0);
    }

}
