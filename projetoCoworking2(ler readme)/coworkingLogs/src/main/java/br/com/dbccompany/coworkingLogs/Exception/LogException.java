package br.com.dbccompany.coworkingLogs.Exception;

public class LogException extends Exception{

    private String mensagem;

    public LogException(String mensagem){
        this.mensagem = mensagem;
    }

    public String getMensagem(){
        return this.mensagem;
    }

}
