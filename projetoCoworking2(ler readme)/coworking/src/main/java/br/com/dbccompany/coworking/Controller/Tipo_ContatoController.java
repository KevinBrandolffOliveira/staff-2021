package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.Saldo_ClienteDTO;
import br.com.dbccompany.coworking.DTO.Tipo_ContatoDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Service.Saldo_ClienteService;
import br.com.dbccompany.coworking.Service.Tipo_ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/tipo_contato")
@CrossOrigin(origins = "*")
public class Tipo_ContatoController {

    @Autowired
    private Tipo_ContatoService service;

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<Tipo_ContatoDTO> salvarTipoContato(@RequestBody Tipo_ContatoDTO tipoContato){
        try{
            return new ResponseEntity<>( service.salvar(tipoContato.converter()), HttpStatus.OK );
        }catch (DadosInvalidos e){
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

}
