package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.PagamentosEntity;

import java.text.ParseException;

import br.com.dbccompany.coworking.Util.Conversores;

public class PagamentosDTO {

    private Integer id;
    private Clientes_X_PacotesDTO clientePacote;
    private ContratacaoDTO contratacao;
    private String tipoPagamento;

    public PagamentosDTO() {
    }

    public PagamentosDTO(PagamentosEntity pagamentos){
        this.id = pagamentos.getId();
        if (pagamentos.getClientesPacotes() != null){
            this.clientePacote = new Clientes_X_PacotesDTO( pagamentos.getClientesPacotes() );
        }
        if (pagamentos.getContratacao() != null) {
            this.contratacao = new ContratacaoDTO( pagamentos.getContratacao() );
        }
        this.tipoPagamento = Conversores.conversorTipoPagamentoParaString( pagamentos.getTipo_pagamento() );
    }

    public PagamentosEntity converter() throws ParseException {
        PagamentosEntity pagamentos = new PagamentosEntity();
        pagamentos.setId(this.id);
        if (this.clientePacote != null){
            pagamentos.setClientesPacotes(this.clientePacote.converter());
        }
        if (this.contratacao != null){
            pagamentos.setContratacao(this.contratacao.converter());
        }
        pagamentos.setTipo_pagamento(Conversores.conversorTipoPagamentoParaEnum( this.tipoPagamento) );
        return pagamentos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Clientes_X_PacotesDTO getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(Clientes_X_PacotesDTO clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoDTO getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoDTO contratacao) {
        this.contratacao = contratacao;
    }

    public String getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(String tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
