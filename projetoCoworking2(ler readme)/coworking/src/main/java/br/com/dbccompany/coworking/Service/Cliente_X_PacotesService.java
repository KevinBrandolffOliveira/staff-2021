package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.Clientes_X_PacotesDTO;
import br.com.dbccompany.coworking.Entity.Clientes_X_PacotesEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.Cliente_X_PacotesRepository;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class Cliente_X_PacotesService {

    @Autowired
    private Cliente_X_PacotesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Clientes_X_PacotesDTO salvar(Clientes_X_PacotesEntity clientePacote ) throws DadosInvalidos {
        try{
            Clientes_X_PacotesEntity clientePacoteNovo = repository.save(clientePacote);
            return new Clientes_X_PacotesDTO(clientePacoteNovo);
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public Clientes_X_PacotesDTO editar( Clientes_X_PacotesEntity clientePacote, int id ) throws DadosInvalidos {
        try{
            clientePacote.setId(id);
            return new Clientes_X_PacotesDTO(this.salvarEEditar( clientePacote ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private Clientes_X_PacotesEntity salvarEEditar( Clientes_X_PacotesEntity clientePacote ){
        return repository.save( clientePacote );
    }

    public void delete(int id) throws IdNaoEncontrado {
        try{
            repository.deleteById(id);
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<Clientes_X_PacotesDTO> trazerTodos() throws ObjetoNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAll());
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new ObjetoNaoEncontrado(e.getMessage());
        }
    }

    public Clientes_X_PacotesDTO trazerPorId(int id) throws IdNaoEncontrado {
        try{
            return new Clientes_X_PacotesDTO( repository.findById(id) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<Clientes_X_PacotesDTO> trazerTodosPorClienteId(int id_cliente) throws IdNaoEncontrado {
        try{
            return converterListaParaDTO( repository.findAllByClienteId(id_cliente) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<Clientes_X_PacotesDTO> trazerTodosPorPacoteId(int id_pacote) throws IdNaoEncontrado {
        try{
            return converterListaParaDTO( repository.findAllByPacoteId(id_pacote) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<Clientes_X_PacotesDTO> converterListaParaDTO(List<Clientes_X_PacotesEntity> ClientePacote){
        List<Clientes_X_PacotesDTO> listaDTO = new ArrayList<>();
        for(Clientes_X_PacotesEntity ClientePacoteAux : ClientePacote){
            listaDTO.add( new Clientes_X_PacotesDTO(ClientePacoteAux) );
        }
        return listaDTO;
    }
}
