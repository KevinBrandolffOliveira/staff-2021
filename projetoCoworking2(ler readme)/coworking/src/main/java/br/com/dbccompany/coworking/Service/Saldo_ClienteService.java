package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.Saldo_ClienteDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.Saldo_ClienteRepository;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class Saldo_ClienteService {

    @Autowired
    private Saldo_ClienteRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Saldo_ClienteDTO salvar(Saldo_ClienteEntity saldoCliente ) throws DadosInvalidos {
        try{
            Saldo_ClienteEntity saldoClienteNovo = repository.save(saldoCliente);
            return new Saldo_ClienteDTO(saldoClienteNovo);
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public Saldo_ClienteDTO editar( Saldo_ClienteEntity saldoCliente, Saldo_ClienteId id ) throws DadosInvalidos {
        try{
            saldoCliente.setId(id);
            return new Saldo_ClienteDTO(this.salvarEEditar( saldoCliente ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private Saldo_ClienteEntity salvarEEditar( Saldo_ClienteEntity saldoCliente ){
        return repository.save( saldoCliente );
    }

    public void delete(Saldo_ClienteId id) throws IdNaoEncontrado {
        try{
            repository.deleteById(id);
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<Saldo_ClienteDTO> trazerTodos() throws ObjetoNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAll());
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new ObjetoNaoEncontrado(e.getMessage());
        }
    }

    public Saldo_ClienteDTO trazerPorId(Saldo_ClienteId id) throws IdNaoEncontrado {
        try{
            Optional<Saldo_ClienteEntity> e = repository.findById(id);
            if(e.isPresent()) {
                return new Saldo_ClienteDTO( repository.findById(id).get() );
            }
            return null;
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<Saldo_ClienteDTO> trazerTodosPorVencimento(Date vencimento) throws DadosInvalidos {
        try{
            return converterListaParaDTO(repository.findAllByVencimento(vencimento));
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private List<Saldo_ClienteDTO> converterListaParaDTO(List<Saldo_ClienteEntity> saldoCliente){
        List<Saldo_ClienteDTO> listaDTO = new ArrayList<>();
        for(Saldo_ClienteEntity saldoClienteAux : saldoCliente){
            listaDTO.add( new Saldo_ClienteDTO(saldoClienteAux) );
        }
        return listaDTO;
    }
}
