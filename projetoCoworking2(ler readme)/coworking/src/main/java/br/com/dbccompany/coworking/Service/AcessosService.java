package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.AcessoSaldoDTO;
import br.com.dbccompany.coworking.DTO.AcessosDTO;
import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.Saldo_ClienteEntity;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.DataNaoEncontrada;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.AcessosRepository;
import br.com.dbccompany.coworking.Repository.Saldo_ClienteRepository;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AcessosService {

    @Autowired
    private AcessosRepository repository;
    @Autowired
    private Saldo_ClienteRepository repositorySaldoCliente;

    @Transactional( rollbackFor = Exception.class)
    public AcessosDTO salvar(AcessosEntity acesso ) throws DadosInvalidos {
        try{
            boolean existe = repositorySaldoCliente.existsById(new Saldo_ClienteId(acesso.getSaldoCliente().getId().getId_cliente(), acesso.getSaldoCliente().getId().getId_espaco()));
            if (existe){
                Saldo_ClienteEntity saldoCliente = repositorySaldoCliente.findById(new Saldo_ClienteId(acesso.getSaldoCliente().getId().getId_cliente(), acesso.getSaldoCliente().getId().getId_espaco())).get();
                acesso.setSaldoCliente(saldoCliente);
            }
            if(acesso.getIs_Entrada()){
                if(acesso.getData() != null){
                    return entrar(acesso);
                }else{
                    acesso.setData(LocalDateTime.now());
                    return entrar(acesso);
                }
            }else{
                return sair(acesso);
            }
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private AcessosDTO entrar (AcessosEntity acesso ) throws DadosInvalidos {
        if(acesso.getSaldoCliente().getVencimento().isAfter(LocalDateTime.now())){
            if(acesso.getSaldoCliente().getQuantidadeEmMinutos() > 0){
                System.out.println("saldo: " + acesso.getSaldoCliente().getQuantidade() + acesso.getSaldoCliente().getTipo_contratacao());
                return new AcessosDTO( salvarEEditar(acesso) );
            }else{
                System.out.println("saldo insuficiente");
            }
        }else{
            acesso.getSaldoCliente().setQuantidade(0);
        }
        return null;
    }

    private AcessosDTO sair ( AcessosEntity acesso ) throws DadosInvalidos {
        int ultimoAcesso = acesso.getSaldoCliente().getAcessos().size();
        if(acesso.getSaldoCliente().getAcessos().get(ultimoAcesso-1).getIs_Entrada()){
            LocalDateTime joinDate = acesso.getSaldoCliente().getAcessos().get(ultimoAcesso-1).getData();
            LocalDateTime exitDate = null;
            if (acesso.getData() == null){
                exitDate = LocalDateTime.now();
                acesso.setData((exitDate));
            }else{
                exitDate = acesso.getData();
            }
            Duration tempoUsado = Duration.between(joinDate, exitDate);
            acesso.getSaldoCliente().setQuantidadeRecebendoEmMinutos( acesso.getSaldoCliente().getQuantidadeEmMinutos() - (int)tempoUsado.toMinutes());
            AcessosDTO acessoDTO = new AcessosDTO(salvarEEditar(acesso));
            acessoDTO.setAviso("Saldo restante: " + acesso.getSaldoCliente().getQuantidade() + " " +acesso.getSaldoCliente().getTipo_contratacao());
            return acessoDTO;
        }else{
            return null;
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public AcessosDTO editar( AcessosEntity acesso, int id ) throws DadosInvalidos {
        try{
            acesso.setId(id);
            return new AcessosDTO(this.salvarEEditar( acesso ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private AcessosEntity salvarEEditar( AcessosEntity acesso ){
        return repository.save( acesso );
    }

    public void delete(int id) throws IdNaoEncontrado {
        try{
            repository.deleteById(id);
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public AcessosDTO trazerPorId(int id) throws IdNaoEncontrado {
        try {
            return new AcessosDTO(repository.findById(id));
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<AcessosDTO> trazerTodos() throws ObjetoNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAll());
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new ObjetoNaoEncontrado(e.getMessage());
        }
    }

    public List<AcessosDTO> trazerTodosPorSaldoClienteId(Saldo_ClienteId id) throws IdNaoEncontrado {
        try{
            return converterListaParaDTO(repository.findAllBySaldoClienteId(id));
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<AcessosDTO> trazerTodosPorData(Date data) throws DataNaoEncontrada {
        try{
            return converterListaParaDTO(repository.findAllByData(data));
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new DataNaoEncontrada();
        }
    }

    private List<AcessosDTO> converterListaParaDTO(List<AcessosEntity> acesso){
        List<AcessosDTO> listaDTO = new ArrayList<>();
        for(AcessosEntity acessoAux : acesso){
            listaDTO.add( new AcessosDTO(acessoAux) );
        }
        return listaDTO;
    }

}
