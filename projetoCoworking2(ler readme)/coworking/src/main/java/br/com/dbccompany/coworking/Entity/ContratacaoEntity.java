package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;

import javax.persistence.*;
import java.util.List;

@Entity
public class ContratacaoEntity {

    @Id
    @SequenceGenerator( name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_espaco")
    private EspacoEntity espaco;

    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private ClienteEntity cliente;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Tipo_ContratacaoEnum tipo_contratacao;

    @Column(nullable = false)
    private Integer quantidade;

    @Column(nullable = true)
    private Double desconto;

    @Column(nullable = false)
    private Integer prazo;

    @OneToMany(mappedBy = "contratacao")
    private List<PagamentosEntity> pagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public Tipo_ContratacaoEnum getTipo_contratacao() {
        return tipo_contratacao;
    }

    public void setTipo_contratacao(Tipo_ContratacaoEnum tipo_contratacao) {
        this.tipo_contratacao = tipo_contratacao;
    }

    public Integer getQuantidade() {
        if(tipo_contratacao == Tipo_ContratacaoEnum.MINUTOS){
            return quantidade;
        }else if(tipo_contratacao == Tipo_ContratacaoEnum.HORAS){
            return quantidade * 60;
        }else if(tipo_contratacao == Tipo_ContratacaoEnum.TURNOS){
            return quantidade * 60 * 5;
        }else if(tipo_contratacao == Tipo_ContratacaoEnum.DIARIAS){
            return quantidade * 60 * 5 * 2;
        }else if(tipo_contratacao == Tipo_ContratacaoEnum.SEMANAS){
            return quantidade * 60 * 5 * 2 * 7;
        }else if(tipo_contratacao == Tipo_ContratacaoEnum.MESES){
            return quantidade * 60 * 5 * 2 * 7 * 4;
        }
        return null;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public List<PagamentosEntity> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentosEntity> pagamento) {
        this.pagamento = pagamento;
    }
}
