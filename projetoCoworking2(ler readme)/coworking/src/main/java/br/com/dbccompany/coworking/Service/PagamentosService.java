package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.PagamentosDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Exception.IdNaoEncontrado;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.*;
import br.com.dbccompany.coworking.Util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentosService {

    @Autowired
    private PagamentosRepository repository;
    @Autowired
    private Cliente_X_PacotesRepository repositoryClientePacote;
    @Autowired
    private ContratacaoRepository repositoryContratacao;
    @Autowired
    private Saldo_ClienteRepository repositorySaldoCliente;
    @Autowired
    private Espacos_X_PacotesRepository repositoryEspacoPacote;
    @Autowired
    private PacotesRepository repositoryPacotes;

    @Transactional( rollbackFor = Exception.class)
    public PagamentosDTO salvar(PagamentosEntity pagamento ) throws DadosInvalidos {
        try{
            if(pagamento.getClientesPacotes() != null ){
                List<Espacos_X_PacotesEntity> listEspacoPacote = repositoryEspacoPacote.findAllByPacoteId(pagamento.getClientesPacotes().getPacote().getId());
                for (int i = 0; i < listEspacoPacote.size(); i++){
                    boolean existeSaldoCliente = repositorySaldoCliente.existsById(new Saldo_ClienteId(pagamento.getClientesPacotes().getCliente().getId(), repositoryPacotes.findById(pagamento.getClientesPacotes().getPacote().getId()).get().getEspacosPacotes().get(i).getEspaco().getId() ));
                    if(existeSaldoCliente == true){
                        Saldo_ClienteEntity saldoCliente = repositorySaldoCliente.findById(new Saldo_ClienteId(pagamento.getClientesPacotes().getCliente().getId(), repositoryPacotes.findById(pagamento.getClientesPacotes().getPacote().getId()).get().getEspacosPacotes().get(i).getEspaco().getId())).get();
                        saldoCliente.setQuantidade(listEspacoPacote.get(i).getQuantidade());
                        saldoCliente.setVencimento(LocalDateTime.now().plusDays(listEspacoPacote.get(i).getPrazo()));
                        saldoCliente.setTipo_contratacao(listEspacoPacote.get(i).getTipo_contratacao());
                        repositorySaldoCliente.save(saldoCliente);
                    }else{
                        Saldo_ClienteEntity saldoCliente = new Saldo_ClienteEntity();
                        saldoCliente.setId(new Saldo_ClienteId(pagamento.getClientesPacotes().getCliente().getId(), repositoryPacotes.findById(pagamento.getClientesPacotes().getPacote().getId()).get().getEspacosPacotes().get(i).getEspaco().getId()));
                        saldoCliente.setQuantidade(listEspacoPacote.get(i).getQuantidade());
                        saldoCliente.setVencimento(LocalDateTime.now().plusDays(listEspacoPacote.get(i).getPrazo()));
                        saldoCliente.setTipo_contratacao(listEspacoPacote.get(i).getTipo_contratacao());
                        saldoCliente.setCliente(pagamento.getClientesPacotes().getCliente());
                        saldoCliente.setEspaco(listEspacoPacote.get(i).getEspaco());
                        repositorySaldoCliente.save(saldoCliente);
                    }
                }
                return new PagamentosDTO( salvarEEditar(pagamento) );
            }else if(pagamento.getContratacao() != null){
                boolean existeSaldoCliente = repositorySaldoCliente.existsById(new Saldo_ClienteId(pagamento.getContratacao().getCliente().getId(), pagamento.getContratacao().getEspaco().getId()));
                if ( existeSaldoCliente == true){
                    Saldo_ClienteEntity saldoCliente = repositorySaldoCliente.findById(new Saldo_ClienteId(pagamento.getContratacao().getCliente().getId(), pagamento.getContratacao().getEspaco().getId())).get();
                    saldoCliente.setQuantidade(pagamento.getContratacao().getQuantidade());
                    saldoCliente.setVencimento(LocalDateTime.now().plusDays(pagamento.getContratacao().getPrazo()));
                    saldoCliente.setTipo_contratacao(pagamento.getContratacao().getTipo_contratacao());
                    repositorySaldoCliente.save(saldoCliente);
                }else {
                    Saldo_ClienteEntity saldoCliente = new Saldo_ClienteEntity();
                    saldoCliente.setId(new Saldo_ClienteId(pagamento.getContratacao().getCliente().getId(), pagamento.getContratacao().getEspaco().getId()));
                    saldoCliente.setQuantidade(pagamento.getContratacao().getQuantidade());
                    saldoCliente.setVencimento(LocalDateTime.now().plusDays(pagamento.getContratacao().getPrazo()));
                    saldoCliente.setTipo_contratacao(pagamento.getContratacao().getTipo_contratacao());
                    saldoCliente.setCliente(pagamento.getContratacao().getCliente());
                    saldoCliente.setEspaco(pagamento.getContratacao().getEspaco());
                    repositorySaldoCliente.save(saldoCliente);
                }
                return new PagamentosDTO(salvarEEditar(pagamento));
            }
            return null;
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public PagamentosDTO editar( PagamentosEntity pagamento, int id ) throws DadosInvalidos {
        try{
            pagamento.setId(id);
            return new PagamentosDTO(this.salvarEEditar( pagamento ));
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new DadosInvalidos(e.getMessage());
        }
    }

    private PagamentosEntity salvarEEditar( PagamentosEntity pagamento ){
        return repository.save( pagamento );
    }

    public void delete(int id) throws IdNaoEncontrado {
        try{
            repository.deleteById(id);
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public List<PagamentosDTO> trazerTodos() throws ObjetoNaoEncontrado {
        try{
            return converterListaParaDTO( repository.findAll() );
        }catch (Exception e){
            LogUtil.lanca400(e);
            throw new ObjetoNaoEncontrado(e.getMessage());
        }
    }

    public PagamentosDTO trazerPorId(int id) throws IdNaoEncontrado {
        try{
            return new PagamentosDTO( repository.findById(id) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public PagamentosDTO trazerPorIdClientePacote(int id_ClientePacote) throws IdNaoEncontrado {
        try{
            return new PagamentosDTO( repository.findByClientesPacotesId(id_ClientePacote) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    public PagamentosDTO trazerPorIdContratacao(int id_Contratacao) throws IdNaoEncontrado {
        try{
            return new PagamentosDTO( repository.findByContratacaoId(id_Contratacao) );
        }catch (Exception e){
            LogUtil.lanca404(e);
            throw new IdNaoEncontrado();
        }
    }

    private List<PagamentosDTO> converterListaParaDTO(List<PagamentosEntity> pagamento){
        List<PagamentosDTO> listaDTO = new ArrayList<>();
        for(PagamentosEntity pagamentoAux : pagamento){
            listaDTO.add( new PagamentosDTO(pagamentoAux) );
        }
        return listaDTO;
    }
}
