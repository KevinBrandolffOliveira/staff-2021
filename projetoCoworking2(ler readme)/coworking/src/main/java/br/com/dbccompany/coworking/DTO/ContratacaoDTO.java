package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.coworking.Util.Conversores;

public class ContratacaoDTO {

    private Integer id;
    private EspacoDTO espaco;
    private ClienteDTO cliente;
    private String tipoContratacao;
    private Integer quantidade;
    private Double desconto;
    private Integer prazo;
    private List<PagamentosDTO> pagamento;
    private String valor;

    public ContratacaoDTO() {
    }

    public ContratacaoDTO(ContratacaoEntity contratacao){
        this.id = contratacao.getId();
        this.espaco = new EspacoDTO( contratacao.getEspaco() );
        this.cliente = new ClienteDTO( contratacao.getCliente() );
        this.tipoContratacao = Conversores.conversorTipoContratacaoParaString( contratacao.getTipo_contratacao() );
        this.quantidade = contratacao.getQuantidade();
        this.desconto = contratacao.getDesconto();
        this.prazo = contratacao.getPrazo();
        //this.pagamento = converterListaParaDTOPagamento( contratacao.getPagamento() );
        this.valor = converterValorParaString();
    }

    public ContratacaoEntity converter() throws ParseException {
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setId(this.id);
        contratacao.setEspaco(this.espaco.converter());
        contratacao.setCliente(this.cliente.converter());
        contratacao.setTipo_contratacao(Conversores.conversorTipoContratacaoParaEnum( this.tipoContratacao ) );
        contratacao.setQuantidade(this.quantidade);
        contratacao.setDesconto(this.desconto);
        contratacao.setPrazo(this.prazo);
        contratacao.setPagamento(converterListaParaEntityPagamento());
        return contratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public String getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(String tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public List<PagamentosDTO> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentosDTO> pagamento) {
        this.pagamento = pagamento;
    }

    private List<PagamentosDTO> converterListaParaDTOPagamento(List<PagamentosEntity> pagamentos){
        if(pagamentos != null){
            List<PagamentosDTO> listaDTO = new ArrayList<>();
            for(PagamentosEntity pagamentosAux : pagamentos){
                listaDTO.add( new PagamentosDTO(pagamentosAux) );
            }
            return listaDTO;
        }
        return null;
    }

    private List<PagamentosEntity> converterListaParaEntityPagamento() throws ParseException {
        if (this.pagamento != null){
            List<PagamentosEntity> list = new ArrayList<>();
            for(PagamentosDTO listaDTO : pagamento){
                list.add(listaDTO.converter());
            }
            return list;
        }
        return null;
    }

    private double calcularValor(){
        double valor = 0;
        if (tipoContratacao == "minutos"){
            valor = quantidade * this.espaco.getValor();
        }else if (tipoContratacao == "horas"){
            valor = quantidade * this.espaco.getValor()/60;
        }else if (tipoContratacao == "turnos"){
            valor = quantidade * (this.espaco.getValor()/60)/5;
        }else if (tipoContratacao == "diarias"){
            valor = quantidade * (this.espaco.getValor()/60)/10;
        }else if (tipoContratacao == "semanas"){
            valor = quantidade * ((this.espaco.getValor()/60)/10)/5;
        }else if (tipoContratacao == "meses"){
            valor = quantidade * (((this.espaco.getValor()/60)/10)/5)/4;
        }
        return valor;
    }

    private String converterValorParaString(){
        String valorConvertido = "R$" + calcularValor();
        return valorConvertido;
    }

}
