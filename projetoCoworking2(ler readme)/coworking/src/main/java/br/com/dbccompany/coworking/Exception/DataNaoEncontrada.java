package br.com.dbccompany.coworking.Exception;

public class DataNaoEncontrada extends DadosInvalidos{

    public DataNaoEncontrada(){
        super("Data invalida, nao foi possivel realizar a operacao");
    }

}
