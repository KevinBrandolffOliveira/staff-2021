package br.com.dbccompany.coworking.DTO;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class LogDTO {

    private String tipo;
    private String descricao;
    private String date;
    private String codigo;

    public LogDTO() {
    }

    public LogDTO(String tipo, Exception e, String logMensagem, String codigo){
        this.tipo = tipo;
        this.descricao = e.getMessage() + " || " + logMensagem;
        this.date = LocalDateTime.now().toString();
        this.codigo = codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
