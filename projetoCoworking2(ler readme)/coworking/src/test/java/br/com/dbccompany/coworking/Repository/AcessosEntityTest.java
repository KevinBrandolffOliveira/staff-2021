package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Entity.Enum.Tipo_PagamentoEnum;
import br.com.dbccompany.coworking.Entity.ids.Saldo_ClienteId;
import br.com.dbccompany.coworking.Exception.DadosInvalidos;
import br.com.dbccompany.coworking.Service.AcessosService;
import br.com.dbccompany.coworking.Service.PagamentosService;
import com.sun.org.apache.bcel.internal.generic.PUSH;
import jdk.vm.ci.meta.Local;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class AcessosEntityTest {

    @Autowired
    private AcessosRepository repository;
    @Autowired
    private Saldo_ClienteRepository repositorySaldoCliente;
    @Autowired
    private ClienteRepository repositoryCliente;
    @Autowired
    private EspacoRepository repositoryEspaco;
    @Autowired
    private ContratacaoRepository repositoryContratacao;
    @Autowired
    private PagamentosRepository repositoryPagamentos;

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @Test
    public void salvarAcesso() throws ParseException {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("PedraDura");
        cliente.setCpf("12345678912");
        cliente.setData_nascimento(sdf.parse("25/09/2000"));
        repositoryCliente.save(cliente);
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("escritorio");
        espaco.setValor(20.00);
        espaco.setQtdPessoas(5);
        repositoryEspaco.save(espaco);
        Saldo_ClienteEntity saldoCliente = new Saldo_ClienteEntity();
        saldoCliente.setId(new Saldo_ClienteId(cliente.getId(), espaco.getId()));
        saldoCliente.setTipo_contratacao(Tipo_ContratacaoEnum.DIARIAS);
        saldoCliente.setQuantidade(3);
        saldoCliente.setVencimento(LocalDateTime.now());
        saldoCliente.setCliente(cliente);
        saldoCliente.setEspaco(espaco);
        repositorySaldoCliente.save(saldoCliente);
        AcessosEntity acesso = new AcessosEntity();
        acesso.setIs_Excecao(false);
        acesso.setIs_Entrada(true);
        acesso.setSaldoCliente(saldoCliente);
        acesso.setData(LocalDateTime.now());
        repository.save(acesso);
        //assertEquals(acesso, repository.findAllByData(sdf.parse("23/06/2021")).get(0));
        //assertEquals(acesso, repository.findAllBySaldoClienteId(acesso.getSaldoCliente().getId()).get(0));
        assertEquals(acesso, repository.findById(1));
        //assertEquals(acesso, repository.findAll().get(0));
        //assertEquals(acesso, repository.findByData(sdf.parse("23/06/2021")));
        //assertEquals(acesso, repository.findBySaldoClienteId(acesso.getSaldoCliente().getId()));
        //PQ ESSE KARALHO NAO FUNCIONA
    }

    @Test
    public void salvarAcesso2() throws ParseException {
        AcessosEntity acesso = new AcessosEntity();
        acesso.setIs_Entrada(true);
        acesso.setSaldoCliente(new Saldo_ClienteEntity());
        acesso.setData(LocalDateTime.now());
        repository.save(acesso);
    }

}
