package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class AcessoControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private EspacoController controllerEspaco;
    @Autowired
    private ClienteController controllerCliente;
    @Autowired
    private Tipo_ContatoController controllerTipoContato;
    @Autowired
    private ContratacaoController controllerContratacao;
    @Autowired
    private PagamentosController controllerPagamento;
    @Autowired
    private AcessosController controllerAcesso;

    /*@Test
    public void retornar200AoConsultarUmaListaDeAcessos() throws Exception {
        URI uri = new URI("/api/acessos/");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarUmAcessoDeEntradaComContratacao() throws Exception {
        Tipo_ContatoDTO tipoContatoEmail = new Tipo_ContatoDTO();
        tipoContatoEmail.setNome("email");
        ResponseEntity<Tipo_ContatoDTO> tipo_contatoDTO1 = controllerTipoContato.salvarTipoContato(tipoContatoEmail);
        Tipo_ContatoDTO tipoContatoCelular = new Tipo_ContatoDTO();
        tipoContatoCelular.setNome("celular");
        ResponseEntity<Tipo_ContatoDTO> tipo_contatoDTO2 = controllerTipoContato.salvarTipoContato(tipoContatoCelular);

        ContatoDTO contato1 = new ContatoDTO();
        contato1.setTipoContato(tipo_contatoDTO1.getBody());
        contato1.setValor("sdfff@dsfd.com");
        ContatoDTO contato2 = new ContatoDTO();
        contato2.setTipoContato(tipo_contatoDTO2.getBody());
        contato2.setValor("12415425");

        List<ContatoDTO> listContato = new ArrayList<>();
        listContato.add(contato1);
        listContato.add(contato2);

        ClienteDTO cliente = new ClienteDTO();
        cliente.setNome("kevin");
        cliente.setCpf("86048953020");
        cliente.setDataNascimento("05/12/2000");
        cliente.setContatos(listContato);
        ResponseEntity<ClienteDTO> clienteDTO = controllerCliente.salvarCliente(cliente);

        EspacoDTO espaco = new EspacoDTO();
        espaco.setNome("escritorio");
        espaco.setQtdPessoas(3);
        espaco.setValor(20.0);
        ResponseEntity<EspacoDTO> espacoDTO = controllerEspaco.salvarEspaco(espaco);

        ContratacaoDTO contratacao = new ContratacaoDTO();
        contratacao.setEspaco(espacoDTO.getBody());
        contratacao.setCliente(clienteDTO.getBody());
        contratacao.setTipoContratacao("horas");
        contratacao.setQuantidade(3);
        contratacao.setDesconto(0.0);
        contratacao.setPrazo(2);
        ResponseEntity<ContratacaoDTO> contratacaoDTO = controllerContratacao.salvarContratacao(contratacao);

        PagamentosDTO pagamento = new PagamentosDTO();
        pagamento.setContratacao(contratacaoDTO.getBody());
        pagamento.setTipoPagamento("credito");
        controllerPagamento.salvarPagamento(pagamento);

        URI uri = new URI("/api/acessos/salvar");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"id_cliente\":1,\n" +
                                "    \"id_espaco\":1,\n" +
                                "    \"is_entrada\": true\n" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

    @Test
    public void salvarUmAcessoDeSaidaComContratacao() throws Exception {

        AcessosDTO acessoEntrada = new AcessosDTO();
        acessoEntrada.setId_cliente(1);
        acessoEntrada.setId_espaco(1);
        acessoEntrada.setIs_entrada(true);
        controllerAcesso.salvarAcesso(acessoEntrada);

        URI uri = new URI("/api/acessos/salvar");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"id_cliente\":1,\n" +
                                "    \"id_espaco\":1,\n" +
                                "    \"is_entrada\": false,\n" +
                                "    \"data\": \"2021-07-01T22:13:40.343505\"\n" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
                //.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1)); PRA QUE QUE SERVE ISSO?
    }*/

}
