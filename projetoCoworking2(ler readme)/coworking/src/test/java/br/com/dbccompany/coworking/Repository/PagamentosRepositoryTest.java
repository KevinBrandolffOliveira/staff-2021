package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Entity.Enum.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Entity.Enum.Tipo_PagamentoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class PagamentosRepositoryTest {

    @Autowired
    private PagamentosRepository repository;
    @Autowired
    private Cliente_X_PacotesRepository repositoryClientePacote;
    @Autowired
    private PacotesRepository repositoryPacote;
    @Autowired
    private ClienteRepository repositoryCliente;
    @Autowired
    private ContratacaoRepository repositoryContratacao;
    @Autowired
    private EspacoRepository repositoryEspaco;

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @Test
    public void salvarPagamentosComClientePacote() throws ParseException {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("PedraDura");
        cliente.setCpf("12345678912");
        cliente.setData_nascimento(sdf.parse("25/09/2000"));
        repositoryCliente.save(cliente);
        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(200.00);
        repositoryPacote.save(pacote);
        Clientes_X_PacotesEntity clientePacote = new Clientes_X_PacotesEntity();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(3);
        repositoryClientePacote.save(clientePacote);
        PagamentosEntity pagamento = new PagamentosEntity();
        pagamento.setTipo_pagamento(Tipo_PagamentoEnum.CREDITO);
        pagamento.setClientesPacotes(clientePacote);
        repository.save(pagamento);
        assertEquals(pagamento, repository.findByClientesPacotesId(pagamento.getClientesPacotes().getId()));
    }

    @Test
    public void salvarPagamentosComContratacao() throws ParseException{
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("PedraDura");
        cliente.setCpf("12345678912");
        cliente.setData_nascimento(sdf.parse("25/09/2000"));
        repositoryCliente.save(cliente);
        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(200.00);
        repositoryPacote.save(pacote);
        Clientes_X_PacotesEntity clientePacote = new Clientes_X_PacotesEntity();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(3);
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("escritorio");
        espaco.setValor(20.00);
        espaco.setQtdPessoas(5);
        repositoryEspaco.save(espaco);
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setTipo_contratacao(Tipo_ContratacaoEnum.HORAS);
        contratacao.setQuantidade(5);
        contratacao.setDesconto(10.0);
        contratacao.setPrazo(5);
        repositoryContratacao.save(contratacao);
        PagamentosEntity pagamento = new PagamentosEntity();
        pagamento.setTipo_pagamento(Tipo_PagamentoEnum.CREDITO);
        pagamento.setContratacao(contratacao);
        repository.save(pagamento);
        assertEquals(pagamento, repository.findByContratacaoId(pagamento.getContratacao().getId()));
    }

}
