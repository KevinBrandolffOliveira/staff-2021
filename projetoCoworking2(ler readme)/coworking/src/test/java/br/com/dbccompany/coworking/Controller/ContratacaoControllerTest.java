package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.DTO.Tipo_ContatoDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class ContratacaoControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private EspacoController controllerEspaco;
    @Autowired
    private ClienteController controllerCliente;
    @Autowired
    private Tipo_ContatoController controllerTipoContato;

    /*@Test
    public void retornar200AoConsultarUmaListaDeContratacao() throws Exception {
        URI uri = new URI("/api/contratacao/");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarUmaContratacao() throws Exception {
        Tipo_ContatoDTO tipoContatoEmail = new Tipo_ContatoDTO();
        tipoContatoEmail.setNome("email");
        ResponseEntity<Tipo_ContatoDTO> tipo_contatoDTO1 = controllerTipoContato.salvarTipoContato(tipoContatoEmail);
        Tipo_ContatoDTO tipoContatoCelular = new Tipo_ContatoDTO();
        tipoContatoCelular.setNome("celular");
        ResponseEntity<Tipo_ContatoDTO> tipo_contatoDTO2 = controllerTipoContato.salvarTipoContato(tipoContatoCelular);

        ContatoDTO contato1 = new ContatoDTO();
        contato1.setTipoContato(tipo_contatoDTO1.getBody());
        contato1.setValor("sdfff@dsfd.com");
        ContatoDTO contato2 = new ContatoDTO();
        contato2.setTipoContato(tipo_contatoDTO2.getBody());
        contato2.setValor("12415425");

        List<ContatoDTO> listContato = new ArrayList<>();
        listContato.add(contato1);
        listContato.add(contato2);

        ClienteDTO cliente = new ClienteDTO();
        cliente.setNome("kevin");
        cliente.setCpf("86048953020");
        cliente.setDataNascimento("05/12/2000");
        cliente.setContatos(listContato);
        controllerCliente.salvarCliente(cliente);

        EspacoDTO espaco = new EspacoDTO();
        espaco.setNome("escritorio");
        espaco.setQtdPessoas(3);
        espaco.setValor(20.0);
        controllerEspaco.salvarEspaco(espaco);

        URI uri = new URI("/api/contratacao/salvar");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"espaco\": {\n" +
                                "        \"id\": 1,\n" +
                                "        \"nome\":\"escritorio\",\n" +
                                "        \"qtdPessoas\": 3,\n" +
                                "        \"valor\": 20\n" +
                                "    },\n" +
                                "    \"cliente\": {\n" +
                                "        \"id\": 1,\n" +
                                "        \"nome\":\"kevin\",\n" +
                                "        \"cpf\":\"86048953020\",\n" +
                                "        \"dataNascimento\": \"05/12/2000\",\n" +
                                "        \"contatos\": [\n" +
                                "            {\n" +
                                "                \"tipoContato\": {\n" +
                                "                    \"id\": 1,\n" +
                                "                    \"nome\":\"email\"\n" +
                                "                },\n" +
                                "                \"valor\":\"sdfff@dsfd.com\"\n" +
                                "            },\n" +
                                "            {\n" +
                                "                \"tipoContato\": {\n" +
                                "                    \"id\":2,\n" +
                                "                    \"nome\":\"celular\"\n" +
                                "                },\n" +
                                "            \"valor\":\"12415425\"\n" +
                                "            }\n" +
                                "        ]\n" +
                                "    },\n" +
                                "    \"tipoContratacao\": \"horas\",\n" +
                                "    \"quantidade\": 3,\n" +
                                "    \"desconto\": 0,\n" +
                                "    \"prazo\": 2\n" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }*/

}