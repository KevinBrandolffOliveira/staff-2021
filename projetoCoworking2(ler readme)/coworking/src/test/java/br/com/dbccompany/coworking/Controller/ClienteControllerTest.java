package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.Tipo_ContatoDTO;
import br.com.dbccompany.coworking.Service.ClienteService;
import br.com.dbccompany.coworking.Service.ContatoService;
import br.com.dbccompany.coworking.Service.Tipo_ContatoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
public class ClienteControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ContatoController controllerContato;
    @Autowired
    private Tipo_ContatoController controllerTipoContato;

    /*@Test
    public void retornar200AoConsultarUmaListaDeCliente() throws Exception {
        URI uri = new URI("/api/cliente/");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarUmCliente() throws Exception {
        Tipo_ContatoDTO tipoContatoEmail = new Tipo_ContatoDTO();
        tipoContatoEmail.setNome("email");
        controllerTipoContato.salvarTipoContato(tipoContatoEmail);
        Tipo_ContatoDTO tipoContatoCelular = new Tipo_ContatoDTO();
        tipoContatoCelular.setNome("celular");
        controllerTipoContato.salvarTipoContato(tipoContatoCelular);

        URI uri = new URI("/api/cliente/salvar");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"nome\":\"kevin\",\n" +
                                "    \"cpf\":\"86048953020\",\n" +
                                "    \"dataNascimento\": \"05/12/2000\",\n" +
                                "    \"contatos\": [\n" +
                                "        {\n" +
                                "           \"tipoContato\": {\n" +
                                "               \"id\": 1,\n" +
                                "               \"nome\":\"email\"\n" +
                                "            },\n" +
                                "            \"valor\":\"sdfff@dsfd.com\"\n" +
                                "        },\n" +
                                "        {\n" +
                                "           \"tipoContato\": {\n" +
                                "               \"id\":2,\n" +
                                "               \"nome\":\"celular\"\n" +
                                "            },\n" +
                                "            \"valor\":\"12415425\"\n" +
                                "        }\n" +
                                "    ]\n" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }*/
}