package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.AcessosDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.DTO.PacotesDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
public class Espacos_X_PacotesControllerTest{

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private EspacoController controllerEspaco;
    @Autowired
    private PacotesController controllerPacote;

    /*@Test
    public void retornar200AoConsultarUmaListaDeEspacoPacote() throws Exception {
        URI uri = new URI("/api/espaco_pacote/");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarUmEspacoXPacote() throws Exception {

        PacotesDTO pacote = new PacotesDTO();
        pacote.setValor(50.00);
        controllerPacote.salvarPacotes(pacote);

        EspacoDTO espaco = new EspacoDTO();
        espaco.setNome("escritorio");
        espaco.setQtdPessoas(3);
        espaco.setValor(20.0);
        controllerEspaco.salvarEspaco(espaco);

        URI uri = new URI("/api/espaco_pacote/salvar");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"espaco\":{\n" +
                                "        \"id\": 1,\n" +
                                "        \"nome\": \"escritorio\",\n" +
                                "        \"qtdPessoas\": 3,\n" +
                                "        \"valor\": 20.0,\n" +
                                "        \"contratacoes\": null,\n" +
                                "        \"espacosPacotes\": null\n" +
                                "    },\n" +
                                "    \"pacote\": {\n" +
                                "        \"id\": 1,\n" +
                                "        \"valor\": 50.0\n" +
                                "    },\n" +
                                "    \"tipoContratacao\": \"diarias\",\n" +
                                "    \"quantidade\": 3,\n" +
                                "    \"prazo\": 5\n" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }*/

}