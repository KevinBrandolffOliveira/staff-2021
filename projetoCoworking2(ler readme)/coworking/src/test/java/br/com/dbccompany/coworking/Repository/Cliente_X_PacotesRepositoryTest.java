package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.Clientes_X_PacotesEntity;
import br.com.dbccompany.coworking.Entity.PacotesEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class Cliente_X_PacotesRepositoryTest {

    @Autowired
    private Cliente_X_PacotesRepository repository;
    @Autowired
    private PacotesRepository repositoryPacote;
    @Autowired
    private ClienteRepository repositoryCliente;

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @Test
    public void salvarClientePacote() throws ParseException {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("PedraDura");
        cliente.setCpf("12345678912");
        cliente.setData_nascimento(sdf.parse("25/09/2000"));
        repositoryCliente.save(cliente);
        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(200.00);
        repositoryPacote.save(pacote);
        Clientes_X_PacotesEntity clientePacote = new Clientes_X_PacotesEntity();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(3);
        repository.save(clientePacote);
        assertEquals(clientePacote, repository.findAllByClienteId(cliente.getId()).get(0));
        assertEquals(clientePacote, repository.findAllByPacoteId(pacote.getId()).get(0));
    }

}
