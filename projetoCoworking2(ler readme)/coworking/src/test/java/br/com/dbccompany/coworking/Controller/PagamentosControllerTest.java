package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.*;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.Tipo_ContatoRepository;
import br.com.dbccompany.coworking.Service.Tipo_ContatoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class PagamentosControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private EspacoController controllerEspaco;
    @Autowired
    private ClienteController controllerCliente;
    @Autowired
    private Tipo_ContatoController controllerTipoContato;
    @Autowired
    private ContratacaoController controllerContratacao;
    @Autowired
    private PacotesController controllerPacote;
    @Autowired
    private Espacos_X_PacotesController controllerEspacoXPacote;
    @Autowired
    private Cliente_X_PacotesController controllerClienteXPacote;
    @Autowired
    private Tipo_ContatoRepository repositoryTipoContato;
    @Autowired
    private ClienteRepository repositoryCliente;

    /*@Test
    public void retornar200AoConsultarUmaListaDePagamentos() throws Exception {
        URI uri = new URI("/api/pagamentos/");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarUmPagamentoContratacao() throws Exception {
        Tipo_ContatoDTO tipoContatoEmail = new Tipo_ContatoDTO();
        tipoContatoEmail.setNome("email");
        ResponseEntity<Tipo_ContatoDTO> tipo_contatoDTO1 = controllerTipoContato.salvarTipoContato(tipoContatoEmail);
        Tipo_ContatoDTO tipoContatoCelular = new Tipo_ContatoDTO();
        tipoContatoCelular.setNome("celular");
        ResponseEntity<Tipo_ContatoDTO> tipo_contatoDTO2 = controllerTipoContato.salvarTipoContato(tipoContatoCelular);

        ContatoDTO contato1 = new ContatoDTO();
        contato1.setTipoContato(tipo_contatoDTO1.getBody());
        contato1.setValor("sdfff@dsfd.com");
        ContatoDTO contato2 = new ContatoDTO();
        contato2.setTipoContato(tipo_contatoDTO2.getBody());
        contato2.setValor("12415425");

        List<ContatoDTO> listContato = new ArrayList<>();
        listContato.add(contato1);
        listContato.add(contato2);

        ClienteDTO cliente = new ClienteDTO();
        cliente.setNome("kevin");
        cliente.setCpf("86048953020");
        cliente.setDataNascimento("05/12/2000");
        cliente.setContatos(listContato);
        ResponseEntity<ClienteDTO> clienteDTO = controllerCliente.salvarCliente(cliente);

        EspacoDTO espaco = new EspacoDTO();
        espaco.setNome("escritorio");
        espaco.setQtdPessoas(3);
        espaco.setValor(20.0);
        ResponseEntity<EspacoDTO> espacoDTO = controllerEspaco.salvarEspaco(espaco);

        ContratacaoDTO contratacao = new ContratacaoDTO();
        contratacao.setEspaco(espacoDTO.getBody());
        contratacao.setCliente(clienteDTO.getBody());
        contratacao.setTipoContratacao("horas");
        contratacao.setQuantidade(3);
        contratacao.setDesconto(0.0);
        contratacao.setPrazo(2);
        controllerContratacao.salvarContratacao(contratacao);

        URI uri = new URI("/api/pagamentos/salvar");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"contratacao\": {\n" +
                                "        \"id\":1,\n" +
                                "        \"espaco\": {\n" +
                                "            \"id\": 1,\n" +
                                "            \"nome\":\"escritorio\",\n" +
                                "            \"qtdPessoas\": 3,\n" +
                                "            \"valor\": 20\n" +
                                "        },\n" +
                                "        \"cliente\": {\n" +
                                "            \"id\": 1,\n" +
                                "            \"nome\":\"kevin\",\n" +
                                "            \"cpf\":\"86048953020\",\n" +
                                "            \"dataNascimento\": \"05/12/2000\",\n" +
                                "            \"contatos\": [\n" +
                                "                {\n" +
                                "                    \"tipoContato\": {\n" +
                                "                        \"id\": 1,\n" +
                                "                        \"nome\":\"email\"\n" +
                                "                    },\n" +
                                "                    \"valor\":\"sdfff@dsfd.com\"\n" +
                                "                },\n" +
                                "                {\n" +
                                "                    \"tipoContato\": {\n" +
                                "                        \"id\":2,\n" +
                                "                        \"nome\":\"celular\"\n" +
                                "                    },\n" +
                                "                \"valor\":\"12415425\"\n" +
                                "                }\n" +
                                "            ]\n" +
                                "        },\n" +
                                "        \"tipoContratacao\": \"horas\",\n" +
                                "        \"quantidade\": 3,\n" +
                                "        \"desconto\": 0,\n" +
                                "        \"prazo\": 2\n" +
                                "    },\n" +
                                "    \"tipoPagamento\": \"credito\"\n" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

    @Test
    public void salvarUmPagamentoClienteXPacote() throws Exception {

        ContatoDTO contato1 = new ContatoDTO();
        contato1.setTipoContato(new Tipo_ContatoDTO( repositoryTipoContato.findById(1) ) );
        contato1.setValor("sdfff@dsfd.com");
        ContatoDTO contato2 = new ContatoDTO();
        contato2.setTipoContato(new Tipo_ContatoDTO( repositoryTipoContato.findById(2) ));
        contato2.setValor("12415425");

        List<ContatoDTO> listContato = new ArrayList<>();
        listContato.add(contato1);
        listContato.add(contato2);

        ClienteDTO cliente = new ClienteDTO();
        cliente.setNome("kevin");
        cliente.setCpf("86048953021");
        cliente.setDataNascimento("05/12/2000");
        cliente.setContatos(listContato);
        ResponseEntity<ClienteDTO> clienteDTO = controllerCliente.salvarCliente(cliente);

        PacotesDTO pacote = new PacotesDTO();
        pacote.setValor(50.00);
        ResponseEntity<PacotesDTO> pacoteDTO = controllerPacote.salvarPacotes(pacote);

        Espacos_X_PacotesDTO espacoPacote = new Espacos_X_PacotesDTO();
        espacoPacote.setPacote(pacoteDTO.getBody());
        espacoPacote.setEspaco(controllerEspaco.trazerPorId(1).getBody());
        espacoPacote.setTipoContratacao("diarias");
        espacoPacote.setQuantidade(3);
        espacoPacote.setPrazo(5);
        controllerEspacoXPacote.salvarEspacoPacote(espacoPacote);

        Clientes_X_PacotesDTO clientePacote = new Clientes_X_PacotesDTO();
        clientePacote.setCliente(clienteDTO.getBody());
        clientePacote.setPacote(pacoteDTO.getBody());
        clientePacote.setQuantidade(2);
        controllerClienteXPacote.salvarClientePacote(clientePacote);

        URI uri = new URI("/api/pagamentos/salvar");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"clientePacote\": {\n" +
                                "        \"id\": 1,\n" +
                                "        \"cliente\": {\n" +
                                "            \"id\": 2,\n" +
                                "            \"nome\": \"kevin\",\n" +
                                "            \"cpf\": \"86048953021\",\n" +
                                "            \"dataNascimento\": \"05/12/2000\",\n" +
                                "            \"contatos\": [\n" +
                                "                {\n" +
                                "                    \"id\": 1,\n" +
                                "                    \"tipoContato\": {\n" +
                                "                        \"id\": 1,\n" +
                                "                        \"nome\": \"email\"\n" +
                                "                    },\n" +
                                "                    \"valor\": \"sdfff@dsfd.com\"\n" +
                                "                },\n" +
                                "                {\n" +
                                "                    \"id\": 2,\n" +
                                "                    \"tipoContato\": {\n" +
                                "                        \"id\": 2,\n" +
                                "                        \"nome\": \"celular\"\n" +
                                "                    },\n" +
                                "                    \"valor\": \"12415425\"\n" +
                                "                }\n" +
                                "            ],\n" +
                                "            \"contratacoes\": null\n" +
                                "        },\n" +
                                "        \"pacote\": {\n" +
                                "            \"id\": 1,\n" +
                                "            \"valor\": 50.0\n" +
                                "        },\n" +
                                "        \"quantidade\": 2,\n" +
                                "        \"pagamento\": null\n" +
                                "    },\n" +
                                "    \"tipoPagamento\": \"credito\"\n" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
                //.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }*/

}